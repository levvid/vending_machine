from __future__ import absolute_import
import os
import time
import asyncio
import serial_asyncio
import socket
import queue
import uuid
import serial
from celery import Celery
from django.conf import settings
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync, sync_to_async

from kiosk.functions.serial_port import write_to_module
from kiosk.functions.serial_port_async import write_and_read
from celery.schedules import crontab
from celery.decorators import periodic_task
from django.apps import apps

from kiosk.jobs.commands import commands
from kiosk.list_serial_ports import serial_ports
from kiosk.jobs.jobs_config import jobs
from kiosk.jobs.job import Job
from kiosk.utils.module_identifier import identify_modules
import environ
import logging

from pegasus.logger_config import get_logger
from kiosk.functions import serial_port
from kiosk.libs.AIOSerial import *
from celery.signals import celeryd_after_setup

logger = get_logger('Celery')

# Initialise environment variables
env = environ.Env()
environ.Env.read_env()

MEDIUM = env('MEDIUM')
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pegasus.settings')
app = Celery('pegasus',
             # include=['kiosk.tasks']
             )

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# default_exchange = Exchange(default_exchange_name, type='direct')
# default_queue = Queue(
#     default_queue_name,
#     default_exchange,
#     routing_key=default_routing_key)
#
# sunshine_queue = Queue(
#     sunshine_queue_name,
#     default_exchange,
#     routing_key=sunshine_routing_key)
#
# moon_queue = Queue(
#     moon_queue_name,
#     default_exchange,
#     routing_key=moon_queue_name)
#
# app.conf.task_queues = (default_queue, sunshine_queue, moon_queue)
#
# app.conf.task_default_queue = default_queue_name
# app.conf.task_default_exchange = default_exchange_name
# app.conf.task_default_routing_key = default_routing_key

GANTRY_LOCATION_TO_RETRIEVE = {
    '1': 'A1',
    '2': 'A2',
    '3': 'A3'
}

RAISE_TOWER = {
    '1': 'U',
    '2': 'Y',
    '3': 'T'
}

GANTRY_MOVE_TO_TOWER = {
    '1': 'T1',
    '2': 'T2',
    '3': 'T3'
}


@app.task(bind=True)
def send_command(self, port, c):
    logger.debug('Sending cmd {} to port {}'.format(c, port))
    try:
        ser = serial.Serial(port, 9600, timeout=1, writeTimeout=1)
        ser.flushInput()
        ser.flushOutput()
        command_response = serial_port.write_to_serial(ser, c, 'Celery', port, True)
        ser.close()

        response = {}
        if command_response == 'G':
            logger.debug('Found gantry at port {}'.format(port))
            send_message_to_job_handler('gantry', 'register_module_response',
                                        port)  # send connect message - message is port
            response = {'gantry': port}
        elif command_response == 'D':
            logger.debug('Found deck at port {}'.format(port))
            send_message_to_job_handler('deck', 'register_module_response',
                                        port)  # send connect message - message is port
            response = {'deck': port}
        elif command_response == 'T':
            logger.debug('Found tower at port {}'.format(port))
            send_message_to_job_handler('tower', 'register_module_response',
                                        port)  # send connect message - message is port
            response = {'tower': port}
        elif command_response == 'A':
            logger.debug('Found additive at port {}'.format(port))
            send_message_to_job_handler('additive', 'register_module_response',
                                        port)  # send connect message - message is port
            response = {'additive': port}
        elif command_response == 'B':
            logger.debug('Found refillAdditive at port {}'.format(port))
            response = {'refillAdditive': port}
        elif command_response == 'R':
            logger.debug('Found reception at port {}'.format(port))
            send_message_to_job_handler('reception', 'register_module_response',
                                        port)  # send connect message - message is port
            response = {'reception': port}
        elif command_response == 'L':
            logger.debug('Found lighting at port {}'.format(port))
            send_message_to_job_handler('lighting', 'register_module_response',
                                        port)  # send connect message - message is port
            response = {'lighting': port}

        return response
    except ValueError:
        return False


def read_serial_port(port):
    ser = serial.Serial(port, 9600, timeout=10)
    bytes_to_read = ser.inWaiting()
    while bytes_to_read == 0:
        time.sleep(1)
        bytes_to_read = ser.inWaiting()
    command_response = str(ser.read(bytes_to_read), 'utf-8')
    logger.debug('module responded with {}'.format(command_response))
    # return command_response


def read_serial(port):
    ser = serial.Serial(port, 9600, timeout=10)
    time.sleep(0.1)
    logger.debug('Reading from port {}'.format(port))
    bytes_to_read = ser.inWaiting()
    while True:
        while bytes_to_read == 0:
            time.sleep(1)
            bytes_to_read = ser.inWaiting()
        command_response = str(ser.read(bytes_to_read))

        if command_response == '':
            logger.debug('Received empty from port.')
        else:
            logger.debug('returning {}'.format(command_response))
    # return commandResponse


@app.task(bind=True)
def debug_task(self):
    logger.debug('Request: {0!r}'.format(self.request))


@app.task(bind=True)
def log_task(self):
    logger.debug('Request: {0!r}'.format(self.request))
    time.sleep(1000)


@app.task(bind=True)
def write_task(self, port, command):
    # logger.debug('Request: {0!r}'.format(self.request))
    # time.sleep(1000)
    return send_command(port, command)


@app.task(bind=True)
def read_task(self, port):
    # logger.debug('Request: {0!r}'.format(self.request))
    # time.sleep(1000)
    read_serial(port)


@app.task(bind=True)
def start_up(self):
    """
    1. sets up all modules.
        => Gantry - G, Deck - D, Reception- R, Lighting - L, Tower - T, Additive - A, refillAdditive - B, scanner - HID,
        payment - javaWebsocket

    2. starts task that monitors module connect/disconnect?
        - add to each module. send data back to task on conn status.
        - when a module is disconnected, notify:
            - DB - check module conn status b4 starting each task? or b4 sending instruction to module?
            - admin? - maybe if disconnected for more than 10 minutes?
            - try to reconnect? - must have tty info. May need to reconnect all modules.
    3.

    """
    ports = []
    return ports


@app.task(bind=True)
def test_start_up(self):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': 'queue_job',
            'job_name': 'start_up',
        }
    )

    return True


def send_message_to_job_handler(module, type, message):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': type,
            'port': message,
            'status': 'CONNECTED',
            'module_name': module,
            'message': 'CONNECTION_STATUS'
        }
    )


# @app.task(bind=True)
# def monitor_refill(self, command):
#     logger.debug('Starting operation...{}'.format(operation_name))
#     channel_layer = get_channel_layer()
#     async_to_sync(channel_layer.send)(
#         'operation_manager',
#         {
#             "type": 'queue_operation',
#             "operation_name": operation_name
#         }
#     )
#     return 'K'

@app.task(bind=True)
def get_additive_code(self, order_uuid):
    from kiosk.models import Order
    return Order.objects.get(uuid=order_uuid).additive_code


@app.task(bind=True)
def get_temperature_code(self, order_uuid):
    from kiosk.models import Order
    return Order.objects.get(uuid=order_uuid).temperature_code


@app.task(bind=True)
def start_operation(self, operation_name, operation_type):
    logger.debug('Starting operation...{}'.format(operation_name))
    from kiosk.models import Order
    order_uuid = ''
    if operation_type == 'order':
        if operation_name == 'make_drop_drink':
            order = Order.objects.create(
                kiosk_id=socket.gethostname(),
                flavor='guava',
                drink_price=0,
                caffeine_level=0,
                temperature='cold',
                water_amount=16,
                order_type='drop_bottle',
                order_medium='Kiosk',
            )

            order_uuid = str(order.uuid)
        else:
            order = Order.objects.create(
                kiosk_id=socket.gethostname(),
                flavor='water',
                drink_price=0,
                caffeine_level=0,
                temperature='cold',
                water_amount=0,
                order_type='refill',
                order_medium='Kiosk',
            )
            order_uuid = str(order.uuid)
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'operation_manager',
        {
            'type': 'queue_operation',
            'operation_name': operation_name,
            'operation_type': operation_type,
            'order_uuid': order_uuid
        }
    )
    return 'K'


@app.task(bind=True)
def start_job(self):
    logger.debug('Starting job...{}'.format('start_up'))
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            "type": 'queue_job',
            "job_name": 'start_up'
        }
    )

    return 'K'


def get_port_list():
    ports = []
    if MEDIUM == 'simulator':
        logger.debug('SIM')
        ports = env('SIMULATOR_PORTS').split(',')
    elif MEDIUM == 'ubuntu_simulator':
        logger.debug('Ubuntu SIM')
        port_range = env('UBUNTU_SIMULATOR_PORTS').split(',')
        ports = []

        for port_number in range(int(port_range[0]), int(port_range[1]) + 1):
            ports.append('/dev/pts/{}'.format(port_number))
    else:  # Kiosk
        ports = serial_ports()
        logger.debug("Kiosk")
        logger.debug('Not FOUND Modules: {}'.format(identify_modules()))
    return ports


@app.task(bind=True)
def connect_modules(self, reconnect=False):
    """
    Connect to all modules.
    """
    ports = get_port_list()
    logger.debug('ports found {}'.format(ports))
    if len(ports) < 7:
        logger.debug('discovered less serial ports than configured modules.')

    for port in ports:
        if reconnect:
            send_command.delay(port, '?')  # query modules
        else:
            send_command.delay(port, '!')  # query modules

    return ports


# @periodic_task(
#     run_every=(crontab(minute='*/1')),
#     name="check_connection_status",
#     # ignore_result=True
# )
@app.task(name='check_connection_status', bind=True)
def check_connection_status(self):
    logger.debug('Running check connection status task.')
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': 'check_module_connection_status'
        }
    )
    return 'checked connection status'


# app.conf.beat_schedule = {
#     # Execute the Speed Test every 1 minutes
#     'check-module-connection-status': {
#         'task': 'check_connection_status',
#         'schedule': crontab(minute='*/1'),
#     },
# }


@app.task(bind=True)
def preauthorize_payment(self):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': 'module_response',
            'message': 'K',
            'module_name': 'payment',
            'result': 'K',
        }
    )


@app.task(bind=True)
def post_order_complete(self): # add to celery beat schedule
    from django.db.models import Sum
    from kiosk.models import Order
    total_water_amount = Order.objects.aggregate(Sum('water_amount'))  # only count when water amount is more than 0

    num_bottles_saved = 0
    if total_water_amount > 0:
        num_bottles_saved = int(total_water_amount / 16)
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'kiosk_client_group', {
            'type': 'job_handler_message',
            'message': {
                'topic': 'number_of_bottles_saved',
                'status': 'number_of_bottles_update',
                'data': num_bottles_saved  # number of bottles saved

            },
        }
    )


@app.task(bind=True)
def post_order_complete(self):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': 'module_response',
            'message': 'K',
            'module_name': 'post_order_complete',
            'result': 'K',
        }
    )


@app.task(bind=True)
def drop_bottle_order(self, order):
    logger.debug('Received bottle order: {}'.format(order))
    from kiosk.models import Order
    order = Order.objects.create(
        kiosk_id=socket.gethostname(),
        flavor=order['flavor'].lower(),
        drink_price=0,
        caffeine_level=order['caffeine_level'],
        temperature=order['temperature'].lower(),
        water_amount=16,
        order_type='drop_bottle',
        order_medium='Kiosk',
    )

    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'operation_manager', {
            'type': 'queue_operation',
            'message': 'K',
            'operation_name': 'make_drop_drink',
            'order_uuid': str(order.uuid),
            'operation_type': 'order'
        }
    )


@app.task(bind=True)
def refill_order(self, order):
    logger.debug('Received refill order: {}'.format(order))
    from kiosk.models import Order
    order = Order.objects.create(
        kiosk_id=socket.gethostname(),
        flavor=order['flavor'].lower(),
        drink_price=0,
        caffeine_level=order['caffeine_level'],
        temperature=order['temperature'].lower(),  # cold, room,
        water_amount=0,
        order_type='refill',
        order_medium='Kiosk',
    )

    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'operation_manager', {
            'type': 'queue_operation',
            'message': 'K',
            'operation_name': 'make_refill_drink',
            'order_uuid': str(order.uuid),
            'operation_type': 'order'
        }
    )


@app.task(bind=True)
def dispense(self, command):
    channel_layer = get_channel_layer()

    # if command == 'end_order':
    #     async_to_sync(channel_layer.send)(
    #         'job_handler',
    #         {
    #             'type': 'module_response',
    #             'message': 'K',
    #             'module_name': 'refill',
    #             'result': 'K',
    #         }
    #     )
    # else:
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            "type": 'dispense',
            "command": command,
        }
    )


@app.task(bind=True)
def monitor_refill(self):
    channel_layer = get_channel_layer()
    # send S to refill every second?
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            "type": 'monitor_refill',
            "command": 'check_dispense_status',
        }
    )


@app.task(bind=True)
def send_msg(self, msg):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': 'kiosk_client',
            'message': 'msg'
        }
    )


async def sync_command(modules, second_command, ):
    first_module = modules[0]
    second_module = modules[1]
    reader, _ = await serial_asyncio.open_serial_connection(url='./reader', baudrate=115200)
    print('Reader created')
    _, writer = await serial_asyncio.open_serial_connection(url='./writer', baudrate=115200)
    print('Writer created')
    messages = [b'foo\n', b'bar\n', b'baz\n', b'qux\n']
    sent = send(writer, messages)
    received = recv(reader)
    await asyncio.wait([sent, received])


async def test_command(data):
    command = data['command']
    logger.info('Received command {}'.format(command))
    command_to_write = command['command']

    port = data['port']

    reader, writer = await serial_asyncio.open_serial_connection(url=port, baudrate=115200)
    print('Reader created')

    sent = send(writer, command_to_write)
    received = recv(reader)
    await asyncio.wait([sent, received])

    print('Received {}'.format(received))

    return received


async def send(w, msgs):
    for msg in msgs:
        w.write(msg)
        print(f'sent: {msg.decode().rstrip()}')
    print('Done sending')


def check_for_lower(s):
    for c in s:
        if c.islower():
            return False

    return True


async def recv(r):
    while True:
        msg = await r.readuntil(b'\r\n')
        if len(msg.rstrip()) > 0 and check_for_lower(msg):
            print('Done receiving {}'.format(msg))
            return msg
        print(f'received: {msg.rstrip().decode()}')


@app.task(bind=True)
def module_logger(self, log_level, module_name, log_message):
    if log_level == 'debug':
        logger.debug('{} - {}'.format(module_name, log_message))
    if log_level == 'warning':
        logger.warning('{} - {}'.format(module_name, log_message))
    if log_level == 'info':
        logger.info('{} - {}'.format(module_name, log_message))
    if log_level == 'error':
        logger.error('{} - {}'.format(module_name, log_message))
    if log_level == 'critical':
        logger.critical('{} - {}'.format(module_name, log_message))
    if log_level == 'exception':
        logger.exception('{} - {}'.format(module_name, log_message))

    return '{} - {}'.format(module_name, log_message)


def get_tower_with_bottles(inventory):
    if not kiosk_out_of_bottles(inventory):
        selected_tower = inventory.selected_tower

        if selected_tower == '1':
            if inventory.tower_one_remaining == 0:
                selected_tower = 2
                inventory.selected_tower = selected_tower
                inventory.save()
        if selected_tower == '2':
            if inventory.tower_two_remaining == 0:
                selected_tower = 3
                inventory.selected_tower = selected_tower
                inventory.save()
        if selected_tower == '3':
            if inventory.tower_three_remaining == 0:
                selected_tower = 1
                inventory.selected_tower = selected_tower
                inventory.save()

        return inventory.selected_tower
    else:  # kiosk out of bottles
        # Error out
        return 'E'


@app.task(bind=True)
def get_gantry_location_for_tower(self):
    """
        Tower 1: T1,
        Tower 2: T2,
        Tower 2: T3
    """
    from kiosk.models import Inventory
    inventory = Inventory.objects.all().first()

    selected_tower = inventory.selected_tower
    logger.debug('get_gantry_location_for_tower selected_tower: {} cmd: {}'.format(selected_tower, GANTRY_MOVE_TO_TOWER[
        selected_tower]))

    working_tower = get_tower_with_bottles(inventory)

    if working_tower == 'E':
        return working_tower
    return GANTRY_MOVE_TO_TOWER[str(working_tower)]


@app.task(bind=True)
def get_raise_tower_commmand(self):
    from kiosk.models import Inventory
    inventory = Inventory.objects.all().first()

    selected_tower = inventory.selected_tower
    logger.debug(
        'get_raise_tower_commmand selected_tower: {} cmd: {}'.format(selected_tower, RAISE_TOWER[selected_tower]))

    return RAISE_TOWER[selected_tower]


@app.task(bind=True)
def get_gantry_location_to_retrieve(self):
    """
        A1, A2, A3
    """
    from kiosk.models import Inventory
    inventory = Inventory.objects.all().first()

    selected_tower = inventory.selected_tower
    logger.debug('get_gantry_location_to_retrieve selected_tower: {} cmd: {}'.format(selected_tower,
                                                                                     GANTRY_LOCATION_TO_RETRIEVE[
                                                                                         selected_tower]))

    return GANTRY_LOCATION_TO_RETRIEVE[selected_tower]


def kiosk_out_of_bottles(inventory):
    return inventory.tower_one_remaining == inventory.tower_two_remaining == inventory.tower_three_remaining == 0


@app.task(bind=True)
def update_bottle_count(self):
    from kiosk.models import Inventory
    inventory = Inventory.objects.all().first()

    selected_tower = inventory.selected_tower
    if selected_tower == '1':
        logger.debug('tower 1 selected')
        remaining_bottles = inventory.tower_one_remaining - 1
        inventory.tower_one_remaining = remaining_bottles
        if remaining_bottles > 0:
            inventory.save()
            return GANTRY_MOVE_TO_TOWER[selected_tower]
        else:
            if not kiosk_out_of_bottles(inventory):
                selected_tower = '2'
                inventory.selected_tower = selected_tower
                inventory.save()
            else:
                logger.error('Tower out of bottles.')
    if selected_tower == '2':
        logger.debug('tower 2 selected')
        remaining_bottles = inventory.tower_two_remaining - 1
        inventory.tower_two_remaining = remaining_bottles
        if remaining_bottles > 0:
            inventory.save()
            return GANTRY_MOVE_TO_TOWER[selected_tower]
        else:
            selected_tower = '3'
            inventory.selected_tower = selected_tower
            inventory.save()
    if selected_tower == '3':
        logger.debug('tower 3 selected')
        remaining_bottles = inventory.tower_three_remaining - 1
        inventory.tower_three_remaining = remaining_bottles
        if remaining_bottles > 0:
            inventory.save()
            return GANTRY_MOVE_TO_TOWER[selected_tower]
        else:
            selected_tower = '1'
            inventory.selected_tower = selected_tower
            inventory.save()


def get_caffeine_dispensed(order, water_amount_ml=400):
    caffeine_dispensed = 0
    if order.caffeine_level == 1:
        caffeine_dispensed = 1.68 * (water_amount_ml / 400)
    elif order.caffeine_level == 2:
        caffeine_dispensed = 3.37 * (water_amount_ml / 400)
    elif order.caffeine_level == 3:
        caffeine_dispensed = 5.9 * (water_amount_ml / 400)

    return caffeine_dispensed


@app.task(bind=True)
def post_inventory_data(self, order_uuid):
    if not order_uuid:
        return
    from kiosk.models import Order, Additives

    order = Order.objects.get(uuid=order_uuid)
    additive = Additives.objects.get(name=order.flavor)
    caffeine = Additives.objects.get(name='caffeine')
    additive_dispensed = 0
    caffeine_dispensed = 0
    if order.order_type == 'drop_bottle':
        logger.debug('drop_bottle inventory post')

        # update number of bottles and selected tower if necessary
        caffeine_dispensed = get_caffeine_dispensed(order)
        if order.flavor != 'water':
            additive_dispensed = additive.concentration
    elif order.order_type == 'refill':
        logger.debug('refill inventory post')
        water_amount_ml = order.water_amount * 29.574  # order.water_amount is in ounces
        caffeine_dispensed = get_caffeine_dispensed(order, water_amount_ml)

        if order.flavor != 'water':
            additive_dispensed = additive.concentration * (water_amount_ml / 400)  # water_amount_ml/400 is the amount
            # dispensed in a normal bottle and that's what the concentration is normalized to.

    additive.amount_remaining -= additive_dispensed
    additive.save()

    caffeine.amount_remaining -= caffeine_dispensed
    caffeine.save()

    return additive_dispensed


@celeryd_after_setup.connect
def reset_machine(sender, instance, **kwargs):
    start_operation.delay('start_up', '')


@app.task(bind=True)
def get_drink_price(self, order_uuid):
    from kiosk.models import Order, DrinkPrice

    try:
        order = Order.objects.get(uuid=order_uuid)

        prices = DrinkPrice.objects.all().first()

        if order.order_type == 'drop_bottle':
            water_price = prices.drop_bottle_just_water
            flavor_price = prices.drop_bottle_flavor
            supplement_price = prices.drop_bottle_supplement
            pass
        else:
            water_price = 0
            flavor_price = prices.refill_flavor
            supplement_price = prices.refill_supplement

        if order.flavor == 'water':
            order.drink_price = water_price
        else:
            order.drink_price = flavor_price

        if order.caffeine_level != 0:
            order.drink_price += order.caffeine_level * supplement_price

        order.save()
        return order.drink_price
    except:
        return None


@app.task(bind=True)
def run_maintenance_task(self, task):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(
        'job_handler',
        {
            'type': 'get_modules_for_maintenance',
            'task_name': task
        }
    )


@app.task(bind=True)
def exec_maintenance_task(self, task, modules):
    commands_list = []
    command = None
    if task == 'Raise Tower 1':
        command = commands['raise_tower_one']
    elif task == 'Raise Tower 2':
        command = commands['raise_tower_two']
    elif task == 'Raise Tower 3':
        command = commands['raise_tower_three']
    elif task == 'Lower Tower 1':
        command = commands['lower_tower_one']
    elif task == 'Lower Tower 2':
        command = commands['lower_tower_two']
    elif task == 'Lower Tower 3':
        command = commands['lower_tower_three']

    if command:
        commands_list.append(command)
    # purging
    if task == 'Purge Additive 1 Drop Bottle':
        purge_additive_one = commands['purge_additive_one']
        set_additive_to_compostable = commands['set_additive_to_compostable']
        command = commands['purge_additive_one']
        commands_list = [set_additive_to_compostable, purge_additive_one]
    elif task == 'Purge Additive 2 Drop Bottle':
        purge_additive_two = commands['purge_additive_two']
        set_additive_to_compostable = commands['set_additive_to_compostable']
        command = commands['purge_additive_two']
        commands_list = [set_additive_to_compostable, purge_additive_two]
    elif task == 'Purge Additive 3 Drop Bottle':
        purge_additive_three = commands['purge_additive_three']
        set_additive_to_compostable = commands['set_additive_to_compostable']
        command = commands['purge_additive_three']
        commands_list = [set_additive_to_compostable, purge_additive_three]
    elif task == 'Purge Additive 4 Drop Bottle':
        purge_additive_four = commands['purge_additive_four']
        set_additive_to_compostable = commands['set_additive_to_compostable']
        command = commands['purge_additive_four']
        commands_list = [set_additive_to_compostable, purge_additive_four]
    elif task == 'Purge Additive 5 Drop Bottle':
        purge_additive_five = commands['purge_additive_five']
        set_additive_to_compostable = commands['set_additive_to_compostable']
        command = commands['purge_additive_five']
        commands_list = [set_additive_to_compostable, purge_additive_five]
    elif task == 'Purge Additive 6 Drop Bottle':
        purge_additive_six = commands['purge_additive_six']
        set_additive_to_compostable = commands['set_additive_to_compostable']
        command = commands['purge_additive_six']
        commands_list = [set_additive_to_compostable, purge_additive_six]

    elif task == 'Purge Additive 1 Refill':  # refill, add water flush after, ##### TODO refill purge workflow
        # REFILL PURGE
        set_purge = commands['set_purge']
        purge_additive_one = commands['purge_additive_one']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_one']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, set_purge, purge_additive_one, start_dispense]
    elif task == 'Purge Additive 2 Refill':  # refill, add water flush after, ##### TODO refill purge workflow
        # REFILL PURGE
        set_purge = commands['set_purge']
        purge_additive_two = commands['purge_additive_two']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_two']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, set_purge, purge_additive_two, start_dispense]
    elif task == 'Purge Additive 3 Refill':  # refill, add water flush after, ##### TODO refill purge workflow
        # REFILL PURGE
        set_purge = commands['set_purge']
        purge_additive_three = commands['purge_additive_three']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_three']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, set_purge, purge_additive_three, start_dispense]
    elif task == 'Purge Additive 4 Refill':
        set_purge = commands['set_purge']
        purge_additive_four = commands['purge_additive_four']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_four']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, set_purge, purge_additive_four, start_dispense]

    elif task == 'Purge Additive 5 Refill':
        set_purge = commands['set_purge']
        purge_additive_five = commands['purge_additive_five']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_five']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, set_purge, purge_additive_five, start_dispense]
    elif task == 'Purge Additive 6 Refill':
        set_purge = commands['set_purge']
        purge_additive_six = commands['purge_additive_six']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_six']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, set_purge, purge_additive_six, start_dispense]
    elif task == 'Flush Refill Cold':
        # set_purge = commands['set_purge']
        purge_additive_six = commands['purge_additive_six']
        set_additive_to_refill = commands['set_additive_to_refill']
        command = commands['purge_additive_six']

        start_dispense = commands['start_dispense']
        commands_list = [set_additive_to_refill, purge_additive_six, start_dispense]


    elif task == 'Open Gripper':
        command = commands['open_gripper']
        commands_list = [command]
    elif task == 'Close Gripper':
        command = commands['close_gripper']
        commands_list = [command]
    elif task == 'Open Reception Door':
        command = commands['open_reception_door']
        commands_list = [command]
    elif task == 'Close Reception Door':
        command = commands['close_reception_door']
        commands_list = [command]

    module = command['module']
    port = modules[module]['port']
    ser = serial.Serial(port, 9600, timeout=1, writeTimeout=1)
    ser.flushInput()
    ser.flushOutput()

    for cmd in commands_list:
        command_response = serial_port.write_to_serial(ser, cmd['command'], module, port, True)

        module_logger.delay('debug', 'Celery',
                            'exec command {} with response {}'.format(command['command'], command_response))
    ser.close()
