# pegasus/asgi.py

"""
ASGI config for pegasus project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

import django
from channels.auth import AuthMiddlewareStack
from channels.layers import get_channel_layer
from channels.routing import ProtocolTypeRouter, URLRouter, ChannelNameRouter
from django.core.asgi import get_asgi_application

import kiosk.module_consumers.job_handler_consumer
import kiosk.module_consumers.operation_manager_consumer
import kiosk.routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pegasus.settings')
django.setup()

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            kiosk.routing.websocket_urlpatterns,
        )
    ),
    "channel": ChannelNameRouter({
        # job handler consumer
        "job_handler": kiosk.module_consumers.job_handler_consumer.JobHandlerConsumer.as_asgi(),
        "operation_manager": kiosk.module_consumers.operation_manager_consumer.OperationManagerConsumer.as_asgi(),
    }),
})
# application = get_default_application()

channel_layer = get_channel_layer()
