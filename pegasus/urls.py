"""pegasus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
# from django.urls import include, url
from django.conf.urls import url, include
from kiosk import views
from django.conf.urls import url
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view

urlpatterns = [
    url(r'', include_docs_urls(title='Kiosk API')),
    path('admin/', admin.site.urls),
    path('kiosks_test/', include('kiosk.urls')),
    # url('start_up/', include('kiosk.urls')),
    path('api/start_up/', views.StartUpAPIView, name='start_up'),

    path('api/inventory/', views.InventoryListApiView.as_view(), name='inventory'),
    path('api/inventory/update/', views.InventoryUpdateApiView.as_view(), name='inventory_update'),
    path('api/flavors/', views.AdditivesApiView.as_view(), name="flavors"),
    path('api/flavors/update/', views.AdditivesUpdateView.as_view(), name='flavors_update'),
    path('api/drinkprices/', views.DrinkPriceApiView.as_view(), name='drink prices'),
    path('api/drinkprices/update/', views.DrinkPriceUpdateApiView.as_view(), name='drink price update'),
    path('api/modulestatus/', views.ModuleStatusApiView.as_view(), name="modulestatus"),
    path('api/kiosk/', views.KioskApiView.as_view(), name="kiosk"),
    path('api/orders/', views.OrderApiView.as_view(), name="orders"),
    path('api/orders/count/', views.OrderCountApiView.as_view(), name="num_orders"),
    path('api/order/create/', views.OrderCreateApiView.as_view(), name="order_create"),



    # Operations
    path('api/operations/list/', views.OperationsApiView.as_view(), name="operations_list"),
    path('api/operations/run/', views.OperationsCreateApiView.as_view(), name="operations_run"),
    path('api/tasks/run/', views.TasksCreateApiView.as_view(), name="tasks_run"),

    path('api/maintenance/', views.MaintenanceTasksApiView.as_view(), name="maintenance"),

    path('api', get_schema_view(
        title="Kiosk APIs",
        description="Kiosk APIs "
    ), name='api'),

]

urlpatterns += [
    path('kiosk_client/', include('kiosk.urls')),
]
