import environ

env = environ.Env()
environ.Env.read_env()

SELECTED_TOWER = env('SELECTED_TOWER')

GANTRY_LOCATION_TO_RETRIEVE = {
    '1': 'A1',
    '2': 'A2',
    '3': 'A3'
}

RAISE_TOWER = {
    '1': 'U',
    '2': 'Y',
    '3': 'T'
}

GANTRY_MOVE_TO_TOWER = {
    '1': 'T1',
    '2': 'T2',
    '3': 'T3'
}

from pegasus.logger_config import get_logger

logger = get_logger('Inventory')


