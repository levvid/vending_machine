import datetime


class Job:
    def __init__(self, name, tasks, entry_point, num_retries):
        self.start_time = datetime.datetime.now()
        self.name = name
        self.end_time = None
        self.status = 'QUEUED'
        self.tasks = tasks
        self.current_task = entry_point
        self.num_retries = num_retries
        self.entry_point = entry_point

    def set_end_time(self):
        self.end_time = datetime.datetime.now()

    def set_status(self, new_status):
        self.status = new_status

    def set_current_task(self, new_task):
        self.current_task = new_task

    def complete_job(self):
        self.current_task = 'idle'
        self.status = 'complete'
        self.end_time = datetime.datetime.now()

    def get_current_task(self):
        return self.tasks[self.current_task]

    @property
    def info(self):
        return ' Job name: {}\n' \
               'Time Started: {}\n' \
               'Time Ended: {}\nNum Retries: {}' \
               'Tasks: {}'.format(self.name, self.start_time, self.end_time, self.num_retries, [task_name for task_name in self.tasks.keys()])
