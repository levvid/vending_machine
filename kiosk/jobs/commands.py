commands = {
    'check_if_holding_bottle': {
        'name': 'check_if_holding_bottle',
        'module': 'gantry',
        'command': 'BB',
    },
    'move_to_neutral_position': {
        'name': 'move_to_neutral_position',
        'module': 'gantry',
        'command': 'BN',
    },
    'cap_check': {
        'name': 'cap_check',
        'module': 'deck',
        'command': 'Q',
    },
    'move_to_capper_for_capping': {
        'name': 'move_to_capper_for_capping',
        'module': 'gantry',
        'command': 'BM',
    },
    'gantry_move_to_tower': {
        'name': 'gantry_move_to_tower',
        'module': 'gantry',
        'command': 'T1',
        # gantryLocationForTowerHeight - T1, T2, T3
    },
    'wait_for_tower': {
        'name': 'wait_for_tower',
        'module': 'gantry',
        'command': 'GT',  #
    },
    'raise_level': {
        'name': 'raise_level',
        'module': 'tower',
        'command': 'GT',
    },
    'stop_tower': {
        'name': 'stop_tower',
        'module': 'tower',
        'command': 'S',
    },
    'grab_bottle': {
        'name': 'grab_bottle',
        'module': 'gantry',
        'command': 'A1',  #
        # gantryLocationToRetrieve - A1, A2, A3
    },
    'double_bottle_check': {
        'name': 'double_bottle_check',
        'module': 'gantry',
        'command': 'BC',
    },
    'move_slider_to_drop_off': {
        'name': 'move_slider_to_drop_off',
        'module': 'reception',
        'command': 'D',
    },
    'garbage_bottle': {
        'name': 'garbage_bottle',
        'module': 'gantry',
        'command': 'GG',
    },
    'flap_seal_one': {
        'name': 'flap_seal_bottle',
        'module': 'gantry',
        'command': 'FC',
    },
    'flap_seal_two': {
        'name': 'flap_seal_bottle',
        'module': 'gantry',
        'command': 'FC',
    },
    'turn_on_uv_light': {
        'name': 'turn_on_uv_light',
        'module': 'deck',
        'command': 'V',
    },
    'turn_off_uv_light': {
        'name': 'turn_off_uv_light',
        'module': 'deck',
        'command': 'B',
    },
    'move_to_capper_for_uncapping': {
        'name': 'move_to_capper_for_uncapping',
        'module': 'gantry',
        'command': 'BT',
    },
    'move_for_uncap': {
        'name': 'move_for_uncap',
        'module': 'gantry',
        'command': 'BU'
    },
    'move_for_cap': {
        'name': 'move_for_cap',
        'module': 'gantry',
        'command': 'BD'
    },
    'probe_bottle': {
        'name': 'probe_bottle',
        'module': 'gantry',
        'command': 'BP'
    },
    'raise_tower_level': {
        'name': 'raise_tower_level',
        'module': 'tower',
        'command': 'U'  # tower 1 - U, tower 2 - Y,
        # tower 3 - T
    },
    'uncap_bottle': {
        'name': 'uncap_bottle',
        'module': 'deck',
        'command': 'U'
    },
    'cap_bottle': {
        'name': 'cap_bottle',
        'module': 'deck',
        'command': 'C'
    },
    'move_to_filler': {
        'name': 'move_to_filler',
        'module': 'gantry',
        'command': 'BF'
    },
    'set_additive_to_compostable': {
        'name': 'set_additive_to_compostable',
        'module': 'additive',
        'command': 'F'
    },
    'set_additive_to_refill': {
        'name': 'set_additive_to_refill',
        'module': 'additive',
        'command': 'R'
    },
    'move_slider_to_garbage': {
        'name': 'move_slider_to_garbage',
        'module': 'reception',
        'command': 'G'
    },
    'write_additive_code': {
        'name': 'write_additive_code',
        'module': 'additive',
        'command': '00000000'
    },
    'write_temperature_code': {
        'name': 'write_temperature_code',
        'module': 'deck',
        'command': '0'
    },
    'write_temperature_code_refill': {
        'name': 'write_temperature_code_refill',
        'module': 'additive',
        'command': 'W'  # cold - W, 8, ambient - 0, C
    },
    'deliver_bottle': {
        'name': 'deliver_bottle',
        'module': 'gantry',
        'command': '00'
    },
    'open_door': {
        'name': 'open_door',
        'module': 'reception',
        'command': 'O'
    },
    'close_door': {
        'name': 'close_door',
        'module': 'reception',
        'command': 'C'
    },
    'release_bottle': {
        'name': 'release_bottle',
        'module': 'gantry',
        'command': 'GG'
    },
    'set_temperature': {
        'name': 'release_bottle',
        'module': 'additive',
        'command': 'W'
    },
    'start_dispense': {
        'name': 'start_dispense',
        'module': 'additive',
        'command': 'P'
    },
    'end_dispense': {
        'name': 'end_dispense',
        'module': 'additive',
        'command': 'O'
    },
    'check_dispense_status': {
        'name': 'check_dispense_status',
        'module': 'additive',
        'command': 'S'
    },
    'raise_tower_one': {
        'name': 'raise_tower_one',
        'module': 'tower',
        'command': 'F'
    },
    'raise_tower_three': {
        'name': 'raise_tower_three',
        'module': 'tower',
        'command': 'G'
    },
    'raise_tower_three': {
        'name': 'raise_tower_three',
        'module': 'tower',
        'command': 'H'
    },
    'lower_tower_one': {
        'name': 'lower_tower_three',
        'module': 'tower',
        'command': 'R'
    },
    'lower_tower_two': {
        'name': 'lower_tower_three',
        'module': 'tower',
        'command': 'E'
    },
    'lower_tower_three': {
        'name': 'lower_tower_three',
        'module': 'tower',
        'command': 'W'
    },

    'set_purge': {
        'name': 'lower_tower_three',
        'module': 'additive',
        'command': 'N'
    },
    'purge_additive_one': {
        'name': 'purge_additive_one',
        'module': 'additive',
        'command': 'W0000000'
    },
    'purge_additive_two': {
        'name': 'purge_additive_two',
        'module': 'additive',
        'command': '0W000000'
    },
    'purge_additive_three': {
        'name': 'purge_additive_three',
        'module': 'additive',
        'command': '00W00000'
    },
    'purge_additive_four': {
        'name': 'purge_additive_four',
        'module': 'additive',
        'command': '000W0000'
    },
    'purge_additive_five': {
        'name': 'purge_additive_five',
        'module': 'additive',
        'command': '0000W000'
    },
    'purge_additive_six': {
        'name': 'purge_additive_six',
        'module': 'additive',
        'command': '00000W00'
    },
    'open_gripper': {
        'name': 'open_gripper',
        'module': 'gantry',
        'command': 'GO'
    },
    'close_gripper': {
        'name': 'close_gripper',
        'module': 'gantry',
        'command': 'GC'
    },
    'open_reception_door': {
        'name': 'open_reception_door',
        'module': 'reception',
        'command': 'O'
    },
    'close_reception_door': {
        'name': 'close_reception_door',
        'module': 'reception',
        'command': 'C'
    },
    'flush_refill_cold': {
        'name': 'flush_refill_cold',
        'module': 'additive',
        'command': 'C'
    },
}
