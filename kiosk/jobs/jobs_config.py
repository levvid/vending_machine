jobs = {
    'start_up': {
        'entry_point': 'register_modules',
        'tasks': {  # flap seal on boot up
            'register_modules': {
                'type': 'task',
                'result': {
                    'K': 'check_if_holding_bottle',
                    'E': 'error'
                },
            },
            'check_if_holding_bottle': {
                'type': 'command',
                'result': {
                    'K': 'check_for_uncap',
                    # 'K': 'retrieve_close_and_uncap_next_bottle',  # TEST
                    'N': 'retrieve_close_and_uncap_next_bottle',
                    'E': 'error'
                },
            },
            'check_for_uncap': {
                'type': 'job',
                'result': {
                    'C': 'move_to_capper_for_capping',  # cap present, mv to capper for uncapping
                    'N': 'uncap_and_probe_bottle',
                    'E': 'error',
                    'K': 'uncap_and_probe_bottle',  # only for testing purposes
                },
            },
            'uncap_and_probe_bottle': {
                'type': 'job',
                'result': {
                    'K': 'send_message_to_kiosk_client',
                },
            },
            'retrieve_close_and_uncap_next_bottle': {
                'type': 'job',
                'result': {
                    'K': 'send_message_to_kiosk_client',
                },
            },
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'send_message_to_kiosk_client',
                },
                # 'status': 'complete'
            },
            'send_message_to_kiosk_client': {
                'type': 'client_message',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete',
                'message': {
                    'topic': 'kiosk_status',
                    'status': 'start_up_complete',
                    'data': 'Start up complete'
                }
            }
        },
    },
    'check_for_uncap': {
        'entry_point': 'move_to_neutral_position',
        'tasks': {
            'move_to_neutral_position': {
                'type': 'command',
                'result': {
                    'K': 'cap_check',
                },
            },
            'cap_check': {
                'type': 'command',
                'result': {
                    'C': 'complete',  # cap present, mv to capper for uncapping
                    'N': 'complete',
                    'K': 'complete',
                },
                'status': 'complete'
            },
        }
    },
    'uncap_and_probe_bottle': {
        'entry_point': 'turn_on_uv_light',
        'tasks': {
            'turn_on_uv_light': {
                'type': 'command',
                'result': {
                    'K': 'uncap_bottle',
                },
            },
            'uncap_bottle': {
                'type': 'job',
                'result': {
                    'K': 'check_for_uncap',
                }
            },  # move to neutral, check_for_uncap, BN
            'check_for_uncap': {
                'type': 'job',
                'result': {
                    'C': 'probe_bottle',
                    'K': 'probe_bottle',
                    # 'N': 'retry'
                }
            },
            'probe_bottle': {
                'type': 'command',
                'result': {
                    'K': 'move_to_capper_for_capping',
                }
            },
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'turn_off_uv_light',
                }
            },
            'turn_off_uv_light': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }
    },
    'retrieve_close_and_uncap_next_bottle': {
        'entry_point': 'retrieve_next_bottle',
        'tasks': {
            'retrieve_next_bottle': {
                'type': 'job',
                'result': {
                    'K': 'update_bottle_count',
                }
            },
            'update_bottle_count': {
                'type': 'task',
                'result': {
                    'K': 'double_bottle_check',
                },
            },
            'double_bottle_check': {
                'type': 'command',
                'result': {
                    'D': 'garbage_bottle',
                    'S': 'flap_seal_bottle',
                    'K': 'flap_seal_bottle'  # TEST
                }
            },
            'garbage_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
            'flap_seal_bottle': {
                'type': 'job',
                'result': {
                    'K': 'uncap_and_probe_bottle',
                }
            },
            'uncap_and_probe_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        },
    },
    'retrieve_next_bottle': {
        'entry_point': 'gantry_move_to_tower',
        'num_retries': 5,
        'tasks': {
            'gantry_move_to_tower': {
                'type': 'task',
                'result': {
                    'K': 'raise_bottle_to_gantry',
                }
            },
            'raise_bottle_to_gantry': {
                'type': 'sync_task',
                'result': {
                    'K': 'stop_tower',
                },
                'sync_tasks': ['raise_tower_level', 'wait_for_tower'],
            },
            'raise_tower_level': {
                'type': 'task',
                'result': {
                    'K': 'complete',
                },
                'read_response': False
            },
            'wait_for_tower': {
                'type': 'task',
                'result': {
                    'K': 'complete',
                },
                'read_response': True
            },
            'stop_tower': {
                'type': 'command',
                'result': {
                    'K': 'grab_bottle',
                }
            },
            'grab_bottle': {
                'type': 'task',
                'result': {
                    'K': 'complete',
                    # 'K': 'retry',
                    'N': 'retry'
                },
                'status': 'complete'
            },
        }
    },
    'garbage_bottle': {
        'entry_point': 'move_slider_to_drop_off',
        'tasks': {
            'move_slider_to_drop_off': {
                'type': 'command',
                'result': {
                    'K': 'release_bottle',
                }
            },
            'release_bottle': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }
    },
    'flap_seal_bottle': {
        'entry_point': 'flap_seal_one',
        'tasks': {
            'flap_seal_one': {
                'type': 'command',
                'result': {
                    'K': 'flap_seal_two',
                },
            },
            'flap_seal_two': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }

    },
    'uncap_bottle': {
        'entry_point': 'move_to_capper_for_uncapping',
        'num_retries': 5,
        'tasks': {
            'move_to_capper_for_uncapping': {
                'type': 'command',
                'result': {
                    'K': 'remove_cap'
                }
            },
            'remove_cap': {
                'type': 'sync_command',
                'result': {
                    'K': 'check_for_uncap',
                },
                'sync_commands': ['uncap_bottle', 'move_for_uncap'],
                'group_name': 'gantry_deck',
                'num_responses': 2
            },
            'uncap_bottle': {
                'type': 'command',
                'result': {
                    'K': 'check_for_uncap'
                },
                'read_response': True
            },
            'move_for_uncap': {
                'type': 'command',
                'result': {
                    'K': 'check_for_uncap'
                },
                'read_response': True
            },
            'check_for_uncap': {
                'type': 'job',
                'result': {
                    'C': 'move_to_capper_for_capping',
                    'K': 'move_to_capper_for_capping',
                    'N': 'retry'
                }
            },
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }
    },
    'make_drop_drink': {
        'entry_point': 'preauthorize_payment',
        'tasks': {
            'preauthorize_payment': {
                'type': 'task',
                'result': {
                    'K': 'send_payment_processing_status',
                },
            },
            'send_payment_processing_status': {
                'type': 'client_message',
                'result': {
                    'K': 'send_payment_success_status',
                },
                'message': {
                    'topic': 'payment',
                    'status': 'processing',  # failed, retrying...,
                    'data': 'processing payment'
                }
            },
            'send_payment_success_status': {
                'type': 'client_message',
                'result': {
                    'K': 'prepare_for_fill',
                },
                'message': {
                    'topic': 'payment',
                    'status': 'success',  # failed, retrying...,
                    'data': 'payment succeeded',
                }
            },
            'prepare_for_fill': {
                'type': 'sync_command',
                'result': {
                    'K': 'cap_check',
                },
                'sync_commands': ['move_to_filler', 'set_additive_to_compostable', 'move_slider_to_garbage'],
                'num_responses': 3
            },
            'move_to_filler': {
                'type': 'command',
                'result': {
                    'K': 'perform_fill',
                },
                'read_response': True
            },
            'set_additive_to_compostable': {
                'type': 'command',
                'result': {
                    'K': 'cap_check',
                },
                'read_response': True
            },
            'move_slider_to_garbage': {
                'type': 'command',
                'result': {
                    'K': 'cap_check',
                },
                'read_response': True
            },
            'cap_check': {
                'type': 'command',
                'result': {
                    'K': 'perform_fill',  # TEST
                    'C': 'perform_fill',
                    'N': 'error',
                },
            },
            'perform_fill': {
                'type': 'sync_task',
                'result': {
                    'K': 'cap_and_deliver_bottle',
                },
                'sync_tasks': ['write_temperature_code', 'write_additive_code'],
            },
            'write_additive_code': {
                'type': 'task',
                'result': {
                    'K': 'complete',
                },
                'read_response': True,
            },
            'write_temperature_code': {
                'type': 'task',
                'result': {
                    'K': 'complete',
                },
                'read_response': True,
            },
            'cap_and_deliver_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
            },
        }
    },
    'make_refill_drink': {
        'entry_point': 'set_additive_to_refill',
        'tasks': {
            'preauthorize_payment': {
                'type': 'task',
                'result': {
                    'K': 'send_payment_processing_status',
                },
            },
            'send_payment_processing_status': {
                'type': 'client_message',
                'result': {
                    'K': 'send_payment_success_status',
                },
                'status': 'complete',
                'message': {
                    'topic': 'payment',
                    'data': 'processing payment',
                    'status': 'processing'  # failed, retrying...,
                }
            },
            'send_payment_success_status': {
                'type': 'client_message',
                'result': {
                    'K': 'set_additive_to_refill',
                },
                'status': 'complete',
                'message': {
                    'topic': 'payment',
                    'data': 'payment succeeded',
                    'status': 'success'  # failed, retrying...,
                }
            },
            'set_additive_to_refill': {
                'type': 'command',
                'result': {
                    'K': 'perform_fill',
                },
            },
            'perform_fill': {
                'type': 'sync_command_refill',
                'result': {
                    'K': 'monitor_refill',
                },
                'sync_commands': ['write_additive_code', 'write_temperature_code_refill'],
            },
            'write_additive_code': {
                'type': 'task',
                'result': {
                    'K': 'monitor_refill',
                },
                'read_response': True
            },
            'monitor_refill': {
                'type': 'task',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
            'write_temperature_code_refill': {
                'type': 'task',
                'result': {
                    'K': 'write_additive_code',
                },
                'read_response': True
            },
        }
    },
    'cap_and_deliver_bottle': {
        'entry_point': 'cap_bottle',
        'tasks': {
            'cap_bottle': {
                'type': 'job',
                'result': {
                    'K': 'send_order_complete_status',
                    'N': 'send_order_complete_status'
                },
            },
            'deliver_bottle': {
                'type': 'command',
                'result': {
                    'K': 'move_slider_to_drop_off',
                },
            },
            'send_order_complete_status': {
                'type': 'client_message',
                'result': {
                    'K': 'deliver_bottle',
                },
                'status': 'complete',
                'message': {
                    'topic': 'order_status',
                    'status': 'order_complete',
                    'data': 'Order complete',
                    'order_type': 'drop_bottle'
                }
            },
            'move_slider_to_drop_off': {
                'type': 'command',
                'result': {
                    'K': 'open_and_close_door',
                },
            },
            'open_and_close_door': {
                'type': 'parallel_job',
                'result': {
                    'K': 'post_inventory_data',
                },
            },
            'post_inventory_data': {
                'type': 'task',
                'result': {
                    'K': 'retrieve_close_and_uncap_next_bottle',
                },
            },
            'retrieve_close_and_uncap_next_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
            },
        }
    },
    'open_and_close_door': {
        'entry_point': 'open_door',
        'tasks': {
            'open_door': {
                'type': 'command',
                'result': {
                    'K': 'send_door_open_message',
                },
            },
            'send_door_open_message': {
                'type': 'client_message',
                'result': {
                    'K': 'close_door',
                },
                'status': 'complete',
                'message': {
                    'topic': 'order_status',
                    'status': 'door_open',
                    'data': 'Door Open',
                    'order_type': 'drop_bottle'
                }
            },
            'close_door': {
                'type': 'command',
                'result': {
                    'K': 'send_door_closed_message',
                },
            },
            'send_door_closed_message': {
                'type': 'client_message',
                'result': {
                    'K': 'retrieve_close_and_uncap_next_bottle',
                },
                'status': 'complete',
                'message': {
                    'topic': 'order_status',
                    'status': 'door_closed',
                    'data': 'Door closed',
                    'order_type': 'drop_bottle'
                }
            },
        }
    },
    'cap_bottle': {
        'entry_point': 'move_to_capper_for_capping',
        'num_retries': 5,
        'tasks': {
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'place_cap'
                }
            },
            'place_cap': {
                'type': 'sync_command',
                'result': {
                    'K': 'check_for_cap',
                },
                'sync_commands': ['cap_bottle', 'move_for_cap'],
                'num_responses': 2,
                'group_name': 'gantry_deck'
            },
            'cap_bottle': {
                'type': 'command',
                'result': {
                    'K': 'complete'
                },
                'read_response': True
            },
            'move_for_cap': {
                'type': 'command',
                'result': {
                    'K': 'complete'
                },
                'read_response': True
            },
            'check_for_cap': {
                'type': 'job',
                'result': {
                    'C': 'retry',
                    'N': 'complete',
                    'K': 'complete'
                },
                'status': 'complete',
            },
        }
    },
    'check_for_cap': {
        'entry_point': 'move_to_neutral_position',
        'tasks': {
            'move_to_neutral_position': {
                'type': 'command',
                'result': {
                    'K': 'cap_check',
                },
            },
            'cap_check': {
                'type': 'command',
                'result': {
                    'N': 'complete',
                    'K': 'complete'
                },
                'status': 'complete'
            },
        }
    },
}
