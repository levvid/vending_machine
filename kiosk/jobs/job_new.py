import datetime


class Operation:
    def __init__(self, name, jobs, entry_point):
        self.start_time = datetime.datetime.now()
        self.name = name
        self.end_time = None
        self.status = 'QUEUED'
        self.jobs = jobs
        self.current_job = entry_point

    def set_end_time(self):
        self.end_time = datetime.datetime.now()

    def set_status(self, new_status):
        self.status = new_status

    def set_current_task(self, new_task):
        self.current_task = new_task

    def complete_job(self):
        self.current_task = 'idle'
        self.status = 'complete'
        self.end_time = datetime.datetime.now()

    def get_current_task(self):
        return self.tasks[self.current_task]

    @property
    def info(self):
        return ' Job name: {}\n' \
               'Time Started: {}\n' \
               'Time Ended: {}\n' \
               'Tasks: {}'.format(self.name, self.start_time, self.end_time, self.tasks)
