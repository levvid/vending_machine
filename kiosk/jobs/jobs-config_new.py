operations = {
    'start_up': {
        'entry_point': 'register_modules',
        'jobs': {
            'register_modules': {
                'type': 'task',
                'result': {
                    'K': 'check_if_holding_bottle',
                    'E': 'throw_error'
                },
            },
            'check_if_holding_bottle': {
                'type': 'command',
                'command': 'check_if_holding_bottle',
                'result': {
                    # 'K': 'check_for_cap',
                    'K': 'retrieve_close_and_uncap_next_bottle',  # TEST
                    'N': 'retrieve_close_and_uncap_next_bottle',
                    'E': 'throw_error'
                },
            },
            'check_for_cap': {
                'type': 'operation',
                'result': {
                    'C': 'move_to_capper_for_capping',  # cap present, mv to capper for uncapping
                    'N': 'un_cap_and_probe_bottle',
                    'E': 'throw_error',
                    'K': 'move_to_capper_for_capping',  # only for testing purposes
                },
            },
            'un_cap_and_probe_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
            },
            'retrieve_close_and_uncap_next_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
            },
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
        },
    },
    'check_for_cap': {
        'entry_point': 'move_to_neutral_position',
        'tasks': {
            'move_to_neutral_position': {
                'type': 'command',
                'result': {
                    'K': 'cap_check',
                },
            },
            'cap_check': {
                'type': ' ',
                'result': {
                    'C': 'complete',
                },
                'status': 'complete'
            },
        }
    },
    'un_cap_and_probe_bottle': {
        'entry_point': 'turn_on_uv_light',
        'tasks': {
            'turn_on_uv_light': {
                'type': 'command',
                'result': {
                    'K': 'uncap_bottle',
                },
            },
            'uncap_bottle': {
                'type': 'job',
                'result': {
                    'K': 'probe_bottle',
                }
            },
            'probe_bottle': {
                'type': 'command',
                'result': {
                    'K': 'move_to_capper_for_capping',
                }
            },
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'turn_off_uv_light',
                }
            },
            'turn_off_uv_light': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }
    },
    'retrieve_close_and_uncap_next_bottle': {
        'entry_point': 'retrieve_next_bottle',
        'tasks': {
            'retrieve_next_bottle': {
                'type': 'job',
                'result': {
                    'K': 'double_bottle_check',
                }
            },
            'double_bottle_check': {
                'type': 'command',
                'result': {
                    'D': 'garbage_bottle',
                    'S': 'flap_seal_bottle',
                    'K': 'flap_seal_bottle'  # TEST
                }
            },
            'garbage_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
            'flap_seal_bottle': {
                'type': 'job',
                'result': {
                    'K': 'un_cap_and_probe_bottle',
                }
            }
        }, 'un_cap_and_probe_bottle': {
            'type': 'job',
            'result': {
                'K': 'complete',
            },
            'status': 'complete'
        }
    },
    'retrieve_next_bottle': {
        'entry_point': 'gantry_move_to_tower',
        'num_retries': 5,
        'tasks': {
            'gantry_move_to_tower': {
                'type': 'command',
                'result': {
                    'K': 'raise_bottle_to_gantry',
                }
            },
            'raise_bottle_to_gantry': {
                'type': 'sync_command',
                'result': {
                    'K': 'stop_tower',
                },
                'sync_commands': ['raise_tower_level', 'wait_for_tower'],
                'num_responses': 1
            },
            'raise_tower_level': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                }
            },
            'wait_for_tower': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                }
            },
            'stop_tower': {
                'type': 'command',
                'result': {
                    'K': 'grab_bottle',
                }
            },
            'grab_bottle': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                    'N': 'retry'
                }
            },
        }
    },
    'garbage_bottle': {
        'entry_point': 'move_slider_to_drop_off',
        'tasks': {
            'move_slider_to_drop_off': {
                'type': 'command',
                'result': {
                    'K': 'release_bottle',
                }
            },
            'release_bottle': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }
    },
    'flap_seal_bottle': {
        'entry_point': 'flap_seal_one',
        'tasks': {
            'flap_seal_one': {
                'type': 'command',
                'result': {
                    'K': 'flap_seal_two',
                },
            },
            'flap_seal_two': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }

    },
    'uncap_bottle': {
        'entry_point': 'move_to_capper_for_uncapping',
        'num_retries': 5,
        'tasks': {
            'move_to_capper_for_uncapping': {
                'type': 'command',
                'result': {
                    'K': 'remove_cap'
                }
            },
            'remove_cap': {
                'type': 'sync_command',
                'result': {
                    'K': 'check_for_cap',
                },
                'sync_commands': ['uncap_bottle', 'move_for_uncap'],
                'num_responses': 2
            },
            'uncap_bottle': {
                'type': 'command',
                'result': {
                    'K': 'check_for_cap'
                }
            },
            'move_for_uncap': {
                'type': 'command',
                'result': {
                    'K': 'check_for_cap'
                }
            },
            'check_for_cap': {
                'type': 'job',
                'result': {
                    'C': 'move_to_capper_for_capping',
                    'K': 'move_to_capper_for_capping',
                    'N': 'retry'
                }
            },
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'C': 'move_to_capper_for_capping',
                    'K': 'complete',
                },
                'status': 'complete'
            }
        }
    },
    'order': {
        'entry_point': 'preauthorize_payment',
        'tasks': {
            'preauthorize_payment': {
                'type': 'task',
                'result': {
                    'K': 'make_drink',
                },
            },
            'make_drink': {
                'type': 'job',
                'result': {
                    'K': 'post_order_complete',
                },
            },
            'post_order_complete': {  # update inventory, save order info, settle payment etc...
                'type': 'task',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
        }
    },
    'make_drink': {
        'entry_point': 'order_type',
        'tasks': {
            'order_type': {
                'type': 'task',
                'result': {
                    'refill': 'make_refill_drink',
                    'drop_bottle': 'make_drop_drink'
                },
            },
            'make_refill_drink': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
            'make_drop_drink': {  # update inventory, save order info, settle payment etc...
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
                'status': 'complete'
            },
        }
    },
    'make_drop_drink': {
        'entry_point': 'move_to_filler',
        'tasks': {
            'move_to_filler': {
                'type': 'command',
                'result': {
                    'K': 'set_additives_to_compostable',
                },
            },
            'set_additives_to_compostable': {
                'type': 'command',
                'result': {
                    'K': 'move_slider_to_garbage',
                },
            },
            'move_slider_to_garbage': {
                'type': 'command',
                'result': {
                    'K': 'cap_check',
                },
            },
            'cap_check': {
                'type': 'command',
                'result': {
                    'K': 'prepare_for_fill',
                },
            },
            'prepare_for_fill': {
                'type': 'sync_command',
                'result': {
                    'K': 'cap_and_deliver_bottle',
                },
                'sync_commands': ['write_additive_code', 'write_temperature_code'],
                'num_responses': 2
            },
            'write_additive_code': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
            },
            'write_temperature_code': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
            },
        }
    },
    'make_refill_drink': {
        'entry_point': 'set_additives_to_refill',
        'tasks': {
            'set_additives_to_refill': {
                'type': 'command',
                'result': {
                    'K': 'prepare_for_fill',
                },
            },
            'monitor_refill': {
                'type': 'command',
                'result': {
                    'K': 'monitor_refill',
                },
                'frequency': 1000  # seconds
            },
            'prepare_for_fill': {
                'type': 'sync_command',
                'result': {
                    'K': 'monitor_refill',
                },
                'sync_commands': ['write_additive_code', 'write_temperature_code'],
                'num_responses': 2
            },
            'write_additive_code': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
            },
            'write_temperature_code': {
                'type': 'command',
                'result': {
                    'K': 'complete',
                },
            },
        }
    },
    'cap_and_deliver_bottle': {
        'entry_point': 'cap_bottle',
        'tasks': {
            'cap_bottle': {
                'type': 'job',
                'result': {
                    'K': 'deliver_bottle',
                },
            },
            'deliver_bottle': {
                'type': 'command',
                'result': {
                    'K': 'move_slider_to_drop_off',
                },
            },
            'move_slider_to_drop_off': {
                'type': 'command',
                'result': {
                    'K': 'open_door',
                },
            },
            'open_door': {
                'type': 'command',
                'result': {
                    'K': 'close_door',
                },
            },
            'close_door': {
                'type': 'command',
                'result': {
                    'K': 'retrieve_close_and_uncap_next_bottle',
                },
            },
            'retrieve_close_and_uncap_next_bottle': {
                'type': 'job',
                'result': {
                    'K': 'complete',
                },
            },
        }
    },
    'cap_bottle': {
        'entry_point': 'move_to_capper_for_capping',
        'num_retries': 5,
        'tasks': {
            'move_to_capper_for_capping': {
                'type': 'command',
                'result': {
                    'K': 'place_cap'
                }
            },
            'place_cap': {
                'type': 'sync_command',
                'result': {
                    'K': 'check_for_cap',
                },
                'sync_commands': ['cap_bottle', 'move_for_cap'],
                'num_responses': 2
            },
            'cap_bottle': {
                'type': 'command',
                'result': {
                    'K': 'complete'
                }
            },
            'move_for_cap': {
                'type': 'command',
                'result': {
                    'K': 'complete'
                }
            },
            'cap_check': {
                'type': 'job',
                'result': {
                    'C': 'move_to_capper_for_capping',
                    'N': 'retry'
                }
            }
        }
    },
}
