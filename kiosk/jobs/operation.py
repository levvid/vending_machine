import datetime


class Operation:
    def __init__(self, name):
        self.start_time = datetime.datetime.now()
        self.name = name
        self.end_time = None
        self.status = 'QUEUED'

    def set_end_time(self):
        self.end_time = datetime.datetime.now()

    def set_status(self, new_status):
        self.status = new_status

    @property
    def info(self):
        return 'Operation name: {}\n' \
               'Time Started: {}\n' \
               'Tasks: {}'.format(self.name, self.start_time, self.end_time, [task_name for task_name in self.tasks.keys()])
