import json
import logging

from channels.generic.websocket import AsyncWebsocketConsumer

log = logging.getLogger(__name__)


class KioskConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        # self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_name = 'kiosk_controller_room'
        self.room_group_name = 'kiosk_controller_group'
        self.modules = {
            'gantry': {
                'port': '',

            }
        }

        print('Room: {} Group: {}'.format(self.room_name, self.room_group_name))
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'kiosk_message',
                'message': message
            }
        )

        if message == 'G':
            await self.channel_layer.send(
                "gantry",
                {
                    "type": 'connect_serial',
                    "port": '/dev/pts/7',
                },
            )
        elif message == 'D':
            await self.channel_layer.send(
                "deck",
                {
                    "type": 'connect_serial',
                    "port": '/dev/pts/8',
                },
            )

        elif message == 'read' or message == 'writer':
            await self.channel_layer.send(
                "gantry",
                {
                    "type": message,
                    "command": message,
                },
            )
        else:
            await self.channel_layer.send(
                "deck",
                {
                    "type": message,
                    "command": message,
                },
            )

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'kiosk_message2',
                'message': 'Message: {}'.format(message)
            }
        )

    # Receive message from room group
    async def kiosk_message(self, event):
        message = event['message']


        print('Received message')
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))

    # Receive message from room group
    async def module_status(self, event):
        status = event['status']
        type = event['type']
        module_name = event['module_name']

        if type == 'CONNECTED':
            self.modules[module_name] = module_name



        print('Received message')
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': event
        }))

    async def module_response(self, event):
        response = event['response']



        print('Received re')
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': response
        }))

    async def kiosk_message2(self, event):
        message = event['message']
        print("Message received in 2: {}".format(message))
