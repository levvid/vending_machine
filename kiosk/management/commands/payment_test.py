from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from django.utils import timezone


class Command(BaseCommand):

    # def add_arguments(self, parser):
    #     parser.add_argument('total', type=int, help='Indicates the number of users to be created')
    #     parser.add_argument('-p', '--prefix', type=str, help='Define a username prefix')
    #     parser.add_argument('-a', '--admin', action='store_true', help='Create an admin account')
    #     pass

    def handle(self, *args, **kwargs):
        # total = kwargs['total']
        # prefix = kwargs['prefix']
        # admin = kwargs['admin']
        #
        #
        time = timezone.now().strftime('%X')
        self.stdout.write("It's now %s" % time)

