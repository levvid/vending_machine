# basic imports
import asyncio as aio
import logging

# Since this is now a python module please copy this example file to the
# AIOSerial parent directory before executing (cp Example.py ../Example.py)

# all the asyncio-serial port related stuff
from kiosk.libs.AIOSerial import (AIOSerial, AIOSerialClosedException,
    AIOSerialErrorException, AIOSerialNotOpenException)

# AIOSerial now logs!
logging.basicConfig(level=logging.DEBUG)

# port usage example
async def write_and_read(port, command):
    # try to open the serial port and do some basic communication
    try:
        # open the port with following settings, use the line mode (return full
        #  text lines from the read method)
        async with AIOSerial(port, baudrate=115200, line_mode=True) as aios:
            # this is the way of accessing the serial port internals directly if
            # you are in need of altering the baudrate, etc..
            # aios.sp.baudrate = 230400
            print('Writing to serial: {}'.format(command))
            # send some data, here we use the AT protocol as it is one of the
            # most popular ways for the communication over the serial port
            bytes_command = command.encode('utf-8')
            await aios.write(bytes_command)

            # uncomment this to get the AIOSerialClosedException
            #
            # await aios.close()

            # if you use virtual com port (like in case of usb-usart dongles)
            # you can test the AIOSerialErrorException by disconnecting the
            # dongle during following sleep period. You will still get all the
            # data that was received before the disconnection, but you won't be
            # able to send any (obviously)
            #
            # await aio.sleep(10)

            # read may fail if the peer device does not understand the AT
            # command
            try:
                while True:
                    # read with timeout
                    rcvd = await aio.wait_for(aios.read(), timeout=2.0)
                    # print the data received
                    print(f"data received: {rcvd}")

                    # if len(rcvd) > 0:
                    return rcvd
            # read timeout
            except aio.TimeoutError:
                print("reception timed out ;-(")
    # unable to open port
    except AIOSerialNotOpenException:
        print("Unable to open the port!")
    # port fatal error
    except AIOSerialErrorException:
        print("Port error!")
    # port already closed
    except AIOSerialClosedException:
        print("Serial port is closed!")


async def write_from_dict(data):
    command = data['command']
    # logger.info('Received command {}'.format(command))
    command_to_write = command['command']

    port = data['port']
    print('Returning.........==========')


    return await write_and_read(port, command_to_write)


# async def read_and_print(aioserial_instance: aioserial.AioSerial):
#     while True:
#         data: bytes = await aioserial_instance.read_async()
#         print(data.decode(errors='ignore'), end='', flush=True)
#         if b'\r\n' in data:
#             aioserial_instance.close()
#             return data
#             # break
#
# async def


