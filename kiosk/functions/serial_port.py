import asyncio
import concurrent
import re
import time

import serial


def is_all_upper_case(s):
    return not any(c.islower() for c in s)


def get_uppercase_responses(response):
    return re.findall(r'[A-Z]', response)


def get_ml_dispensed(response):
    return re.findall(r'[0-9]+', response)


def write_to_module(data, port, read_response=True, monitor_refill=False):
    command = data['command']
    # print('Received command {}'.format(command))
    command_to_write = command['command']

    module_name = command['module']

    port = data['port']

    # print('Received command {}'.format(command_to_write))
    module = serial.Serial(port, baudrate=9600, timeout=1, writeTimeout=1)

    bytes_command = command_to_write.encode('utf-8')

    try:
        #  Write to serial
        module.write(bytes_command)
    except serial.serialutil.SerialTimeoutException:
        print('Failed to write command {} to module {}.'.format(command, module_name))
        return 'E'
    print('Command {} written to module {}.'.format(command, module_name))

    start_time = time.time()

    if not read_response:
        return 'K'

    bytes_to_read = module.inWaiting()
    module_response = ''
    while module_response == '':
        while bytes_to_read == 0:
            # time.sleep(.3)
            end_time = time.time()
            time_elapsed = end_time - start_time
            bytes_to_read = module.inWaiting()
            if time_elapsed > 30:
                print(
                    '{} timed out while trying to read serial port {}. Seconds elapsed: {}'.format(module_name,
                                                                                                   port,
                                                                                                   time_elapsed))
                module_response = 'E'
                break
        if module_response != 'E':
            module_response = str(module.read(bytes_to_read), 'utf-8')

        uppercase_response = get_uppercase_responses(module_response)

        ml_dispensed = 0
        if monitor_refill:
            ml_dispensed = get_ml_dispensed(module_response)
            print('ml dispensed: {}'.format(ml_dispensed))

        if len(module_response.strip()) > 0:
            print('{} response.'.format(module_response.strip()))

        if len(uppercase_response) == 1:
            print(
                '{0} responded with {1} in {2:.2f} seconds.'.format(module_name, uppercase_response[0],
                                                                    time.time() - start_time))
            if monitor_refill:
                return ml_dispensed, uppercase_response[0]
            return uppercase_response[0]
        else:
            module_response = ''


################################################################


def get_and_print(module, command, module_name, port, read_response=True):
    test = yield from get_byte_async(module, logger, command, module_name, port, read_response)


# Runs blocking function in executor, yielding the result
@asyncio.coroutine
def get_byte_async(module, logger, command, module_name, port, read_response=True):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(get_and_print())
    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        res = yield from loop.run_in_executor(executor, write_to_serial(module, logger, command, module_name, port,
                                                                        read_response))
        return res


def write_to_serial(module, command, module_name, port, read_response=True):
    print('Received command {}'.format(command))
    bytes_command = command.encode('utf-8')
    try:
        #  Write to serial
        module.write(bytes_command)
    except serial.serialutil.SerialTimeoutException:
        return 'E'
    print('Command {} written to module {}.'.format(command, module_name))

    command_response = 'K'
    start_time = time.time()

    if read_response:
        bytes_to_read = module.inWaiting()
        command_response = ''
        while command_response == '':
            while bytes_to_read == 0:
                # time.sleep(.3)
                end_time = time.time()
                time_elapsed = end_time - start_time
                bytes_to_read = module.inWaiting()
                if time_elapsed > 30:
                    print(
                        '{} timed out while trying to read serial port {}. Seconds elapsed: {}'.format(module_name,
                                                                                                       port,
                                                                                                       time_elapsed))
                    command_response = 'E'
                    break
            if command_response != 'E':
                command_response = str(module.read(bytes_to_read), 'utf-8')
            print('Response: {}'.format(command_response))
            split_response = command_response.split('\r\n')
            print('Split response: {}'.format(split_response))

            for res in split_response:
                if not res.isdigit() and is_all_upper_case(res) and len(res) > 0:
                    command_response = res
                else:
                    print('response {} is a digit.'.format(res))
                    command_response = ''
            print('Final response: {}'.format(command_response))
        print('{0} responded with {1} in {2:.2f} seconds.'.format(module_name, command_response,
                                                                  time.time() - start_time))

    # if len(command_response) > 1:  # TEST, only take the first char. For some reason it reads twice on 2nd order
    #     command_response = command_response[0]

    return command_response


async def write_to_serial_async(logger, data, port, read_response=True):
    command = data['command']
    # print('Received command {}'.format(command))
    command_to_write = command['command']

    module_name = command['module']

    port = data['port']

    print('Received command {}'.format(command_to_write))
    module = serial.Serial(port, baudrate=9600, timeout=1, writeTimeout=1)

    bytes_command = command_to_write.encode('utf-8')

    try:
        #  Write to serial
        module.write(bytes_command)
    except serial.serialutil.SerialTimeoutException:
        return 'E'
    print('Command {} written to module {}.'.format(command, module_name))

    start_time = time.time()

    if not read_response:
        return 'K'

    bytes_to_read = module.inWaiting()
    module_response = ''
    while module_response == '':
        while bytes_to_read == 0:
            # time.sleep(.3)
            end_time = time.time()
            time_elapsed = end_time - start_time
            bytes_to_read = module.inWaiting()
            if time_elapsed > 30:
                print(
                    '{} timed out while trying to read serial port {}. Seconds elapsed: {}'.format(module_name,
                                                                                                   port,
                                                                                                   time_elapsed))
                module_response = 'E'
                break
        if module_response != 'E':
            module_response = str(module.read(bytes_to_read), 'utf-8')
        print('Response: {}'.format(module_response))
        split_response = module_response.split('\r\n')
        print('Split response: {}'.format(split_response))

        for res in split_response:
            print('Checking {}'.format(res))

            if not res.isdigit() and is_all_upper_case(res) and len(res) > 0:
                print('Passed')
                command_response = res
                module_response = res
                print('Responding with: {}'.format(res))
                return res
            elif res == '0000':
                return 'K'
            else:
                module_response = ''
                print("Failed ==================")

                print(
                    '{0} responded with {1} in {2:.2f} seconds.'.format(module_name, res, time.time() - start_time))

    # if len(command_response) > 1:  # TEST, only take the first char. For some reason it reads twice on 2nd order
    #     command_response = command_response[0]

    # return command_response
