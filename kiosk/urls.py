# kiosk/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('', views.kiosk_client, name='kiosk-client'),
    path('<str:room_name>/', views.room, name='room'),
    path('start_up/', views.StartUpAPIView, name='api-start-job'),
    path('kiosk_client/other', views.kiosk_client, name='kiosk-client2'),
]
