import sys
import glob
import serial


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    print('List serial ports called.')
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
         # kiosk
        print('linux/cygwin')
        ports = glob.glob('/dev/ttyAC[A-Za-z]*')
        print('Ports found {}'.format(ports))
    elif sys.platform.startswith('darwin'):
        print('Darwin')
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    print('Returning result. {}'.format(result))
    return result


if __name__ == '__main__':
    ports = serial_ports()

