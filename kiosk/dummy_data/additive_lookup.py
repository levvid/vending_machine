additives = [
    {
        "name": "mint",
        "type": "flavor",
        "levels": "1",
        "concentration": 4,
        "color": "G"
    },
    {
        "name": "cucumber",
        "type": "flavor",
        "levels": "1",
        "concentration": 1.2,
        "color": "G"
    },
    {
        "name": "lemonade",
        "type": "flavor",
        "levels": "1",
        "concentration": 4,
        "color": "Y"
    },
    {
        "name": "guava",
        "type": "flavor",
        "levels": "1",
        "concentration": 4,
        "color": "R"
    },
    {
        "name": "cusaMango",
        "type": "flavor",
        "levels": "1",
        "concentration": 4,
        "color": "B"
    },
    {
        "name": "cusaLemon",
        "type": "flavor",
        "levels": "1",
        "concentration": 4,
        "color": "B"
    },
    {
        "name": "water",
        "type": "flavor",
        "levels": "1",
        "concentration": 0,
        "color": "B"
    },
    {
        "name": "caffeine",
        "type": "supplement",
        "levels": 3,
        "concentration": 4,
        "color": "B"
    }
]

additive_dispense_rate = [
  {
    "name": "mint",
    "mlPerNum": 0.092
  },
  {
    "name": "cucumber",
    "mlPerNum": 0.1
  },
  {
    "name": "lime",
    "mlPerNum": 0.1025
  },
  {
    "name": "lemonade",
    "mlPerNum": 0.1025
  },
  {
    "name": "guava",
    "mlPerNum": 0.1025
  },
  {
    "name": "melon",
    "mlPerNum": 0.088
  },
  {
    "name": "mango",
    "mlPerNum": 0.1
  },
  {
    "name": "cusaMango",
    "mlPerNum": 0.1
  },
  {
    "name": "cusaLemon",
    "mlPerNum": 0.1
  },
  {
    "name": "caffeine",
    "mlPerNum": 0.1
  }
]
