import json
import socket
import os
import django
import uuid

HOSTNAME = socket.gethostname()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pegasus.settings')

django.setup()

from kiosk.models import Inventory, Order, DrinkPrice, ModuleStatus, Kiosk, Additives
from django.db import transaction


ADDITIVES = [
    {
        'name': 'cucumber',
        'type': 'flavor',
        'levels': 1,
        'concentration': 1.2,
        'color': 'G',
        'amount_remaining': 500,
        'pump_number': 1,
        'dispense_rate': 0.1,
    }, {
        'name': 'lemonade',
        'type': 'flavor',
        'levels': 1,
        'concentration': 4.0,
        'color': 'Y',
        'amount_remaining': 500,
        'pump_number': 2,
        'dispense_rate': 0.1025,
    }, {
        'name': 'guava',
        'type': 'flavor',
        'levels': 1,
        'concentration': 4.0,
        'color': 'R',
        'amount_remaining': 500,
        'pump_number': 3,
        'dispense_rate': 0.1025,
    }, {
        'name': 'water',
        'type': 'water',
        'levels': 1,
        'concentration': 0,
        'color': 'B',
        'amount_remaining': 0,
        'pump_number': 0,
        'dispense_rate': 0,
    },
]

SUPPLEMENTS = [
    {
        'name': 'caffeine',
        'type': 'supplement',
        'levels': 3,
        'concentration': 1.2,
        'color': 'B',
        'amount_remaining': 500,
        'pump_number': 6,
        'dispense_rate': 0.1,
    },
]


def load_json_file(file_name):
    with open(file_name) as json_file:
        data = json.load(json_file)
        return data


def create_dummy_orders(num_orders):
    current_order = 'Drop Bottle'
    pass


def create_dummy_inventory():
    print('create_dummy_inventory')
    Inventory.objects.create(
        tower_one_remaining=48,
        tower_two_remaining=48,
        tower_three_remaining=48,
        selected_tower='1',
        sync_status=False,
    )
    print('create_dummy_inventory done')


def create_dummy_additives():
    print('create_dummy_additives start')
    count = 1
    for additive in SUPPLEMENTS:
        Additives.objects.create(
            name=additive['name'],
            type=additive['type'],
            levels=additive['levels'],
            concentration=additive['concentration'],
            color=additive['color'],
            amount_remaining=additive['amount_remaining'],
            pump_number=additive['pump_number'],
            dispense_rate=additive['dispense_rate']
        )

        count += 1
    print('create_dummy_additives done')


def create_dummy_drink_price():
    print('create_dummy_drink_price')
    DrinkPrice.objects.create(
        drop_bottle_just_water=250,
        drop_bottle_flavor=100,
        drop_bottle_supplement=100,
        refill_just_water=100,
        refill_flavor=100,
        refill_supplement=100,
    )
    print('create_dummy_drink_price done')


def create_dummy_module_status():
    print('create_dummy_module_status')
    ModuleStatus.objects.create(
        drain_tank_status='Okay',
        drop_bottle_status='On',
        refill_status='On',
        uv_filter_status='Okay',
    )

    print('create_dummy_module_status done')


def create_dummy_kiosk():
    print('create_dummy_kiosk')
    Kiosk.objects.create(
        kiosk_id=HOSTNAME,
        nickname='pegasus',
        losant_id=uuid.uuid4(),
        location='Drop Water HQ',
        location_alias='HQ',
        company_id=uuid.uuid4(),
        alias='pegasus'

    )

    print('create_dummy_kiosk done')

@transaction.atomic
def dummy_data():
    create_dummy_inventory()
    create_dummy_drink_price()
    create_dummy_module_status()
    create_dummy_kiosk()
    create_dummy_additives()
    print('main done')


if __name__ == '__main__':
    dummy_data()
