# kiosk.view.py
from django.db.models import Sum

from pegasus.celery import start_job, refill_order, drop_bottle_order, start_operation, dispense, module_logger, \
    run_maintenance_task
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response

from kiosk.models import Inventory, DrinkPrice, ModuleStatus, Additives, Kiosk, Order, Operations
from kiosk.serializers import InventorySerializer, AdditivesSerializer, DrinkPriceSerializer, ModuleStatusSerializer, \
    KioskSerializer, OrderSerializer, OperationsSerializer, TasksSerializer, MaintenanceTasksSerializer
# Create your views here.
from django.shortcuts import render
import os
import json
from rest_framework.views import APIView
from rest_framework import status

from rest_framework import generics

OPERATIONS = {
    'Reset': 'start_up',
    'Make Drop Drink': 'make_drop_drink',
    'Make Refill Drink': 'make_refill_drink',
    'Check For Cap': 'check_for_cap',
    'Uncap And Probe Bottle': 'uncap_and_probe_bottle',
    'Retrieve And Stage Next Bottle': 'retrieve_close_and_uncap_next_bottle',
    'Retrieve Next Bottle': 'retrieve_next_bottle',
    'Garbage Bottle': 'garbage_bottle',
    'Flap Seal Bottle': 'flap_seal_bottle',
    'Cap, Deliver And Stage Next Bottle': 'cap_and_deliver_bottle'
}

TASKS = {
    'Start Dispense': 'start_dispense',
    'End Dispense': 'end_dispense',
    'End Refill Order': 'end_order'
}


class InventoryListApiView(generics.ListAPIView):
    """
    Returns a list of this kiosk's inventory.
    """
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer


class InventoryUpdateApiView(generics.RetrieveUpdateAPIView):
    queryset = Inventory.objects.all().first()
    serializer_class = InventorySerializer

    def get_object(self):
        obj = Inventory.objects.all().first()
        self.check_object_permissions(self.request, obj)
        return obj


class AdditivesApiView(generics.ListAPIView):
    """
    Returns a list of flavors on this kiosk
    """
    queryset = Additives.objects.all()
    serializer_class = AdditivesSerializer


class AdditivesUpdateView(generics.RetrieveUpdateAPIView):
    queryset = Additives.objects.all()
    serializer_class = AdditivesSerializer

    # def get_object(self):
    #     obj = Additives.objects.all().first()
    #     self.check_object_permissions(self.request, obj)
    #     return obj


class DrinkPriceApiView(generics.ListAPIView):
    """
    Returns a list of drink prices on this kiosk
    """
    queryset = DrinkPrice.objects.all()
    serializer_class = DrinkPriceSerializer


class DrinkPriceUpdateApiView(generics.RetrieveUpdateAPIView):
    queryset = DrinkPrice.objects.all().first()
    serializer_class = DrinkPriceSerializer

    def get_object(self):
        obj = DrinkPrice.objects.all().first()
        self.check_object_permissions(self.request, obj)
        return obj


class ModuleStatusApiView(generics.ListAPIView):
    """
    Returns a list of this kiosk's module statuses
    """
    queryset = ModuleStatus.objects.all()
    serializer_class = ModuleStatusSerializer


class KioskApiView(generics.ListAPIView):
    """
    Returns details about the host machine/kiosk.
    """
    queryset = Kiosk.objects.all()
    serializer_class = KioskSerializer


class OrderApiView(generics.ListAPIView):
    """
    Returns a list of active orders sorted by date (newest first)
    """
    queryset = Order.objects.all().order_by('-date_created')
    serializer_class = OrderSerializer


class OrderCountApiView(APIView):
    """
    Returns a list of active orders sorted by date (newest first)
    """

    # renderer_classes = (JSONRenderer,)

    def get(self, request, format=None):
        order_count = Order.objects.count()

        total_water_amount = Order.objects.aggregate(Sum('water_amount'))  # only count when water amount is more than 0

        num_bottles_saved = 0
        if total_water_amount > 0:
            num_bottles_saved = int(total_water_amount / 16)
        content = {'num_bottles_saved': num_bottles_saved }
        return Response(content)


class OperationsApiView(generics.ListAPIView):
    """
    Used to start operations from the operations list drop down
    """
    queryset = Operations.objects.all()
    serializer_class = OperationsSerializer


class OperationsCreateApiView(generics.CreateAPIView):
    serializer_class = OperationsSerializer

    def create(self, request, *args, **kwargs):
        # create.delay(request)
        print('create request: {}'.format(request))

    def post(self, request, *args, **kwargs):
        print('Operations post request.')

        operation_name = OPERATIONS[request.data['operation_name']]

        if operation_name == 'make_drop_drink' or operation_name == 'make_refill_drink':
            operation_type = 'order'
        else:
            operation_type = 'operation'

        start_operation.delay(operation_name, operation_type)

        return Response('{} {} queued.'.format(operation_type, operation_name), status=status.HTTP_200_OK)


class TasksCreateApiView(generics.CreateAPIView):
    serializer_class = TasksSerializer

    def create(self, request, *args, **kwargs):
        # create.delay(request)
        print('create request: {}'.format(request))

    def post(self, request, *args, **kwargs):
        print('Operations post request.')
        task_name = request.data['task_name']
        module_logger.delay('debug', 'Views', 'Task API request data: {}'.format(request.data))

        print('Task received. {}'.format(task_name))

        if task_name == 'Start Dispense' or task_name == 'End Dispense' or task_name == 'End Refill Order':
            refill_commands = {
                'Start Dispense': 'start_dispense',
                'End Dispense': 'end_dispense',
                'End Refill Order': 'end_order'
            }
            dispense.delay(refill_commands[task_name])

        return Response('{} queued.'.format(task_name), status=status.HTTP_200_OK)


class MaintenanceTasksApiView(generics.CreateAPIView):
    serializer_class = MaintenanceTasksSerializer

    def create(self, request, *args, **kwargs):
        # create.delay(request)
        print('maintenance create request: {}'.format(request))

    def post(self, request, *args, **kwargs):
        print('maintenance post request.')
        task_name = request.data['task_name']
        module_logger.delay('debug', 'Views', 'maintenance API request data: {}'.format(request.data))

        print('{} maintenance task queued.'.format(task_name))

        run_maintenance_task.delay(task_name)

        return Response('{} queued.'.format(task_name), status=status.HTTP_200_OK)


class OrderCreateApiView(generics.CreateAPIView):
    """
    Used to create an order
    """
    serializer_class = OrderSerializer

    def create(self, request, *args, **kwargs):
        # create.delay(request)
        print('create request: {}'.format(request))

    def post(self, request, *args, **kwargs):
        print('order post request.')

        order_type = request.data['order_type']
        print('received API request: {}'.format(request.data))

        if order_type == 'Drop Bottle':
            print('Drop Bottle: {}'.format(order_type))
            drop_bottle_order.delay(request.data)
        else:
            print('Refill: {}'.format(order_type))
            refill_order.delay(request.data)
        return Response('{} queued.'.format(order_type), status=status.HTTP_200_OK)


def load_json_file(file_name):
    script_dir = os.path.dirname(__file__)
    abs_file_path = os.path.join(script_dir, file_name)
    with open(abs_file_path) as json_file:
        data = json.load(json_file)
        return data


def index(request):
    print('Loading this....')
    return render(request, 'kiosk/index.html')


def room(request, room_name):
    return render(request, 'kiosk/room.html', {
        'room_name': room_name
    })


def kiosk_client(request):
    return render(request, 'kiosk/kiosk_client.html')


def StartUpAPIView(request):
    print('start_up api called. ')
    ret = start_operation.delay('start_up', '')
    print('Start job result. {}'.format(ret.result))
    return JsonResponse({'start_up': 'queued'})


def ModuleStatusAPIView(request):
    print('order api called. ')
    # ret = start_job.delay()
    # print('Start job result. {}'.format(ret.result))
    data = load_json_file('dummy_data/module-status.json')
    return JsonResponse(data)


def QRCodeAPIView(request):
    print('order api called. ')
    # ret = start_job.delay()
    # print('Start job result. {}'.format(ret.result))
    return JsonResponse({'QRCodeAPIView': 'QRCode'})

# getPrice - getPrice
# getFlavors - /flavors
# getModuleStatus
# getQRCode
