import logging
import os
import pty
import sys
import threading
import time

MODULES = {
    'G': 'Gantry',
    'L': 'Lighting',
    'A': 'Additive',
    'T': 'Tower',
    'R': 'Reception',
    'D': 'Deck',
    'B': 'Refill'
}

logger = logging.getLogger('SerialPortSimulator')


def serial_simulator(port, module_name):
    while True:
        res = b""
        # while not res.endswith(b'\r\n'):
        while res == b"":
            # read the response
            res += os.read(port, 1000)
        print('{} module read command: {}'.format(MODULES[module_name], res.decode("utf-8")))

        decoded_res = res.decode("utf-8")

        if decoded_res == '!' or decoded_res == '?':
            time.sleep(2)  # for testing purposes to test timeouts
            response = module_name
        else:
            response = 'K'
            # time.sleep(1000) # test delays
        print('{} module responding with: {}'.format(MODULES[module_name], response))
        # response = '{}\r\n'.format(response)

        # if res.decode("utf-8") == 'U':
        #     time.sleep(10)

        os.write(port, str.encode(response))


def create_serial_port(module):
    master, slave = pty.openpty()  # open the pseudoterminal
    s_name = os.ttyname(slave)  # translate the slave fd to a filename

    print('Created port {} for module {}'.format(s_name, module))
    # create a separate thread that listens on the master device for commands
    thread = threading.Thread(target=serial_simulator, args=[master, module])
    thread.start()


if __name__ == '__main__':
    n = len(sys.argv)
    print('number of modules started {}'.format(n))

    mod_list = []
    for i in range(1, n):
        mod_list.append(sys.argv[i])

    print('Modules {}'.format(mod_list))

    for mod in mod_list:
        create_serial_port(mod)
