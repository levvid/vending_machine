# This program allows us to send individual commands
# to the individual modules to help recover from errors remotely

import serial

from kiosk.list_serial_ports import serial_ports

MODULES = ['G', 'D', 'L', 'R', 'A', 'T']


def identify_modules():
    modules = []
    ports = serial_ports()

    for portString in ports:
        # portString = "/dev/ttyACM" + str(portCount)
        ser = serial.Serial(portString, baudrate=9600, timeout=5)
        print("sending ? to port " + portString)
        ser.write(b'?')
        print('Waiting for port {} to respond.'.format(portString))
        moduleData = str(ser.read(), 'utf-8')
        modules.append(moduleData)
        print('Module data is {}'.format(moduleData))
        ser.close()

    print('Modules found: {}'.format(modules))

    not_found = [x for x in MODULES if x not in modules]

    print('not found: {}'.format(not_found))

    return not_found


if __name__ == '__main__':
    identify_modules()
