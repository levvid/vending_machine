# This program allows us to send individual commands
# to the individual modules to help recover from errors remotely

import time

import serial


def read_serial_port():
    bytes_to_read = ser.inWaiting()
    while bytes_to_read == 0:
        time.sleep(1)
        bytes_to_read = ser.inWaiting()
    command_response = str(ser.read(bytes_to_read), 'utf-8')

    split_response = command_response.split('\r\n')

    for res in split_response:
        print('Is a number: {}'.format(res.isdigit()))

    print('responses {}'.format(split_response))
    print('module responded with {}'.format(command_response))
    return command_response


def has_capital_response(response_string):
    return any(char.isupper() for char in response_string)


port_string = input("Which port do you want to test?")
print('connecting to port: {}'.format(port_string))
ser = serial.Serial(port_string, baudrate=9600, timeout=1)

command = input("What command would you like to give? ")
ser.write(str.encode(command))
commandResponse = read_serial_port()

print('Responded with {}'.format(commandResponse))
nextCommand = ""
while nextCommand != "exit":
    nextCommand = input("Next command (type exit to quit) ")
    if nextCommand != "exit":
        ser.write(str.encode(nextCommand))
        commandResponse = read_serial_port()
        print('Responded with {}'.format(commandResponse))
ser.close()
