import multiprocessing

import serial

## Change this to match your local settings
# SERIAL_PORT = '/dev/ttyACM0'
SERIAL_BAUDRATE = 115200


class SerialProcess(multiprocessing.Process):

    def __init__(self, input_queue, output_queue, port):
        multiprocessing.Process.__init__(self)
        self.input_queue = input_queue
        self.output_queue = output_queue
        self.port = port
        self.sp = serial.Serial(port, SERIAL_BAUDRATE, timeout=1)

    def close(self):
        self.sp.close()

    def write_serial(self, data):
        self.sp.write(data)
        # time.sleep(1)

    def read_serial(self):
        return self.sp.readline()

    def run(self):

        self.sp.flushInput()

        while True:
            # look for incoming tornado request
            if not self.input_queue.empty():
                data = self.input_queue.get()

                # send it to the serial device
                self.write_serial(data.encode())
                print("writing to serial: {} for port {}".format(data, self.port))

            # look for incoming serial data
            if self.sp.inWaiting() > 0:
                data = self.read_serial()
                print("reading from serial: {} for port {}".format(data, self.port))
                # send it back to the module_manager
                result = {
                    'data': data,
                    'port': self.port
                }
                self.output_queue.put(result)
