import uuid
from decimal import Decimal

from django.db import models

from pegasus.logger_config import get_logger

# Create your models here.

logger = get_logger('ModelFunctions')


def get_temperature_code(temperature):
    code = '0'  # cold
    if temperature == 'room':
        code = '8'

    return code


def get_desired_pump_value(concentration, dispense_rate):
    desired_pump_value = round(concentration / dispense_rate) + 48
    if desired_pump_value > 126:
        # did this because the ASCII table ran out (Marc)
        logger.warning('Tried to command too high of an additive pump value')
        desired_pump_value = 126

    if chr(desired_pump_value) == '?' or chr(desired_pump_value) == 'X':
        desired_pump_value += 1

    return chr(desired_pump_value)


def get_caffeine_pump_value(caffeine_value, dispense_rate):
    desired_pump_value = 48
    if caffeine_value == 0:
        desired_pump_value = 48
    elif caffeine_value == 1:
        desired_pump_value = round(Decimal.from_float(1.68) / dispense_rate)
        desired_pump_value += 48
    elif caffeine_value == 2:
        desired_pump_value = round(Decimal.from_float(3.37) / dispense_rate)
        desired_pump_value += 48
    elif caffeine_value == 3:
        desired_pump_value = round(Decimal.from_float(5.9) / dispense_rate)
        desired_pump_value += 48

    if desired_pump_value > 126:
        desired_pump_value = 126

    return chr(desired_pump_value)


def generate_additive_code(flavor, caffeine_value):
    additive_array = ["0", "0", "0", "0", "0", "0", "0", "0"]

    try:
        additive = Additives.objects.get(name=flavor)
    except: # track this!!
        additive = Additives.objects.get(name='water')
    concentration = additive.concentration
    dispense_rate = additive.dispense_rate
    pump_number = additive.pump_number

    if additive.name == 'water':
        additive_dispense_rate = '0'
    else:
        additive_dispense_rate = get_desired_pump_value(concentration, dispense_rate)

    caffeine = Additives.objects.get(name='caffeine')
    caffeine_dispense_rate = get_caffeine_pump_value(caffeine_value,
                                                     caffeine.dispense_rate)

    caffeine_pump_number = caffeine.pump_number

    additive_array[pump_number - 1] = additive_dispense_rate
    additive_array[caffeine_pump_number - 1] = caffeine_dispense_rate

    return ''.join(additive_array)


class Inventory(models.Model):
    tower_one_remaining = models.PositiveIntegerField(null=True, blank=True)
    tower_two_remaining = models.PositiveIntegerField(null=True, blank=True)
    tower_three_remaining = models.PositiveIntegerField(null=True, blank=True)
    selected_tower = models.CharField(max_length=50, null=True, blank=True)
    tower_selected = models.CharField(max_length=50, null=True, blank=True)
    sync_status = models.BooleanField(default=False)

    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, blank=True)


class Additives(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    type = models.CharField(max_length=100, null=True, blank=True)  # flavor, supplement...
    levels = models.PositiveIntegerField(null=True, blank=True, default=1)  # 1, 2, 3...
    concentration = models.DecimalField(max_digits=20, decimal_places=10, default=4.0)
    color = models.CharField(max_length=2, null=True, blank=True)  # light color command
    amount_remaining = models.DecimalField(max_digits=20, decimal_places=10, default=500.0)
    pump_number = models.PositiveIntegerField(null=True, blank=True, default=1)
    dispense_rate = models.DecimalField(max_digits=20, decimal_places=10, default=0.1)


class DrinkPrice(models.Model):
    drop_bottle_just_water = models.PositiveIntegerField(null=True, blank=True)
    drop_bottle_flavor = models.PositiveIntegerField(null=True, blank=True)
    drop_bottle_supplement = models.PositiveIntegerField(null=True, blank=True)

    refill_just_water = models.PositiveIntegerField(null=True, blank=True)
    refill_flavor = models.PositiveIntegerField(null=True, blank=True)
    refill_supplement = models.PositiveIntegerField(null=True, blank=True)

    sync_status = models.BooleanField(default=False)

    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, blank=True)


class ModuleStatus(models.Model):
    drain_tank_status = models.CharField(max_length=100, null=True, blank=True)
    drop_bottle_status = models.CharField(max_length=100, null=True, blank=True)
    refill_status = models.CharField(max_length=100, null=True, blank=True)
    uv_filter_status = models.CharField(max_length=100, null=True, blank=True)

    sync_status = models.BooleanField(default=False)

    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, blank=True)


class Kiosk(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, blank=True)

    kiosk_id = models.CharField(max_length=100, null=True, blank=True)
    alias = models.CharField(max_length=100, null=True, blank=True)
    company_id = models.UUIDField(default=uuid.uuid4, editable=False)
    location = models.CharField(max_length=200)
    location_alias = models.CharField(max_length=50)
    losant_id = models.CharField(max_length=100, null=True, blank=True)
    nickname = models.CharField(max_length=100, null=True, blank=True)


class KioskDataUse(models.Model):
    kiosk_id = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    day_total = models.PositiveIntegerField(null=True, blank=True)
    tx = models.PositiveIntegerField(null=True, blank=True)
    rx = models.PositiveIntegerField(null=True, blank=True)

    sync_status = models.BooleanField(default=False)


class Order(models.Model):
    ORDER_TYPES = (
        ("Drop Bottle", "drop_bottle"),
        ("Screen Refill", "refill"),
        ("Button Refill", "button_refill"),
    )
    ORDER_MEDIUMS = (
        ("Kiosk", "KIOSK"),
        ("Mobile", "MOBILE"),
    )

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    kiosk_id = models.CharField(max_length=100, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, blank=True)
    drink_price = models.PositiveIntegerField(null=True, blank=True, default=0)
    flavor = models.CharField(max_length=100, null=True, blank=True)
    caffeine_level = models.PositiveIntegerField(null=True, blank=True, default=0)
    water_amount = models.PositiveIntegerField(null=True, blank=True)
    temperature = models.CharField(max_length=100, default='cold')
    order_type = models.CharField(
        choices=ORDER_TYPES,
        default='Screen Refill',
        max_length=20
    )
    order_medium = models.CharField(
        choices=ORDER_MEDIUMS,
        default='KIOSK',
        max_length=20
    )
    sync_status = models.BooleanField(default=False)
    additive_code = models.CharField(max_length=100, default='00000000')  # 88000000
    temperature_code = models.CharField(max_length=100, default='0')

    def save(self, *args, **kwargs):
        self.additive_code = generate_additive_code(self.flavor, self.caffeine_level)
        self.temperature_code = get_temperature_code(self.temperature)
        super(Order, self).save(*args, **kwargs)


class User(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=100, null=True, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    role = models.CharField(max_length=100, null=True, blank=True)


class OperationNames(models.TextChoices):
    STARTUP = 'Reset', 'start_up'
    MAKE_DROP_DRINK = 'Make Drop Drink', 'make_drop_drink'
    MAKE_REFILL_DRINK = 'Make Refill Drink', 'make_refill_drink'
    CHECK_FOR_CAP = 'Check For Cap', 'check_for_cap'
    UNCAP_AND_PROBE_BOTTLE = 'Uncap And Probe Bottle', 'uncap_and_probe_bottle'
    RETRIEVE_CLOSE_AND_UNCAP_NEXT_BOTTLE = 'Retrieve And Stage Next Bottle', 'retrieve_close_and_uncap_next_bottle'
    RETRIEVE_NEXT_BOTTLE = 'Retrieve Next Bottle', 'retrieve_next_bottle'
    GARBAGE_BOTTLE = 'Garbage Bottle', 'garbage_bottle'
    FLAP_SEAL_BOTTLE = 'Flap Seal Bottle', 'flap_seal_bottle',
    CAP_AND_DELIVER_BOTTLE = 'Cap, Deliver And Stage Next Bottle', 'cap_and_deliver_bottle'


class Operations(models.Model):
    operation_name = models.CharField(
        max_length=50,
        choices=OperationNames.choices,
        default=OperationNames.STARTUP,
    )
    entry_point = models.CharField(max_length=100, null=True, blank=True)
    operation_type = models.CharField(max_length=100, null=True, blank=True)


class TaskNames(models.TextChoices):
    START_DISPENSE = 'Start Dispense', 'start_dispense'
    END_DISPENSE = 'End Dispense', 'end_dispense'
    END_REFILL_ORDER = 'End Refill Order', 'end_order'


class Tasks(models.Model):
    task_name = models.CharField(
        max_length=50,
        choices=TaskNames.choices,
        default=TaskNames.START_DISPENSE,
    )
    entry_point = models.CharField(max_length=100, null=True, blank=True)


class MaintenanceTaskNames(models.TextChoices):
    RAISE_TOWER_ONE = 'Raise Tower 1', 'raise_tower_one'
    RAISE_TOWER_TWO = 'Raise Tower 2', 'raise_tower_two'
    RAISE_TOWER_THREE = 'Raise Tower 3', 'raise_tower_three'
    LOWER_TOWER_ONE = 'Lower Tower 1', 'lower_tower_one'
    LOWER_TOWER_TWO = 'Lower Tower 2', 'lower_tower_one'
    LOWER_TOWER_THREE = 'Lower Tower 3', 'lower_tower_three'
    PURGE_ADDITIVE_ONE_DROP_BOTTLE = 'Purge Additive 1 Drop Bottle', 'purge_additive_one_drop_bottle'
    PURGE_ADDITIVE_TWO_DROP_BOTTLE = 'Purge Additive 2 Drop Bottle', 'purge_additive_two_drop_bottle'
    PURGE_ADDITIVE_THREE_DROP_BOTTLE = 'Purge Additive 3 Drop Bottle', 'purge_additive_three_drop_bottle'
    PURGE_ADDITIVE_FOUR_DROP_BOTTLE = 'Purge Additive 4 Drop Bottle', 'purge_additive_four_drop_bottle'
    PURGE_ADDITIVE_FIVE_DROP_BOTTLE = 'Purge Additive 5 Drop Bottle', 'purge_additive_five_drop_bottle'
    PURGE_ADDITIVE_SIX_DROP_BOTTLE= 'Purge Additive 6 Drop Bottle', 'purge_additive_six_drop_bottle'
    PURGE_ADDITIVE_ONE_REFILL = 'Purge Additive 1 Refill', 'purge_additive_one_refill'
    PURGE_ADDITIVE_TWO_REFILL = 'Purge Additive 2 Refill', 'purge_additive_two_refill'
    PURGE_ADDITIVE_THREE_REFILL = 'Purge Additive 3 Refill', 'purge_additive_three_refill'
    PURGE_ADDITIVE_FOUR_REFILL = 'Purge Additive 4 Refill', 'purge_additive_four_refill'
    PURGE_ADDITIVE_FIVE_REFILL = 'Purge Additive 5 Refill', 'purge_additive_five_refill'
    PURGE_ADDITIVE_SIX_REFILL = 'Purge Additive 6 Refill', 'purge_additive_six_refill'
    FLUSH_BOTTLE_AMBIENT = 'Flush Bottle (Ambient)', 'flush_bottle_ambient'  # 0 or 8 to deck
    FLUSH_BOTTLE_COLD = 'Flush Bottle (Cold)', 'flush_bottle_cold'
    FLUSH_REFILL_AMBIENT = "Flush Refill Ambient", 'flush_refill_ambient'  # set to refill, set_temp,
    # send_additive_code, start_dispense
    FLUSH_REFILL_COLD = "Flush Refill Cold", 'flush_refill_cold'
    OPEN_GRIPPER = "Open Gripper", 'open_gripper'
    CLOSE_GRIPPER = "Close Gripper", 'close_gripper'
    OPEN_RECEPTION_DOOR = 'Open Reception Door', 'open_reception_door'
    CLOSE_RECEPTION_DOOR = 'Close Reception Door', 'close_reception_door'


class MaintenanceTasks(models.Model):
    task_name = models.CharField(
        max_length=50,
        choices=MaintenanceTaskNames.choices,
        default=MaintenanceTaskNames.RAISE_TOWER_ONE,
    )
    entry_point = models.CharField(max_length=100, null=True, blank=True)
