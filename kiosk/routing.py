# kiosk/routing.py
from django.urls import re_path

from kiosk.module_consumers import kiosk_consumer, chat_consumer

websocket_urlpatterns = [
    # re_path(r'ws/kiosk/(?P<room_name>\w+)/$', consumers.KioskConsumer.as_asgi()),
    re_path(r'ws/kiosk/kiosk_client/', kiosk_consumer.KioskConsumer.as_asgi()),
    re_path(r'ws/kiosk/(?P<room_name>\w+)/$', chat_consumer.ChatConsumer.as_asgi()),
    re_path(r'ws/kiosk_client/$', chat_consumer.ChatConsumer.as_asgi()),

]