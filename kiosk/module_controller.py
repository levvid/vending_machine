# This program allows us to send individual commands
# to the individual modules to help recover from errors remotely

import time

import serial


def read_serial_port():
    bytes_to_read = ser.inWaiting()
    while bytes_to_read == 0:
        time.sleep(1)
        bytes_to_read = ser.inWaiting()
    command_response = str(ser.read(bytes_to_read), 'utf-8')

    split_response = command_response.split('\r\n')

    for res in split_response:
        print('Is a number: {}'.format(res.isdigit()))

    print('responses {}'.format(split_response))
    print('module responded with {}'.format(command_response))
    return command_response


def has_capital_response(response_string):
    return any(char.isupper() for char in response_string)


module = input("Which Module Are We Controlling? ")
print(module)

portCount = 0
while portCount < 7:
    portString = "/dev/ttyACM" + str(portCount)
    ser = serial.Serial(portString, baudrate=9600, timeout=1)
    print("sending ? to port " + portString)
    ser.write(b'?')
    print('Waiting for port {} to respond.'.format(portString))
    moduleData = str(ser.read(), 'utf-8')
    print('Module data is {}'.format(moduleData))
    if moduleData.lower() == module.lower():
        print("found a match at port " + portString)
        command = input("What command would you like to give? ")
        ser.write(str.encode(command))
        commandResponse = read_serial_port()
        # while not has_capital_response(commandResponse):
        #     time.sleep(1)
        #     commandResponse = read_serial_port()
        print('Responded with {}'.format(commandResponse))
        nextCommand = ""
        while nextCommand != "exit":
            nextCommand = input("Next command (type exit to quit) ")
            if nextCommand != "exit":
                ser.write(str.encode(nextCommand))
                commandResponse = read_serial_port()
                print('Responded with {}'.format(commandResponse))
                # while not has_capital_response(commandResponse):
                #     time.sleep(1)
                #     commandResponse = read_serial_port()
                #     print('Responded with {}'.format(commandResponse))
        ser.close()
        break
    ser.close()
    portCount = portCount + 1
