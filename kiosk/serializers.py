from rest_framework import serializers
from kiosk.models import Inventory, DrinkPrice, ModuleStatus, Additives, Kiosk, Order, Operations, Tasks, MaintenanceTasks


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = ['date_created', 'date_updated', 'tower_one_remaining', 'tower_two_remaining', 'tower_three_remaining',
                  'selected_tower', 'sync_status']


class AdditivesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Additives
        fields = ('name', 'amount_remaining', 'pump_number', 'concentration', 'dispense_rate')


class DrinkPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrinkPrice
        fields = (
            'date_created', 'date_updated', 'drop_bottle_just_water', 'drop_bottle_flavor', 'drop_bottle_supplement',
            'refill_just_water', 'refill_flavor', 'refill_supplement')


class ModuleStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleStatus
        fields = (
            'date_created', 'date_updated', 'drain_tank_status', 'drop_bottle_status', 'refill_status',
            'uv_filter_status')


class KioskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kiosk
        fields = ('date_created', 'date_updated', 'kiosk_id', 'alias', 'location', 'location_alias', 'losant_id',
                  'nickname')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'date_created', 'date_updated', 'uuid', 'kiosk_id', 'drink_price', 'flavor', 'caffeine_level',
            'water_amount',
            'temperature', 'order_type', 'order_medium', 'additive_code', 'temperature_code')


class OperationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operations
        fields = ['operation_name']


class TasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ['task_name']


class MaintenanceTasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaintenanceTasks
        fields = ['task_name']
