# chat/consumers.py
import json
from channels.generic.websocket import AsyncWebsocketConsumer

from pegasus.celery import send_msg

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_group_name = 'kiosk_client_group'
        self.room_name = 'kiosk_client'

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        print('received msg: {}'.format(message), flush=True)
        # send_msg.delay(message)
        # send message to job handler
        await self.channel_layer.send(
            'job_handler',
            {
                'type': 'kiosk_client',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        print('received chat_msg: {}'.format(message))

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))

    async def job_handler_message(self, event):
        message = event['message']

        print('Sending msg {}'.format(message))

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
