import json
import logging
import json
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer

log = logging.getLogger(__name__)


class KioskConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.room_group_name = 'kiosk_client_group'
        self.room_name = 'kiosk_client'

    async def connect(self):
        print('Room: {} Group: {}'.format(self.room_name, self.room_group_name))
        # Join room group
        print("connected to websockets")
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        # Send message to job handler
        print('Received WS message: {}'.format(message))
        await self.channel_layer.group_send(
            'job_handler',
            {
                'type': 'kiosk_client',
                'message': message
            }
        )

    async def job_handler_message(self, event):
        message = event['message']

        print('Received message from job_handler {}'.format(message))

        print('Room: {} Group: {}'.format(self.room_name, self.room_group_name))

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))

