import copy
import copy
import queue
from multiprocessing import Pool

from channels.consumer import AsyncConsumer

from kiosk.functions.serial_port import write_to_module
from kiosk.jobs.commands import commands
from kiosk.jobs.job import Job
from kiosk.jobs.jobs_config import jobs
from pegasus.celery import connect_modules, post_order_complete, preauthorize_payment, \
    monitor_refill, dispense, module_logger, get_additive_code, get_temperature_code, get_gantry_location_to_retrieve, \
    get_gantry_location_for_tower, get_raise_tower_commmand, post_inventory_data, update_bottle_count, get_drink_price, \
    exec_maintenance_task

pool = Pool()

COLOR_ACTIONS = {
    'start_page': 'W',
    'drop_bottle': 'F',
    'refill': 'S',
    'guava': 'R',
    'cucumber': 'G',
    'mint': 'G',
    'lemonade': 'Y',
    'water': 'B',
    'bottle_select': 'D'
}


class JobHandlerConsumer(AsyncConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.order_uuid = None
        self.current_order_type = None
        self.name = 'JobHandler'
        self.channel_name = 'job_handler'
        self.current_jobs = queue.LifoQueue()
        self.current_job = None
        self.current_order_uuid = None
        self.num_modules = 6
        self.monitor_refill_status = False
        self.refill_status_count = 0
        self.water_amount_refill = 0
        self.modules = {
            'gantry': {},
            'deck': {},
            'tower': {},
            'lighting': {},
            'refill': {},
            'additive': {},
            'reception': {},
            'payment': {}
        }
        self.module_ports = set()
        self.current_parallel_job = None

    async def queue_job(self, event):
        job_name = event['job_name']
        job = jobs[job_name]

        try:
            job_type = event['job_type']
        except:
            job_type = ''

        module_logger.delay('debug', 'Job Handler', 'starting job: {}'.format(job_name))
        tasks = job['tasks']
        job_entry_point = job['entry_point']
        num_retries = job.get('num_retries', 1)

        new_job = Job(job_name, tasks, job_entry_point, num_retries)

        if job_type == 'order':
            self.current_order_uuid = event['order_uuid']

        module_logger.delay('debug', 'Job Handler', 'queueing new job {}'.format(new_job.info))

        # push job to background if another job is in progress

        if self.current_job:
            self.current_jobs.put(self.current_job)
            module_logger.delay('debug', 'Job Handler',
                                'Pushing current_job {} to background tasks. Background tasks length {}'
                                .format(self.current_job.name, self.current_jobs.qsize()))
        self.current_job = new_job  # check first to make sure current job is not occupied

        module_logger.delay('debug', 'Job Handler', 'Current job {}'.format(self.current_job.name))
        await self.channel_layer.send(
            self.channel_name, {
                'type': 'queue_task',
                'task_name': self.current_job.current_task,
            }
        )

    async def queue_task(self, event):
        task_name = event['task_name']

        module_logger.delay('debug', 'Job Handler', 'Queueing task: {}'.format(task_name))

        self.current_job.current_task = task_name

        job_type = self.current_job.tasks[self.current_job.current_task]['type']
        module_logger.delay('debug', 'Job Handler', 'Job type: {}'.format(job_type))

        if job_type == 'task':
            # start async task
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'exec_task',
                }
            )
        elif job_type == 'command':
            # fetch command and send to module
            command = commands[self.current_job.current_task]
            module = command['module']
            port = self.modules[module]['port']

            module_logger.delay('debug', 'Job Handler', 'CMD: {} Module: {}'.format(command, module))
            data = {'command': command, 'module': module, 'port': port}

            res = pool.apply_async(write_to_module, [data, port])

            res = res.get(timeout=30)

            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_next_task',
                    'message': 'queue_next_task',
                    'result': res
                }
            )
            module_logger.delay('debug', 'Job Handler', 'Response: {}'.format(res))
        elif job_type == 'sync_command':
            await self.channel_layer.send(
                self.channel_name, {
                    "type": 'exec_sync_commands',
                },
            )
        elif job_type == 'sync_command_refill':
            await self.channel_layer.send(
                self.channel_name, {
                    "type": 'exec_sync_commands_refill',
                },
            )  # exec_sync_commands_refill
        elif job_type == 'sync_task':
            await self.channel_layer.send(
                self.channel_name, {
                    "type": 'exec_sync_tasks',
                },
            )
        elif job_type == 'client_message':
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'send_message_to_kiosk_client',
                }
            )
        elif job_type == 'parallel_job':
            self.current_parallel_job = self.current_job.current_task
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'exec_parallel_job',
                }
            )
        else:  # job
            # queue job
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_job',
                    'job_name': self.current_job.current_task
                }
            )

    async def post_order_complete(self, event):
        module_logger.delay('debug', 'Job Handler', 'post_order_complete ...')
        self.order_uuid = None
        self.current_order_type = None
        post_order = post_order_complete.delay()
        module_logger.delay('debug', 'Job Handler', 'started post_order_complete task: {}'.format(post_order))
        return post_order

    async def register_module_response(self, event):
        message = event['message']
        module_logger.delay('debug', 'Job Handler', 'received module message: {}'.format(message))
        module_name = event['module_name']
        connection_status = event['status']

        if connection_status == 'CONNECTED':
            port = event['port']
            status = event['status']
            self.modules[module_name]['port'] = port
            self.modules[module_name]['status'] = status
            self.module_ports.add(port)

            module_logger.delay('debug', 'Job Handler',
                                'module {} connected successfully. {}'.format(module_name, self.modules[module_name]))

            module_logger.delay('debug', 'Job Handler', 'num_connected_modules: {}'.format(len(self.module_ports)))
            if len(self.module_ports) == self.num_modules and self.current_job:
                module_logger.delay('debug', 'Job Handler', 'All modules connected. Calling queue next task.')
                await self.channel_layer.send(
                    self.channel_name, {
                        'type': 'queue_next_task',
                        'message': 'queue_next_task',
                        'result': 'K'
                    }
                )
        elif connection_status == 'DISCONNECTED':
            module_logger.delay('debug', 'Job Handler', 'module {} disconnected.'.format(module_name))
            self.modules[module_name]['port'] = event['port']
            self.modules[module_name]['status'] = event['status']

    async def module_response(self, event):
        module_name = event['module_name']
        module_logger.delay('debug', 'Job Handler', 'received response from module {}'.format(module_name))
        current_task = self.current_job.tasks[self.current_job.current_task]
        message = event['message']
        result = event['result']

        if 'status' in current_task:
            module_logger.delay('debug', 'Job Handler', 'Found a terminal state {}'.format(result))
            if current_task['status'] == 'complete':  # job complete. dequeue one if present.
                module_logger.delay('debug', 'Job Handler', 'status complete.')
                completed_job = self.current_job
                completed_job.set_end_time()  # mark job as complete

                module_logger.delay('debug', 'Job Handler', 'Completed {} job at {}.'
                                    .format(completed_job.name, completed_job.end_time.strftime("%b %d %Y %H:%M:%S")))

                if not self.current_jobs.empty():
                    module_logger.delay('debug', 'Job Handler', 'Current jobs queue not empty. Queue next job.')
                    next_job = self.current_jobs.get()
                    self.current_job = next_job
                    module_logger.delay('debug', 'Job Handler', 'Current job is {}'.format(self.current_job.name,
                                                                                           self.current_job.current_task))
                else:
                    module_logger.delay('debug', 'Job Handler', 'no more tasks in queue. ')

                    if not self.current_parallel_job:
                        await self.channel_layer.send(
                            'operation_manager', {
                                'type': 'operation_update',
                                'operation_name': self.current_job.name,
                                'status': 'K'
                            }
                        )
                        self.current_job = None
                        self.monitor_refill_status = False
                        self.refill_status_count = 0
                        self.current_order_uuid = None
                        # self.water_amount_refill = 0
        else:  # not a terminal task
            module_logger.delay('debug', 'Job Handler', 'not a terminal task')

        if self.current_job:  # only queue next task if there is a job in progress
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_next_task',
                    'message': 'queue_next_task',
                    'result': result
                }
            )

    async def queue_next_task(self, event):
        """
        1. receive message from module
            a) check whether the result is mapped, if so pick the next task and add it to queue
            b) if error, send notifs.


        A. get next task.
            - if job add to queue.
            - if task run it
            - if command, send to module
        B. send error notifs.
        """

        result = event['result']
        module_logger.delay('debug', 'Job Handler', 'Queueing next task with result {}'.format(result))

        current_job_result_dict = self.current_job.tasks[self.current_job.current_task]['result']

        if result in current_job_result_dict:  # result mapped, queue task.
            next_task = self.current_job.tasks[self.current_job.current_task]['result'][result]

            if next_task == 'complete':
                module_logger.delay('debug', 'Job Handler', 'next task is complete')
                completed_job = self.current_job
                completed_job.set_end_time()  # mark job as complete

                module_logger.delay('debug', 'Job Handler', 'Completed {} job at {}.'
                                    .format(completed_job.name, completed_job.end_time.strftime("%b %d %Y %H:%M:%S")))

                if not self.current_jobs.empty():
                    module_logger.delay('debug', 'Job Handler', 'Current jobs queue not empty. Queue next job.')
                    next_job = self.current_jobs.get()
                    self.current_job = next_job
                    module_logger.delay('debug', 'Job Handler', 'Current job is {}'.format(self.current_job.name,
                                                                                           self.current_job.current_task))
                    await self.channel_layer.send(
                        self.channel_name, {
                            'type': 'queue_next_task',
                            'message': 'queue_next_task',
                            'result': result
                        }
                    )
                else:
                    module_logger.delay('debug', 'Job Handler', 'no more tasks in queue. ')
                    if not self.current_parallel_job:
                        await self.channel_layer.send(
                            'operation_manager', {
                                'type': 'operation_update',
                                'operation_name': self.current_job.name,
                                'status': 'K'
                            }
                        )
                        self.current_job = None
                        self.current_jobs.queue.clear()
            elif next_task == 'retry':
                self.current_job.num_retries -= 1
                if self.current_job.num_retries >= 1:
                    module_logger.delay('debug', 'Job Handler',
                                        'Retrying operation {}. Num tries: {}.'.format(self.current_job.name,
                                                                                       self.current_job.num_retries))
                    await self.channel_layer.send(
                        self.channel_name, {
                            'type': 'queue_task',
                            'task_name': self.current_job.entry_point
                        }
                    )
                else:
                    module_logger.delay('debug', 'Job Handler',
                                        'num attempts on operation {} have been exceeded.'.format(
                                            self.current_job.name))
                    await self.channel_layer.send(
                        'operation_manager', {
                            'type': 'operation_update',
                            'operation_name': self.current_job.name,
                            'status': 'K'
                        }
                    )
                    self.current_job = None
                    self.current_job = None
                    self.current_jobs.queue.clear()
            elif next_task == 'error':
                module_logger.delay('debug', 'Job Handler', 'An error has occurred.')

                await self.channel_layer.send(
                    'operation_manager', {
                        'type': 'operation_update',
                        'operation_name': self.current_job.name,
                        'status': 'E'
                    }
                )

                self.current_job = None
                self.current_jobs.queue.clear()
            else:
                module_logger.delay('debug', 'Job Handler', 'task not complete. calling queue task.')
                await self.channel_layer.send(
                    self.channel_name, {
                        'type': 'queue_task',
                        'task_name': next_task
                    }
                )
        else:
            module_logger.delay('debug', 'Job Handler', 'Result does not match current config.')
            module_logger.delay('debug', 'Job Handler',
                                'Result: {} jobsDict: {}'.format(result, current_job_result_dict))
            module_logger.delay('debug', 'Job Handler', 'Tasks: {}'.format(self.current_job.tasks))

            await self.channel_layer.send(
                'operation_manager', {
                    'type': 'operation_update',
                    'operation_name': self.current_job.name,
                    'status': 'E'
                }
            )
            await self.channel_layer.group_send(
                'kiosk_client_group', {
                    'type': 'job_handler_message',
                    'message': {
                        'topic': 'order_status',
                        'status': 'order_failed',
                        'data': 'Order failed',
                        'order_type': 'drop_bottle'
                    },
                }
            )
            self.current_job = None
            self.current_parallel_job = None
            self.current_jobs.queue.clear()

    async def exec_sync_commands(self, event):
        sync_commands = self.current_job.tasks[self.current_job.current_task]['sync_commands']
        next_task_results = self.current_job.tasks[self.current_job.current_task]['result']

        data_list = []
        ports = []
        read_responses = []

        args = []

        for sync_command in sync_commands:
            command = copy.deepcopy(commands[sync_command])
            module = command['module']
            read_response = self.current_job.tasks[sync_command]['read_response']
            port = self.modules[module]['port']
            data = {'command': command, 'module': module, 'port': port}

            read_responses.append(read_response)

            data_list.append(data)
            ports.append(port)

            args.append((data, port, read_response))

        res = pool.starmap(write_to_module, args)
        module_logger.delay('debug', 'Job Handler', 'Response {}'.format(res))

        if len(set(res)) == 1:  # make sure the two results match
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_task',
                    'task_name': next_task_results[res[0]]
                }
            )

    async def exec_sync_commands_refill(self, event):
        sync_commands = self.current_job.tasks[self.current_job.current_task]['sync_commands']
        next_task_results = self.current_job.tasks[self.current_job.current_task]['result']

        data_list = []
        ports = []
        read_responses = []

        responses = []

        for sync_command in sync_commands:
            command = copy.deepcopy(commands[sync_command])
            if sync_command == 'write_additive_code':
                additive_code = get_additive_code.delay(self.current_order_uuid)
                command['command'] = additive_code.wait(timeout=None, interval=0.5)
            elif sync_command == 'write_temperature_code' or sync_command == 'write_temperature_code_refill':
                temperature_code = get_temperature_code.delay(self.current_order_uuid)
                command['command'] = temperature_code.wait(timeout=None, interval=0.5)

            module = command['module']
            read_response = self.current_job.tasks[sync_command]['read_response']
            port = self.modules[module]['port']
            data = {'command': command, 'module': module, 'port': port}

            read_responses.append(read_response)

            data_list.append(data)
            ports.append(port)
            res = pool.apply_async(write_to_module, [data, port])
            res = res.get(timeout=30)
            responses.append(res)

            module_logger.delay('debug', 'Job Handler',
                                'REFILL DEBUG: {} Response {} Next task: {}'.format(sync_command, res,
                                                                                    next_task_results))

        if len(set(responses)) == 1:  # make sure the two results match
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_task',
                    'task_name': next_task_results[res[0]]
                }
            )

        # await self.channel_layer.send(
        #     self.channel_name, {
        #         'type': 'queue_task',
        #         'task_name': next_task_results[res[0]]
        #     }
        # )

    async def sync_module_response(self, event):
        module_name = event['module_name']

        result = event['result']

        current_task = self.current_job.tasks[self.current_job.current_task]
        message = event['message']

        self.num_responses_received += 1
        self.responses.append(result)

        module_logger.delay('debug', 'Job Handler',
                            'sync response {} from module {}. num_responses: {}, num_responses_received: {}'.format(
                                result, module_name,
                                self.num_responses,
                                self.num_responses_received))

        if self.num_responses_received >= self.num_responses:
            if len(set(self.responses)) == 1:  # all responses are the same, proceed
                module_logger.delay('debug', 'Job Handler',
                                    'Received enough responses for sync_command {}. Queuing next task.'.format(
                                        self.current_job.current_task))
                if self.current_job:  # only queue next task if there is a job in progress
                    await self.channel_layer.send(
                        self.channel_name, {
                            'type': 'queue_next_task',
                            'message': 'queue_next_task',
                            'result': result
                        }
                    )
            else:
                module_logger.delay('debug', 'Job Handler',
                                    'Unmatched responses. Received enough responses for sync_command {}. Queuing next task.'.format(
                                        self.current_job.current_task))
                if self.current_job:  # only queue next task if there is a job in progress
                    await self.channel_layer.send(
                        self.channel_name, {
                            'type': 'queue_next_task',
                            'message': 'queue_next_task',
                            'result': result
                        }
                    )

    async def exec_task(self, event):
        if self.current_job.current_task == 'register_modules':
            module_logger.delay('debug', 'Job Handler', 'starting register_modules task.')
            self.module_ports = set()
            modules = connect_modules.delay()
            module_logger.delay('debug', 'Job Handler', 'Started connect_modules task: {}'.format(modules))
            return modules
        elif self.current_job.current_task == 'preauthorize_payment':
            module_logger.delay('debug', 'Job Handler', 'preauthorizing payment...')
            payment = preauthorize_payment.delay()
            module_logger.delay('debug', 'Job Handler', 'started preauthorize_payment task: {}'.format(payment))
            return payment
        elif self.current_job.current_task == 'monitor_refill':
            module_logger.delay('debug', 'Job Handler', 'Started monitor refill task.')
            self.monitor_refill_status = True
            monitor_refill.delay()
            return 'monitor_refill'
        elif self.current_job.current_task == 'update_bottle_count':
            update_bottle_count.delay()
            module_logger.delay('debug', 'Job Handler', 'Updating bottle count.')
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_next_task',
                    'message': 'queue_next_task',
                    'result': 'K'
                }
            )
            return 'update_bottle_count'
        elif self.current_job.current_task == 'post_inventory_data':

            post_inventory_data.delay(self.current_order_uuid)
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_next_task',
                    'message': 'queue_next_task',
                    'result': 'K'
                }
            )
            return 'post_inventory_data'

        command = commands[self.current_job.current_task]
        module = command['module']
        port = self.modules[module]['port']
        if self.current_job.current_task == 'write_temperature_code_refill':
            temperature_code = get_temperature_code.delay(self.current_order_uuid)
            command['command'] = temperature_code.wait(timeout=None, interval=0.5)
            module_logger.delay('debug', 'Job Handler', 'Temp code: {}'.format(command))
        module_logger.delay('debug', 'Job Handler', 'CMD: {} Module: {}'.format(command, module))

        if self.current_job.current_task == 'grab_bottle':
            task = get_gantry_location_to_retrieve.delay()
            command['command'] = task.wait(timeout=None, interval=0.5)
        elif self.current_job.current_task == 'gantry_move_to_tower':
            task = get_gantry_location_for_tower.delay()
            tower = task.wait(timeout=None, interval=0.5)

            if tower == 'E':
                module_logger.delay('debug', 'Job Handler', 'Tower out of bottles.')
                await self.channel_layer.send(
                    'operation_manager', {
                        'type': 'operation_update',
                        'operation_name': self.current_job.name,
                        'status': 'K'
                    }
                )
                self.current_parallel_job = None
                self.current_job = None
                self.current_job = None
                self.current_jobs.queue.clear()
                self.monitor_refill_status = False
                self.current_order_uuid = None
                self.refill_status_count = 0
                self.water_amount_refill = 0
                return
                # TODO - change this to a reset function
            else:
                command['command'] = tower

        data = {'command': command, 'module': module, 'port': port}

        res = pool.apply_async(write_to_module, [data, port])

        res = res.get(timeout=30)

        await self.channel_layer.send(
            self.channel_name, {
                'type': 'queue_next_task',
                'message': 'queue_next_task',
                'result': res
            }
        )
        module_logger.delay('debug', 'Job Handler', 'Response: {}'.format(res))

    async def exec_parallel_job(self, event):

        await self.channel_layer.send(
            self.channel_name, {
                'type': 'queue_next_task',
                'message': 'queue_next_task',
                'result': 'K'
            }
        )

        open_and_close_door_job = jobs['open_and_close_door']
        tasks = open_and_close_door_job['tasks']
        open_door_command = commands['open_door']
        close_door_command = commands['close_door']

        module = open_door_command['module']
        port = self.modules[module]['port']
        data = {'command': open_door_command, 'module': module, 'port': port}
        res = pool.apply_async(write_to_module, [data, port])
        module_logger.delay('debug', 'Job Handler', 'opened door.')
        res = res.get(timeout=30)

        if res != 'K':
            module_logger.delay('An error occurred while trying to open the door.')
            await self.channel_layer.group_send(
                'kiosk_client_group', {
                    'type': 'job_handler_message',
                    'message': {
                        'topic': 'order_status',
                        'status': 'order_failed',
                        'data': 'Order failed',
                        'order_type': 'drop_bottle'
                    },
                }
            )
            return
        await self.channel_layer.group_send(
            'kiosk_client_group', {
                'type': 'job_handler_message',
                'message': tasks['send_door_open_message']['message'],
            }
        )

        data = {'command': close_door_command, 'module': module, 'port': port}
        res = pool.apply_async(write_to_module, [data, port])

        res = res.get(timeout=30)
        module_logger.delay('debug', 'Job Handler', 'Closed door.')
        if res != 'K':
            module_logger.delay('An error occurred while trying to close the door.')

            return
        await self.channel_layer.group_send(
            'kiosk_client_group', {
                'type': 'job_handler_message',
                'message': tasks['send_door_closed_message']['message'],
            }
        )

        self.current_parallel_job = None

    async def exec_sync_tasks(self, event):
        sync_tasks = self.current_job.tasks[self.current_job.current_task]['sync_tasks']
        next_task_results = self.current_job.tasks[self.current_job.current_task]['result']
        module_logger.delay('debug', 'Job Handler', 'Response {}'.format(next_task_results))
        data_list = []
        ports = []
        read_responses = []

        args = []

        for sync_task in sync_tasks:
            command = copy.deepcopy(commands[sync_task])

            if sync_task == 'write_additive_code':
                additive_code = get_additive_code.delay(self.current_order_uuid)
                command['command'] = additive_code.wait(timeout=None, interval=0.5)
            elif sync_task == 'write_temperature_code' or sync_task == 'write_temperature_code_refill':
                temperature_code = get_temperature_code.delay(self.current_order_uuid)
                command['command'] = temperature_code.wait(timeout=None, interval=0.5)

                # module_logger.delay('TEMPERATURE CODE: {} {}'.format(command, sync_task))
            elif sync_task == 'raise_tower_level':
                task = get_raise_tower_commmand.delay()
                command['command'] = task.wait(timeout=None, interval=0.5)
            module = command['module']
            read_response = self.current_job.tasks[sync_task]['read_response']
            port = self.modules[module]['port']
            data = {'command': command, 'module': module, 'port': port}

            read_responses.append(read_response)

            data_list.append(data)
            ports.append(port)

            args.append((data, port, read_response))

        res = pool.starmap(write_to_module, args)
        module_logger.delay('debug', 'Job Handler', 'Response: {}'.format(res))

        if len(set(res)) == 1:  # make sure the two results match
            await self.channel_layer.send(
                self.channel_name, {
                    'type': 'queue_task',
                    'task_name': next_task_results[res[0]]
                }
            )

    async def monitor_refill(self, event):
        module_logger.delay('debug', 'Job Handler', 'monitor_refill ...')
        command = commands['check_dispense_status']
        module = command['module']

        port = self.modules[module]['port']
        data = {'command': command, 'module': 'additive', 'port': port}

        res = pool.apply_async(write_to_module, [data, port, True, True])

        res = res.get(timeout=30)
        ounces, _ = res
        # module_logger.delay('debug', 'Job Handler', 'Monitor refill {}'.format(res))  # (['0'], 'K')

        try:
            ounces_dispensed = int(ounces[0]) * 0.0338
        except:
            ounces_dispensed = 0  # remove and track refill amount better to avoid false 0's, only send when the
            # amount changes?
        task = get_drink_price.delay(self.current_order_uuid)

        order_running_price = task.wait(timeout=None, interval=0.5)
        self.water_amount_refill = ounces_dispensed
        module_logger.delay('debug', 'Job Handler', 'Monitor refill {}, self.water_amount_refill {}'.format(res, self.water_amount_refill))  # (['0'], 'K')
        if order_running_price is not None:
            order_running_price = (order_running_price / 16) * ounces_dispensed

            if order_running_price != 0:
                if order_running_price < 100:
                    order_running_price += 15

            message = {
                'topic': 'refill_order_status',
                'status': 'refill_in_progress',  # refill_timeout  - TBD
                'data': {'water_amount': round(ounces_dispensed, 1), 'price': int(order_running_price)},
                # amount - ounces, price - cents
                'order_type': 'refill'
            }

            await self.channel_layer.group_send(
                'kiosk_client_group', {
                    'type': 'job_handler_message',
                    'message': message,
                }
            )



        if self.monitor_refill_status:
            self.refill_status_count += 1
            monitor_refill.apply_async(countdown=1)  # run after 2 second(s)

    async def send_message_to_kiosk_client(self, event):
        module_logger.delay('debug', 'Job Handler', 'in send_message_to_kiosk_client {}'.format(event))
        message = self.current_job.tasks[self.current_job.current_task]['message']

        module_logger.delay('debug', 'Job Handler', 'sending message: {}'.format(message))
        # time.sleep(2)  # TODO - only for testing, remove for PROD

        await self.channel_layer.group_send(
            'kiosk_client_group', {
                'type': 'job_handler_message',
                'message': message,
            }
        )

        await self.channel_layer.send(
            self.channel_name, {
                'type': 'queue_next_task',
                'message': 'queue_next_task',
                'result': 'K'
            }
        )

    async def check_module_connection_status(self, event):
        if len(self.module_ports) == self.num_modules:
            module_logger.delay('debug', 'Job Handler', 'all modules connected.')
        else:
            module_logger.delay('debug', 'Job Handler', 'all modules not connected.')

            if not self.current_job:
                module_logger.delay('debug', 'Job Handler', 'no current job running. Starting connect modules task.')
                connect_modules.delay(reconnect=True)

    async def refill_response(self, event):
        result = event['result']
        command = event['message']
        module_logger.delay('debug', 'Job Handler', 'monitor refill response: {}'.format(result))

        if self.refill_status_count >= 60:  # 90 second timeout
            module_logger.delay('debug', 'Job Handler', 'Refill inactivity timeout {}'.format(self.refill_status_count))
            await self.channel_layer.send(
                'operation_manager', {
                    'type': 'operation_update',
                    'operation_name': self.current_job.name,
                    'status': 'K'
                }
            )
            dispense.delay('stop_dispense')
            self.current_parallel_job = None
            self.current_job = None
            self.current_job = None
            self.current_jobs.queue.clear()
            self.monitor_refill_status = False
            self.current_order_uuid = None
            self.refill_status_count = 0

        if command == 'end_order':
            self.monitor_refill_status = False
            self.refill_status_count = 0
            self.current_parallel_job = None
            # update self.water_amount_refill

        if command == 'start_dispense':
            self.refill_status_count = 0

        if self.monitor_refill_status:
            self.refill_status_count += 1
            monitor_refill.apply_async(countdown=1)  # run after 1 second

        return 'K'

    async def dispense(self, event):
        dispense_command = event['command']

        module_logger.delay('debug', 'Job Handler', 'In refill dispense')
        if dispense_command == 'end_order':
            await self.channel_layer.send(
                'operation_manager', {
                    'type': 'operation_update',
                    'operation_name': 'make_refill_drink',  ### Error
                    'status': 'K'
                }
            )
            dispense.delay('end_dispense')
            self.current_parallel_job = None
            self.current_job = None
            self.current_job = None
            self.current_jobs.queue.clear()
            self.monitor_refill_status = False
            self.current_order_uuid = None
            self.refill_status_count = 0
            # update refill amount
            temp_refill_amount = self.water_amount_refill
            module_logger.delay('debug', 'Job Handler', 'Amount of water B4: {}'.format(temp_refill_amount))
            self.water_amount_refill = 0  # TODO - wait to confirm on mini
            module_logger.delay('debug', 'Job Handler', 'Amount of water After: {}'.format(self.water_amount_refill))
        else:
            module_logger.delay('debug', 'job Handler', 'REFILL CMD: {}'.format(dispense_command))
            command = commands[dispense_command]
            module = command['module']
            port = self.modules[module]['port']
            data = {'command': command, 'module': 'additive', 'port': port}

            res = pool.apply_async(write_to_module, [data, port])

            res = res.get(timeout=30)

            module_logger.delay('debug', 'Job Handler', 'Res is {}'.format(res))


    async def kiosk_client(self, event):
        message = event['message']
        topic = message['topic']
        status = message['status']

        module_logger.delay('debug', 'Job Handler', 'Received WS message: {}'.format(topic))

        if topic == 'refill_order':
            command = status
            module_logger.delay('debug', 'Job Handler', 'Received refill_order command: {}'.format(command))
            await self.channel_layer.send(
                'job_handler',
                {
                    'type': 'dispense',
                    'command': command
                }
            )
        elif topic == 'color_action':
            await self.channel_layer.send(
                'job_handler',
                {
                    'type': 'set_light_color',
                    'command': message
                }
            )

    async def set_light_color(self, event):

        button_selected = event['command']['status']

        module_logger.delay('debug', 'job Handler', 'Button color action: {}'.format(button_selected))
        command = {
            'name': 'color',
            'module': 'lighting',
            'command': COLOR_ACTIONS[button_selected]
        }
        module = command['module']
        port = self.modules[module]['port']
        data = {'command': command, 'module': 'lighting', 'port': port}

        res = pool.apply_async(write_to_module, [data, port, False])

        # res = res.get(timeout=30)
        #
        # module_logger.delay('debug', 'Job Handler', 'Res is {}'.format(res))

    async def get_modules_for_maintenance(self, event):
        task_name = event['task_name']

        exec_maintenance_task.delay(task_name, self.modules)
