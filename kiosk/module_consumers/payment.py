import websocket

try:
    import thread
except ImportError:
    import _thread as thread
import time


class Payment:
    def __init__(self):
        self.module_name = 'Payment'
        self.port = None
        self.module = None
        self.logger = None
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp("ws://192.168.86.31:8000/ws/kiosk_client/",
                                         on_open=self.on_open,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        self.ws.run_forever()

    @property
    def info(self):
        return 'Name: {}\nWS:{}'.format(self.module_name, self.ws)

    def on_open(self, ws):
        def run(*args):
            for i in range(3):
                time.sleep(1)
                ws.send('{"message": "TEST MSG."}')
            time.sleep(1)
            # ws.close()
            print("thread terminating...")

        thread.start_new_thread(run, ())

    def handle_incoming_message(self, message):
        type = message.type
        print('MSG type: {}'.format(type))
        if type == 'RES_ON_WS_INIT_REQUIRED':
            # connectToMiddleware()
            pass
        elif type == 'RES_ON_WS_INIT':
            # initWSMiddlewareWithConfig()
            pass
        elif type == 'RES_ON_WS_RESTART':
            # response sent after successfully restarting websocket
            pass
        elif type == 'RES_ON_MESSAGE':

            payload = {
                type: "RES_ON_MESSAGE",
                message: message.data.message
            }

            # processMessage(payload)
            pass
        elif type == 'RES_ON_SET_MODE':
            # response after setting SDK mode (unused by me)
            pass
        elif type == 'RES_ON_SET_LOG_LEVEL':
            # response after setting Log level (probably not going to use)
            pass
        elif type == 'RES_ON_SETTINGS_RETRIEVED':
            # initCardReaderDevice()
            pass
        elif type == 'RES_ON_DEVICE_CONNECTED':
            # processMessage
            pass
        elif type == 'RES_ON_SALE_RESPONSE':
            payload = {type: "RES_ON_SALE_RESPONSE",
                       message: message.data.saleResponse
                       }
            # winston.log("debug", "RES_ON_SALE_RESPONSE", message.data.saleResponse);
            # processMessage(payload);

            pass
        elif type == 'RES_ON_REFUND_RESPONSE':

            pass
        elif type == 'RES_ON_TRANSACTION_LIST_RESPONSE':

            pass
        elif type == 'RES_ON_DEVICE_ERROR' or type == 'RES_ON_ERROR' or type == 'RES_ON_WS_ERROR':

            # processMsg
            pass

    def on_message(self, ws, message):
        # self.handle_incoming_message(message)
        print(message)
        print('WS received message above.')

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        print("### closed ###")


if __name__ == '__main__':
    payment = Payment()

    print('This is payment. {}'.format(payment.info))
