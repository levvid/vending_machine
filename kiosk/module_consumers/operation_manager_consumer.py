import json
import serial
import time
import logging
from pegasus.celery import app
import json
import aioserial
import asyncio
import queue
import datetime
import logging
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer
from channels.consumer import SyncConsumer, AsyncConsumer

from pegasus.celery import write_task, read_task, connect_modules, post_order_complete, preauthorize_payment, \
    monitor_refill
from kiosk.jobs.jobs_config import jobs
from kiosk.jobs.operation import Operation
from kiosk.jobs.commands import commands
from kiosk.models import Inventory, Order, DrinkPrice, ModuleStatus, Kiosk, Additives

from pegasus.logger_config import get_logger

logger = get_logger('OperationManager')


class OperationManagerConsumer(AsyncConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = 'OperationManager'
        self.channel_name = 'operation_manager'
        self.current_operations = queue.Queue()
        self.current_operation = None

    async def queue_operation(self, event):
        operation_name = event['operation_name']

        try:
            operation_type = event['operation_type']
        except:
            operation_type = 'operation'

        logger.debug('starting operation: {}'.format(operation_name))

        new_operation = Operation(operation_name)

        if self.current_operation:
            # self.current_operations.put(self.current_operation)
            logger.info('There is a current operation in progress. Adding {} to operation queue.'.format(
                self.current_operation))
        else:
            pass

        # logger.debug('')
        self.current_operation = new_operation  # check first to make sure current job is not occupied

        logger.debug('Current operation {}'.format(self.current_operation.name))

        order_uuid = ''
        job_type = ''

        if operation_type == 'order':
            order_uuid = event['order_uuid']
            job_type = 'order'

        await self.channel_layer.send(
            'job_handler', {
                'type': 'queue_job',
                'job_name': self.current_operation.name,
                'job_type': job_type,
                'order_uuid': order_uuid
            }
        )

    async def operation_update(self, event):

        operation_name = event['operation_name']
        status = event['status']

        if status == 'K':
            logger.debug('operation {} completed successfully.'.format(operation_name))
            if not self.current_operations.empty():
                self.current_operation = self.current_operations.get()

                await self.channel_layer.send(
                    'job_handler', {
                        'type': 'queue_job',
                        'job_name': self.current_operation.name,
                    }
                )
            else:
                self.current_operation = None
        else:
            logger.error("An error has occurred while performing operation {}".format(operation_name))
            self.current_operation = None
            self.current_operations.queue.clear()
### Run parallel task. Save # of tasks required then loop thru them & send cmds. for any new task queued,
# make sure no conflicting task is running currently
