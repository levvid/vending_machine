import time

# import websocket
from channels.consumer import AsyncConsumer


def on_message(ws, message):
    print(message)
    print('message received: {}'.format(message))


def on_error(ws, error):
    print('an error has occurred.')
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    for i in range(3):
        time.sleep(1)
        ws.send('{"message": "TEST MSG {}"}'.format(i))
    # time.sleep(1)
    # ws.close()

    print('Opened this')


class PaymentConsumer(AsyncConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        print('In payment consumer')
        self.port = None
        self.module = None
        self.module_name = 'Payment'
        self.logger = None
        self.ws = None

    async def connect(self, event):
        print('Connected. to payment')
        # websocket.enableTrace(True)
        # self.ws = websocket.WebSocketApp('ws://192.168.86.31:8000/ws/kiosk_client/',
        #                                  on_open=on_open,
        #                                  on_message=on_message,
        #                                  on_error=on_error,
        #                                  on_close=on_close)

        print('Started WS connections: ')