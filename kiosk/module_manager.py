import multiprocessing
from kiosk.serial_process import SerialProcess
from kiosk.list_serial_ports import serial_ports

input_queue = multiprocessing.Queue()
output_queue = multiprocessing.Queue()


def start_modules():
    print("Starting modules")
    ports = serial_ports()

    for port in ports:
        sp = SerialProcess(input_queue, output_queue, port)
        sp.daemon = True
        sp.start()

    for port in ports:
        print('Adding to input_queue {}'.format(input_queue))
        input_queue.put('!')


def check_queue():
    print('Checking queue.')
    if not output_queue.empty():
        message = output_queue.get()
        print("Message is: {}".format(message))


def manager():
    print("Starting manager.")
    start_modules()

    # while True:
    #     print("Checking queue {}".format(output_queue))
    #     check_queue()
