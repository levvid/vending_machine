#### Set Up
1. Install python3-pip
    - `sudo apt install python3-pip`
1. Create a virtual environment
    - https://docs.python.org/3/tutorial/venv.html
    - `python3 -m venv <dropwater>`
    - Activate the virtual environment
        - `source <dropwater>/bin/activate
`
1. Navigate to the project's root directory and install requirements.
    - `pip install -r requirements.txt`
1. Install and start redis-server
    - `sudo apt update`
    - `sudo apt install redis-server`
    - `redis-server &`
    
1. Create the database `kioskdb` and user dropwater with the usual password.
1. Make and apply database migrations
    - `python manage.py makemigrations`
    - `python manage.py migrate`
1. To populate the database, run the script
   -  Start the django shell
      - `python manage.py shell`
      - `from kiosk.dummy_data.dummy_initial_data import dummy_data`
      - `dummy_data()`

1. Append `alias pip=pip3` at the end of your ~/.bashrc.
   - `source ~/.bashrc` 
   
1. Add user dropwater to both dialout and tty groups
    - `sudo adduser dropwater dialout`
    - `sudo adduser dropwater tty`

##### Logging
- `DEBUG`: Low level system information for debugging purposes
- `INFO`: General system information
- `WARNING`: Information describing a minor problem that has occurred.
- `ERROR`: Information describing a major problem that has occurred.
- `CRITICAL`: Information describing a critical problem that has occurred.

###### Making Logging calls
- `logger.debug()`
- `logger.info()`
- `logger.warning()`
- `logger.error()`
- `logger.critical()`
There are two other logging calls available:
- `logger.log()`: Manually emits a logging message with a specific log level.
- `logger.exception()`: Creates an ERROR level logging message wrapping the current exception stack frame.


#### RUNNING
##### DEVELOPMENT
1. Start the django server
   - `python manage.py runserver`
   - OR with port forwarding
      - `python manage.py runserver 0.0.0.0:8000`
###### On the console
1. Start celery workers
   - `celery -A pegasus worker -l DEBUG`
1. Start the module simulator
   - `python kiosk/utils/module_serial_port_simulator.py G D T R L A`
1. Start the module consumers
    - `python manage.py runworker job_handler operation_manager`
1. Read logs
    - `sudo supervisorctl tail -f celery`
    - `sudo supervisorctl tail -f pegasus`
    - `sudo supervisorctl tail -f pegasus-worker`
    - `sudo supervisorctl restart all`
    - `python -m kiosk.utils.module_identifier`
   
To read all logs
    - `tail -f ~/logs/pegasus/celery.out.log -f ~/logs/pegasus/pegasus.out.log -f ~/logs/pegasus/pegasus-worker.out.log`


##### PRODUCTION
###### Using SUPERVISOR
1. Start supervisor
    - `sudo supervisorctl start celery pegasus-worker pegasus_server`
1. Restart supervisor
    - `sudo supervisorctl start celery pegasus_consumers pegasus_server`
   
###### Using SYSTEMD - (deprecated)
1. Daemon Location - `/etc/systemd/system/<service-name>.service`
1. Status - `sudo systemctl status <service-name>`
1. Start - `sudo systemctl start <service-name>`
1. Restart - `sudo systemctl restart <service-name>`
1. Run at system boot - `sudo systemctl enable <service-name>`
1. Logs
    - Tail - `sudo journalctl -f -u <service-name>`

###### Service Names
1. ```pegasus```
1. ```pegasus-worker```
1. ```celery```
1. ```redis-server```
   
NOTE: After editing service files, run `sudo systemctl daemon-reload`
   

#### Kiosk API
1. Location
    - `<server_ip_address>:8000`
##### Calls
1. Order API
    - Command - `/api/order/create/`
    - Payload - `{ order_object }` 
1. Refill dispense API
    - Command - `/api/tasks/run/`
    - Payloads 
       - Start dispense -  `{ command: start_dispense }`
       - end dispense - `{ command: end_dispense }`
       - End order -  `{ command: end_order }`
   
#### Kiosk Websockets
1. Location
    - `ws://<server_IP_address>:8000/ws/kiosk_client/`
   
##### Message formats
1. Startup/Reset
```python
    {
        'message': {
           'topic': 'kiosk_status',
           'status': 'start_up_complete',
           'data': 'Start up complete'
        }
    }
```
       - States: `start_up_complete`, `start_up_failed`
1. Payment
```python
    {
        'message': {
            'topic': 'payment',
           'status': 'success',  # failed, retrying, processing...,
            'data': 'processing payment',  # payment succeeded, payment failed
        }
    }
```
    - Data: `preauthorization_failure`, `preauthorization_success`
    - Status: `success`, `failed`, `retrying`, `processing`
   
1. Order status
```python
    {
        'message': {
            'topic': 'order_status',
            'status': 'order_complete',
            'data': 'Order complete',
            'order_type': 'drop_bottle'
        }
    }
```
    - Status: `order_complete`, `door_open`, `order_failed`, `door_closed`
    - Order types: `drop_bottle`, `refill`
    - Data: 'Order complete', 'Door Open'
   
1. Refill Status
```python 
    { 
         'message': {
              'topic': 'refill_order_status',
              'status': 'refill_in_progress', # refill_timeout  - TBD
              'data': { 'water_amount': 30, 'price': 100 },  # amount - ounces, price - cents
              'order_type': 'refill'
         }
    }
```
   - Start/End dispense and/or order - from client

```python 
    {
       'message': {
           'topic': 'refill_order',
           'status': 'start_dispense',
       }
    }
```
      - Status: `start_dispense`, `end_dispense`, `end_order`
   

1. Number of bottles saved WS 

```python 
    { 
         'message': {
              'topic': 'number_of_bottles_saved',
              'status': 'number_of_bottles_update',
              'data': 500  # number of bottles saved
         }
    }
```



### Maintenance mode API endpoint - `api/maintenance/`
#### Payload - POST request

```json
{
   "taskname": <value>
}
```

#### TaskNames
```json
[
   {
      "value": "raise_tower_one",
      "display_name": "Raise Tower 1"
   },
   {
      "value": "raise_tower_two",
      "display_name": "Raise Tower 2"
   },
   {
      "value": "raise_tower_three",
      "display_name": "Raise Tower 3"
   },
   {
      "value": "lower_tower_one",
      "display_name": "Lower Tower 1"
   },
   {
      "value": "lower_tower_two",
      "display_name": "Lower Tower 2"
   },
   {
      "value": "lower_tower_three",
      "display_name": "Lower Tower 3"
   },
   {
      "value": "purge_additive_one_drop_bottle",
      "display_name": "Purge Additive 1 Drop Bottle"
   },
   {
      "value": "purge_additive_two_drop_bottle",
      "display_name": "Purge Additive 2 Drop Bottle"
   },
   {
      "value": "purge_additive_three_drop_bottle",
      "display_name": "Purge Additive 3 Drop Bottle"
   },
   {
      "value": "purge_additive_four_drop_bottle",
      "display_name": "Purge Additive 4 Drop Bottle"
   },
   {
      "value": "purge_additive_five_drop_bottle",
      "display_name": "Purge Additive 5 Drop Bottle"
   },
   {
      "value": "purge_additive_six_drop_bottle",
      "display_name": "Purge Additive 6 Drop Bottle"
   },
   {
      "value": "purge_additive_one_refill",
      "display_name": "Purge Additive 1 Refill"
   },
   {
      "value": "purge_additive_two_refill", 
      
      "display_name": "Purge Additive 2 Refill"
   },
   {
      "value": "purge_additive_three_refill",
      "display_name": "Purge Additive 3 Refill"
   },
   {
      "value": "purge_additive_four_refill",
      "display_name": "Purge Additive 4 Refill"
   },
   {
      "value": "purge_additive_five_refill",
      "display_name": "Purge Additive 5 Refill"
   },
   {
      "value": "purge_additive_six_refill",
      "display_name": "Purge Additive 6 Refill"
   },
   {
      "value": "flush_bottle_ambient",
      "display_name": "Flush Bottle (Ambient)"
   },
   {
      "value": "flush_bottle_cold",
      "display_name": "Flush Bottle (Cold)"
   },
   {
      "value": "flush_refill_ambient",
      "display_name": "Flush Refill Ambient"
   },
   {
      "value": "flush_refill_cold",
      "display_name": "Flush Refill Cold"
   },
   {
      "value": "open_gripper",
      "display_name": "Open Gripper"
   },
   {
      "value": "close_gripper",
      "display_name": "Close Gripper"
   },
   {
      "value": "open_reception_door",
      "display_name": "Open Reception Door"
   },
   {
      "value": "close_reception_door",
      "display_name": "Close Reception Door"
   }
]
```