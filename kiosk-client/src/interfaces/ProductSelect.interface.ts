
export interface IButtonSmallProps {
  groupId: string
  imageUrl: any
  bottleIcnUrl?: any
  refillIcnUrl?: any
  description: string
  caffeine_level?: number
  cost?: number
  type?: string
  direction?: string
  active: boolean
  activeClass: string
}

export interface FlavorSelectPageProps  {
  type: string
}
