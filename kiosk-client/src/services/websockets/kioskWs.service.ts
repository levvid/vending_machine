import { PrepareWs } from "../rxjs/PrepareWs.service";
import { PaymentWs } from "../rxjs/PaymentWs.service";
import { RefillPourWs } from "../rxjs/RefillPourWs.service";
import axios from "axios";
import { ColorSelectWs } from "../rxjs/ColorSelectWs.service";
// keep these values out here, there is only one connection

const socketUrl = "ws://localhost:8000/ws/kiosk_client/";
//const socketUrl = "ws://192.168.86.58:8000/ws/kiosk_client/";
//const socketUrl = "ws://localhost:8888/kiosk_client";
// const socketUrl = "ws://localhost:8888/kiosk_client";
let retryWait = 1;
let socket: any = null;

export function listenWebSocket() {
  // Create WebSocket connection.
  socket = new WebSocket(socketUrl);

  // Connection opened
  socket.onopen = () => {
    console.log("Websocket Connection Succefully Opened");
    retryWait = 1;

    // these might be temporary, API should initiate process
    PrepareWs.onMessage().subscribe((data: any) => {
      const command: string = data?.item;
      switch (command) {
        case "start_drop":
          console.log("sending command: " + command);
          //socket.send(command);
          break;
        default:
          break;
      }
    });

    PaymentWs.onMessage().subscribe((data: any) => {
      const command: string = data?.item;
      switch (command) {
        case "start_payment":
          console.log("sending command: " + command);
          //socket.send(command);

          break;
        default:
          break;
      }
    });

    ColorSelectWs.onMessage().subscribe((data: any) => {
      const value: string = data?.item;

      const bottleSelectMessage = {
        message: {
          topic: "color_action",
          status: value  
        },
      };
      let bottleSelectMsg = JSON.stringify(bottleSelectMessage);
      console.log(bottleSelectMsg);
      socket.send(bottleSelectMsg);
    });

    RefillPourWs.onMessage().subscribe((data: any) => {
      console.log("Sending refill pour message.");
      const command: string = data?.item;
      console.log("Command getting sent: " + command);
      switch (command) {
        case "begin_refill":
          //socket.send(command);
          break;
        case "end_refill":
          //socket.send(command);
          const endPourMessage = {
            message: {
              topic: "refill_order",
              status: "end_order",
            },
          };
          let endPourMsg = JSON.stringify(endPourMessage);
          console.log(endPourMessage);
          socket.send(endPourMsg);
          break;
        case "start_pour":
          //socket.send(command);
          const startPourMessage = {
            message: {
              topic: "refill_order",
              status: "start_dispense",
            },
          };
          let msg = JSON.stringify(startPourMessage);
          console.log(msg);
          socket.send(msg);
          break;
        case "stop_pour":
          const stopPourMessage = {
            message: {
              topic: "refill_order",
              status: "end_dispense",
            },
          };
          let stopPourMsg = JSON.stringify(stopPourMessage);
          socket.send(stopPourMsg);
          break;
        default:
          break;
      }
    });
  };

  // Listen for messages
  socket.onmessage = (event: any) => {
    try {
      const obj = JSON.parse(event.data);

      const topic = obj?.message?.topic;
      const status = obj?.message?.status;

      // Drop Water Order
      // send message to listening components
      if (topic === "order_status") {
        switch (status) {
          case "order_complete":
            PrepareWs.sendMessage("order_complete");
            break;
          case "door_open":
            PrepareWs.sendMessage("door_open");
            break;
          case "door_closed":
            PrepareWs.sendMessage("door_closed");
            break;
          case "order_failed":
            PrepareWs.sendMessage("order_failed");
            break;
        }
      } else if (topic === "payment") {
        switch (status) {
          case "processing":
            PaymentWs.sendMessage("payment_processing");
            break;
          case "success":
            PaymentWs.sendMessage("payment_success");
            break;
          default:
            console.log("ERROR: unknown payment status:" + status);
            break;
        }
      } else if (topic === "refill_order_status") {
        console.log("topic: " + topic)
        RefillPourWs.sendMessage(obj);
      }
    } catch (e) {
      console.log("ERROR: cannot JSON.parse: " + event.data);
      console.log(e);
    }
  };

  socket.onerror = (event: any) => {
    console.log("ERROR: Socket ERROR");
    console.log(event);
    setTimeout(() => {
      check();
    }, retryWait * 1000);
    retryWait = setRetryWait();
  };

  socket.onclose = (event: any) => {
    console.log("ERROR: Socket CLOSED");
    console.log(event);
    setTimeout(() => {
      check();
    }, retryWait * 1000);
    retryWait = setRetryWait();
  };
}

// progressivly back off socket connections
// after 2 minutes reset counter
function setRetryWait() {
  if (retryWait > 120) {
    return 1;
  }
  return retryWait * 2 + 1;
}

function check() {
  //check if websocket instance is closed, if so call `connect` function.
  if (!socket || socket.readyState === WebSocket.CLOSED) listenWebSocket();
}

// export const sendServerStartup = () => {
//     return axios
//         //.get('http://localhost:8000/api/start_up/' )
//         .get('http://192.168.86.58:8000/api/start_up/' )
//         .then((res:any) => {
//             console.log(res);
//         })
//         .catch((err:any) => {
//             console.log(err);
//         })
//
// }
