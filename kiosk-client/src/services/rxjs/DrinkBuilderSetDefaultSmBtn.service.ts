import { Subject } from 'rxjs';

const subject = new Subject();

export const DrinkBuilderSetDefaultSmBtn = {
    sendMessage: (item: any) => subject.next({ item }),
    clearMessages: () => subject.next(),
    onMessage: () => subject.asObservable()
};
