
  export const ingredients: any = { 
    "Guava":  {
      ingredients: 'Water, Natural Flavors, Malic Acid, Citric Acid, Sodium Citrate, Stevia Extract ',
      sodiumWt: '25',
      sodiumPct: '1'
    },
    "Lemonade": {
      ingredients: 'Water, Natural Flavors, Citric Acid, Malic Acid, Sodium Citrate, Stevia Extract',
      sodiumWt: '28',
      sodiumPct: '1'
    },
    "Cucumber": {
      ingredients: 'Water, Natural Flavors',
      sodiumWt: '0',
      sodiumPct: '0'
    },
    "Water": {
      ingredients: 'Water',
      sodiumWt: '0',
      sodiumPct: '0'
    }
  };

  export const getIngredients = (flavor: string) => {
    return ingredients[flavor];
  }

  // associate caffeine level, 0: 0, 2: 30, 3: 40, 4: 70
  export const caffeineText = ['0', '20', '40', '70'];

  export const getCaffeineText = (level: number): string => {
      return caffeineText[level];
  }