// flavors
import CucumIcn from "../../assets/images/cucumberIcn.svg";
import GuavaIcn from "../../assets/images/guavaIcn.svg";
import LemonIcn from "../../assets/images/lemonadeIcn.svg";
import WaterIcn from "../../assets/images/waterIcn.svg";
// caffeine
import Caffeine0 from "../../assets/images/icn-caffeine-0-mg.svg";
import Caffeine20 from "../../assets/images/icn-caffeine-20-mg.svg";
import Caffeine40 from "../../assets/images/icn-caffeine-40-mg.svg";
import Caffeine70 from "../../assets/images/icn-caffeine-70-mg.svg";

// compostable bottles with flavor
// just water
import CompostableIcn from "../../assets/images/CompostableIcn.svg";
// epmty bottle
import EmptyCompostableIcn from "../../assets/images/empty_compostable_water.svg";
// flavor bottles
import CucumberBoostCompostIcn from "../../assets/images/CucumberBoostCompostIcn.svg";
import GuavaBoostCompostIcn from "../../assets/images/GuavaBoostCompostIcn.svg";
import LemonBoostCompostIcn from "../../assets/images/LemonBoostCompostIcn.svg";

// refill bottles with flavor
import CucumberBoostRefillIcn from "../../assets/images/CucumberBoostRefillIcn.svg";
import GuavaBoostRefillIcn from "../../assets/images/GuavaBoostRefillIcn.svg";
import LemonBoostRefillIcn from "../../assets/images/LemonBoostRefillIcn.svg";
import ContainerRefill from "../../assets/images/ContainerRefill.svg";

// Caffeine Labels
import CaffeineSomeLabel from "../../assets/images/CaffeineSomeLabel.svg";
import CaffeineMoreLabel from "../../assets/images/CaffeineMoreLabel.svg";
import CaffeineExtraLabel from "../../assets/images/CaffeineExtraLabel.svg";

// water temp
import RoomTempIcn from "../../assets/images/icn-room-temp.svg";
import IceColdTempIcn from "../../assets/images/icn-ice-cold.svg";

// bottle logo
import bottleLogo from "../../assets/images/bottle-logo.svg";

import RefillLeftSlice from "../../assets/images/refill-left-slice.svg";

// misc icons
import Back from "../../assets/images/BackBtnIcn.svg";
import Exit from '../../assets/images/Exit.svg';
import InformationIcn from "../../assets/images/info.svg";
import ErrorBadge from "../../assets/images/error-badge.svg";

export const preloadImages = () => {
  let images = [];
  images.push(CucumIcn);
  images.push(GuavaIcn);
  images.push(LemonIcn);
  images.push(WaterIcn);
  images.push(Caffeine0);
  images.push(Caffeine20);
  images.push(Caffeine40);
  images.push(Caffeine70);
  images.push(CompostableIcn);
  images.push(EmptyCompostableIcn);
  images.push(CucumberBoostCompostIcn);
  images.push(GuavaBoostCompostIcn);
  images.push(LemonBoostCompostIcn);
  images.push(CucumberBoostRefillIcn);
  images.push(GuavaBoostRefillIcn);
  images.push(LemonBoostRefillIcn);
  images.push(ContainerRefill);
  images.push(CaffeineSomeLabel);
  images.push(CaffeineMoreLabel);
  images.push(CaffeineExtraLabel);
  images.push(Back);
  images.push(Exit);
  images.push(InformationIcn);

  images.forEach(image => {
    const i = new Image();
    i.src = image;
  })

}

export const mapTagToImage = (tag: string) => {
  switch (tag) {
    case "None | 0 MG":
      return Caffeine0;
    case "Some | 20 MG":
      return Caffeine20;
    case "More | 40 MG":
      return Caffeine40;
    case "Extra | 70 MG":
      return Caffeine70;
    case "Water":
      return WaterIcn;
    case "Guava":
      return GuavaIcn;
    case "Lemonade":
      return LemonIcn;
    case "Cucumber":
      return CucumIcn;
    case "room":
      return RoomTempIcn;
    case "ice":
      return IceColdTempIcn;
    case "refill":
      return ContainerRefill;
    case "drop_bottle":
      return CompostableIcn;
    case "empty_drop_bottle":
      return EmptyCompostableIcn;
    case "cucumber_bottle":
      return CucumberBoostCompostIcn;
    case "guava_bottle":
      return GuavaBoostCompostIcn;
    case "lemon_bottle":
      return LemonBoostCompostIcn;
    case "bottle_logo":
      return bottleLogo;
    case "refill-left-slice":
        return RefillLeftSlice;
    case "error-badge":
        return ErrorBadge;
    default:
      break;
  }
};

export const mapFlavorToDropBottle = (flavor: string) => {
  switch(flavor) {
    case "Water":
      return CompostableIcn;
    case "Guava":
      return GuavaBoostCompostIcn;
    case "Lemonade":
      return LemonBoostCompostIcn;
    case "Cucumber":
      return CucumberBoostCompostIcn;
    default:
      return CompostableIcn;
  }
};

export const mapFlavorToRefillBottle = (flavor: string) => {
  switch(flavor) {
    case "Water":
      return ContainerRefill;
    case "Guava":
      return GuavaBoostRefillIcn;
    case "Lemonade":
      return LemonBoostRefillIcn;
    case "Cucumber":
      return CucumberBoostRefillIcn;
    default:
      return CompostableIcn;
  };
}