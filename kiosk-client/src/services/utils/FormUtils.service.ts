
const isNum = new RegExp('^[0-9]+$');

export const isNumber = (value: string) => {
    if(isNum.test(value)) {
        return true;
    }
    return false;
}

export const backspace = (value: string) => {
    if (value.length > 1) {
      return value.substring(0, value.length - 2);
    } else {
      return '';
    }
  }
