export interface Iitem {
    description: string;
    quantity?: number;
    flavor?: string;
    flavorCost?: number;
    caffeine?: string;
    caffeineCost?: number;
    caffeine_level?: number;
    compostBottleCost?: number;
    totalCost?: number;
    temperature?: string;
    order_type?: string;
}

export interface Iflavor {
  flavor: string;
  flavorCost: number;
}

export interface Icaffeine {
  caffeine: string;
  caffeineCost: number;
}

export interface IcompostBottle {
  compostBottleCost: number;
}

export interface IaddItem {
  addItem: (item: Iitem) => void
}

export interface IaddFlavor {
  addFlavor: (item: Iitem) => void
}

export interface IaddCaffeine {
  addCaffeine: (item: Iitem) => void
}

export interface IaddCompostBottle {
  addCompostBottle: (item: IcompostBottle) => void
}

export interface IclearCart {
  clearCart: () => void
}
