//import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
//import { RootState, AppThunk } from '../store';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Iitem } from '../cart.interface';
import { RootState , AppThunk } from '../store';
import axios from 'axios';
import { getJSDocTemplateTag } from 'typescript';
import { initial } from 'lodash';

// Define the initial state using that type
const initialState: Iitem = {
    description: '',
    quantity: 0,
    flavor: '',
    flavorCost: 0,
    caffeine: '',
    caffeine_level: 0,
    caffeineCost: 0,
    compostBottleCost: 0,
    //temperature: 'room',
    temperature: 'ice',
    totalCost: 0,
    order_type: "refill"
    //order_type: "drop_bottle",
}

export interface IaddFlavorCost {
    flavor: string;
    flavorCost?: number;
}

export interface IaddCaffeineCost {
    caffeine: string;
    caffeineCost?: number;
    caffeine_level?: number;
}

export interface IaddBottleCost {
    compostBottleCost: number;
    order_type: string;
}

export interface IsetTemperatureLevel {
    temperature: string;
}

async function sendMakeOrder(cart: any) {
    const order = {
        caffeine_level: cart.caffeine_level,
        drink_price: Math.floor(cart.totalCost * 100),
        flavor: cart.flavor,
        kiosk_id: "M1-9",
        order_medium: "KIOSK",
        order_type: "drop_bottle",
        temperature: cart.temperature,
        water_amount: 16,
    };

    if (cart.order_type === 'drop_bottle') {
        order.order_type = 'Drop Bottle';
    } else {
        order.order_type = 'Screen Refill';
    }

    return axios
        //.post('http://192.168.86.58:8000/api/order/create/', order)
        .post('http://localhost:8000/api/order/create/', order)
        .then((res:any) => {
            console.log(res);
        })
        .catch((err:any) => {
            console.log(err);
        })
    
}

async function sendStartPourOrder(cart: any) {
    const order = {
        caffeine_level: cart.caffeine_level,
        drink_price: Math.floor(cart.totalCost * 100),
        flavor: cart.flavor,
        //flavor: "water",
        kiosk_id: "M1-9",
        order_medium: "KIOSK",
        order_type: "refill",
        //temperature: "room"
        temperature: cart.temperature,
    };

    return axios
        //.post('http://192.168.86.58:8000/api/order/create/', order)
        .post('http://localhost:8000/api/order/create/', order)
        .then((res:any) => {
            console.log(res);
        })
        .catch((err:any) => {
            console.log(err);
        })
    
}

export const makeOrder = (): AppThunk => (
    dispatch,
    getState
) => { 
    const cart = selectCart(getState());
    sendMakeOrder(cart);
};

export const startPourOrder = (): AppThunk => (
    dispatch,
    getState
) => { 
    const cart = selectCart(getState());
    sendStartPourOrder(cart);
};

async function pour(op: string) {
    const start = {
        task_name: "Start Dispense"
    };

    const stop = {
        task_name: "End Dispense"
    };

    const finish = {
        task_name: "End Dispense"
    };
    let order;

    switch (op) {
        case 'start':
            order = start;
            break;
        case 'stop':
            order = stop;
            break;
        case 'finish':
            order = finish
            break;
        default:
            order = finish;
    }

//     return axios
//         .post('http://localhost:8000/api/tasks/run/', order)
//         .then((res:any) => {
//             console.log(res);
//         })
//         .catch((err:any) => {
//             console.log(err);
//         })
}

export const startPour = (): AppThunk => (
    dispatch,
    getState
) => { 
    pour('start');
};

export const stopPour = (): AppThunk => (
    dispatch,
    getState
) => { 
    pour('stop');
};

export const finishPour = (): AppThunk => (
    dispatch,
    getState
) => { 
    pour('finish');
};


function cleanValue(value: any) {
    if (value === undefined) {
        return 0;
    }
    return value;
}

function getTotal(bottleCost: any, caffeineCost: any, flavorCost: any) {
    let bc: number = cleanValue(bottleCost);
    let cc: number = cleanValue(caffeineCost);
    let fc: number = cleanValue(flavorCost);
    return bc + cc + fc;
}

export const cartSlice = createSlice({
  name: 'cart',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
      addCompostBottleCost: (state, action: PayloadAction<IaddBottleCost>) => {
          state.compostBottleCost = action.payload.compostBottleCost;
          state.order_type = action.payload.order_type;
          state.totalCost = getTotal(state?.compostBottleCost, state?.caffeineCost, state?.flavorCost);
      },
      addFlavorCost: (state, action: PayloadAction<IaddFlavorCost>) => {
          state.flavorCost = action.payload.flavorCost;
          state.flavor = action.payload.flavor;
          state.totalCost = getTotal(state?.compostBottleCost, state?.caffeineCost, state?.flavorCost);
      },
      addCaffeineCost: (state, action: PayloadAction<IaddCaffeineCost>) => {
          state.caffeine = action.payload.caffeine;
          state.caffeineCost = action.payload.caffeineCost;
          state.caffeine_level = action.payload.caffeine_level;
          state.totalCost = getTotal(state?.compostBottleCost, state?.caffeineCost, state?.flavorCost);
      },
      setTemperature: (state, action: PayloadAction<IsetTemperatureLevel> ) => {
          state.temperature = action.payload.temperature;
      },
      resetState: (state) => {
        state.description = initialState.description;
        state.quantity = initialState.quantity;
        state.flavor = initialState.flavor;
        state.flavorCost = initialState.flavorCost;
        state.caffeine = initialState.caffeine;
        state.caffeine_level = initialState.caffeine_level;
        state.caffeineCost = initialState.caffeineCost;
        state.compostBottleCost = initialState.compostBottleCost;
        state.temperature = initialState.temperature;
        state.totalCost = initialState.totalCost;
        state.order_type = initialState.order_type;
      }
  },
})

export const { addCompostBottleCost, addFlavorCost, addCaffeineCost, setTemperature, resetState } = cartSlice.actions

export const selectCartTotal = (state: RootState) => state.cart.totalCost;
export const selectCart= (state: RootState) => state.cart;


export default cartSlice.reducer  