import React, { useState, useEffect } from "react";
import styles from "./Home.module.css";
import HomeAnim from "../../assets/videos/home-animation.mp4";
import ReactPlayer from "react-player";
import { useHistory } from "react-router-dom";
import Login from "../Maintenance/Login/Login";
import { HomeLoginService } from "../../services/rxjs/HomeLogin.service";
import { ReactComponent as WaterRefillIcn } from "../../assets/images/icn-water-refill.svg";
import ScanCard from "../ui/ScanCard";
import HomeFooter from "../ui/HomeFooter";
import { ColorSelectWs } from "../../services/rxjs/ColorSelectWs.service";
import BottlesSavedCard from "../ui/BottlesSavedCard";
import { resetState } from '../../services/redux/slices/cartSlice';
import { useAppDispatch } from '../../services/redux/hooks';



const Home: React.FC = () => {
  let history = useHistory();
  const dispatch = useAppDispatch();
  const [pressCount, setPressCount] = useState(0);
  const [blockClick, setBlockClick] = useState(false);
  const [showLoginForm, setShowLoginForm] = useState(false);
  const [refillPressed, setRefillPressed] = useState(false);

  useEffect(() => {
    ColorSelectWs.sendMessage("start_page");
    HomeLoginService.onMessage().subscribe((message: any) => {
      if (message.item === "hide") {
        setShowLoginForm(false);
        setBlockClick(false);
      }
    });

    dispatch(
      resetState()
    );
  });

  function blockClickEvent() {
    setBlockClick(true);
  }

  function clickHandler() {
    if (!blockClick) {
      ColorSelectWs.sendMessage("bottle_select");
      history.push("/bottle");
    }
  }

  function handleRefillPressed() {
    setRefillPressed(true);
  }

  function refillClickHandler() {
    ColorSelectWs.sendMessage("refill");
    history.push("/refill");
  }

  // for 5 taps
  setTimeout(() => {
    setPressCount(0);
  }, 5000);

  function toggleLoginForm(e: any) {
    e.stopPropagation();
    console.log(pressCount);
    if (pressCount > 4) {
      setShowLoginForm(true);
      setBlockClick(true);
    }
    setPressCount(pressCount + 1);
  }

  function hideLoginForm() {
    setShowLoginForm(false);
  }

  return (
    <div>
      <div
        className={styles.Home}
        data-testid="Home"
        onMouseUp={clickHandler}
        onTouchEnd={clickHandler}
      >
        <div
          className={styles.secretPad}
          onMouseUpCapture={toggleLoginForm}
          onTouchEndCapture={toggleLoginForm}
        ></div>
        {showLoginForm && (
          <div className={styles.loginPad} onClick={blockClickEvent}>
            <Login />
          </div>
        )}
        <div className={styles.playerWrap}>
          <ReactPlayer
            className={styles.player}
            url={HomeAnim}
            playing={true}
            loop={true}
            controls={false}
            width={1080}
            height={1920}
          />
        </div>

        <BottlesSavedCard />
        <ScanCard />
        <HomeFooter />
      </div>

      <div onClick={refillClickHandler} onMouseDownCapture={handleRefillPressed}>
        <div
          className={refillPressed ? styles.refillBtnPressed : styles.refillBtn}
        ></div>
        <WaterRefillIcn className={styles.waterRefillIcn} />
        <div className={styles.refillBtnLineOne}>Water Refill</div>
        <div className={styles.refillBtnLineTwo}>FREE</div>
      </div>
    </div>
  );
};

export default Home;
