import React, { useState, useEffect } from 'react';
import Modal from '@material-ui/core/Modal';
import styles from './InformationContent.module.css';
import Exit from '../../assets/images/Exit.svg';
import NutritionLabel from './NutritionLabel/NutritionLabel';
import { useAppSelector } from '../../services/redux/hooks';
import { selectCart } from '../../services/redux/slices/cartSlice';
import { mapFlavorToDropBottle, mapFlavorToRefillBottle } from '../../services/utils/Images.service';
import { getIngredients } from '../../services/utils/NutritionInformation.service';
import { NutritionModalService } from '../../services/rxjs/NutritionModal.service'

// Caffeine Labels 
import CaffeineSomeLabel from '../../assets/images/CaffeineSomeLabel.svg';
import CaffeineMoreLabel from '../../assets/images/CaffeineMoreLabel.svg';
import CaffeineExtraLabel from '../../assets/images/CaffeineExtraLabel.svg';

// default values, sets type in useState();
import CompostableIcn from '../../assets/images/CompostableIcn.svg';
import ContainerRefill from '../../assets/images/ContainerRefill.svg';

const InformationContent: React.FC = () => {
  const cart: any = useAppSelector(selectCart);
  let containertype = cart.order_type === 'drop_bottle' ? CompostableIcn: ContainerRefill;
  const [centerBottleImage, setCenterBottleImage] = useState(containertype);
  const [centerBottleLabel, setCenterBottleLabel] = useState(CaffeineSomeLabel);
  const [hasLabel, setHasLabel] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [closePressed, setClosePressed] = useState(false);
  const titleText = getFlavorCaffeineTitle();

  const ingredientsText = getIngredients(cart.flavor);

  function handleClosePressed() {
    setClosePressed(true);
  }

  function getFlavorCaffeineTitle() {
    if(cart.caffeine_level > 0) {
      return `${cart.flavor.toLowerCase()} + Caffeine`;
    } else {
      return cart.flavor;
    }
  }

  function setCenterImageLabel(level: number) {
    switch(level) {
      case 1:
        setCenterBottleLabel(CaffeineSomeLabel);
        setHasLabel(true);
        break;
      case 2:
        setCenterBottleLabel(CaffeineMoreLabel);
        setHasLabel(true);
        break;
      case 3:
        setCenterBottleLabel(CaffeineExtraLabel);
        setHasLabel(true);
        break;
      default:
        setHasLabel(false);
        break;
    }
  }

  function setCenterImage(type: string, refillIcn: any, bottleIcn: any) {
    if (type === 'refill') {
      setCenterBottleImage(refillIcn);
    } else {
      setCenterBottleImage(bottleIcn);
    }
  }


  const closeModal = () => {
    setClosePressed(false);
    setShowModal(false);
  }

  useEffect(() => {
    setCenterImageLabel(cart.caffeine_level)
    setCenterImage(cart.order_type, mapFlavorToRefillBottle(cart.flavor), mapFlavorToDropBottle(cart.flavor));

      const nutritionModalService = NutritionModalService.onMessage().subscribe((message: any) => {
        if (message.item === "Show") {
          setShowModal(true);
        }
      });


    return () => {
      // component cleanup
      nutritionModalService.unsubscribe();
    }
  })
  
  return (
    <div className={styles.InformationContent} data-testid="InformationContent">
      {showModal && <div className={styles.modalWrap}>
        <Modal open={showModal} onClose={closeModal}>
          <div className={styles.content}>

          <div className={ closePressed ? styles.exitPressed : styles.Exit} 
            onMouseDown={handleClosePressed}
            onMouseUp={closeModal}
            onTouchEnd={closeModal}
          >
            <div className={styles.ExitIcn}
              style={{
                backgroundImage: `url(${Exit})`
              }}
            ></div>
          </div>


          <div
            className={styles.CenterContainer}
            style={
              {
                backgroundImage: `url(${centerBottleImage})`,
              }}
          >
            {hasLabel && <div
              className={styles.CenterContainerLabel}
              style={
                {
                  backgroundImage: `url(${centerBottleLabel})`,
                }}
            > </div>}
          </div>


          <div className={styles.Nutrition}>
            <NutritionLabel />
          </div>
          <div className={styles.TextLabel}>
            {titleText}
          </div>
          <div className={styles.TextCalories}>
            Zero Calories
    </div>
          <div className={styles.TextSugar}>
            Zero Sugar
    </div>
          <div className={styles.TextIngredients}>
            Ingredients: {ingredientsText.ingredients}
          </div>
          </div>
        </Modal>
      </div>}
    </div>
)};

export default InformationContent;
