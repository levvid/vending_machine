import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import InformationContent from './InformationContent';

describe('<InformationContent />', () => {
  test('it should mount', () => {
    render(<InformationContent />);
    
    const informationContent = screen.getByTestId('InformationContent');

    expect(informationContent).toBeInTheDocument();
  });
});