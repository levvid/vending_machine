import React from 'react';
import s from './NutritionLabel.module.css';
import classNames from 'classnames';
import { useAppSelector } from '../../../services/redux/hooks';
import { selectCart } from '../../../services/redux/slices/cartSlice';
import { getIngredients, getCaffeineText } from '../../../services/utils/NutritionInformation.service';

const NutritionLabel: React.FC = () => {
  const cart: any = useAppSelector(selectCart);

  const nutritions = {
    "guava":
    {
      caloriesPerSving: '0',
      fatWt: '0',
      fatPct: '0',
      cholosterolWt: '0',
      cholosterolPct: '0',
      sodiumWt: '.1',
      sodiumPct: '25',
      carbWt: '0',
      carbPct: '0',
      fiberWt: '0',
      fiberPct: '0',
      sugarWt: '0',
      sugarPct: '0',
      proteinWt: '0',
      proteinPct: '0',
      vitDWt: '0',
      vitPct: '0',
      calciumWt: '0',
      calciumPct: '0',
      ironWt: '0',
      ironPct: '0',
      potassiumWt: '0',
      potassiumPct: '0',
      ingredients: 'Water, Natural Flavors, Citric Acid, Malic Acid, Sodium Citrate, Stevia Extract',
    },
    "lemonade":
    {
      caloriesPerSving: '0',
      fatWt: '0',
      fatPct: '0',
      cholosterolWt: '0',
      cholosterolPct: '0',
      sodiumWt: '.1',
      sodiumPct: '28',
      totalCarbWt: '0',
      totalCarbPct: '0',
      totalSugarWt: '0',
      totalSugarPct: '0',
      proteinWt: '0',
      proteinPct: '0',
      vitDWt: '0',
      vitDPct: '0',
      calciumWt: '0',
      calciumPct: '0',
      ironWt: '0',
      ironPct: '0',
      potassiumWt: '0',
      potassiumPct: '0',
      ingredients: 'Water, Natural Flavors, Citric Acid, Malic Acid, Sodium Citrate, Stevia Extract',
    }
  };
  //const nutrition = nutritions[props.flavor];
  // get from store, current flavor, caffeine
  const n = nutritions['guava'];

  const ingredients = getIngredients(cart.flavor);
  n.sodiumPct = ingredients.sodiumPct;
  n.sodiumWt = ingredients.sodiumWt;

  const caffeineAmt = getCaffeineText(cart.caffeine_level);



  // format classNames(base, X, Y, specificAttributes);
  // column One
  const nutritionFacts = classNames(s.base, s.colOneX, s.nutritionFacts);
  const colOneLineHeavy = classNames(s.base, s.colOneX, s.topLineY, s.lineHeavyColOne)
  const servingsPerContainer = classNames(s.base, s.colOneX, s.textLineOneY, s.servingsPerContainer);
  const servingSizeText = classNames(s.base, s.colOneX, s.lineTwoY, s.servingSizeText);
  const servingSizeAmt = classNames(s.base, s.servingSizeAmtX, s.lineTwoY, s.servingSizeAmt);
  const colOneLineLight = classNames(s.base, s.colOneX, s.colOneLineLightY, s.colOneLineLight);
  const caleriesPerServingText = classNames(s.base, s.colOneX, s.caloriesPerServingY, s.caloriesPerServingText);
  const caleriesPerServingAmt = classNames(s.base, s.caloriesPerServingAmtX, s.caloriesPerServingY, s.caleriesPerServingAmt);

  // column Two
  const dailyValue = classNames(s.base, s.colTwoX, s.dailyValueY, s.dailyValue);
  const colTwoHeaderAmt = classNames(s.base, s.colTwoX, s.colTwoHeaderY, s.colTwoHeader);
  const colTwoHeaderValue = classNames(s.base, s.colTwoHeaderValueX, s.colTwoHeaderY, s.colTwoHeader);
  const colTwoLineHeavyUp = classNames(s.base, s.colTwoX , s.topLineY, s.lineWideHeavy);

  const totalFatText = classNames(s.base, s.colTwoX, s.totalFatY, s.ingredientsTextBold);
  const totalFatWt = classNames(s.base, s.totalFatWtX, s.totalFatY, s.ingredientsText);
  const totalFatPct = classNames(s.base, s.colTwoPctX, s.totalFatPctY, s.ingredientsTextBoldLg);

  const colTwoLineLightOne = classNames(s.base, s.colTwoX, s.colTwoLineLightOneY, s.lineWide);

  const cholesterolText = classNames(s.base, s.colTwoX, s.cholesterolY,s.ingredientsTextBold);
  const cholesterolWt = classNames(s.base, s.cholesterolWtX, s.cholesterolY ,s.ingredientsText);
  const cholesterolPct = classNames(s.base, s.colTwoPctX ,s.cholesterolPctY , s.ingredientsTextBoldLg);

  const colTwoLineLightTwo = classNames(s.base, s.colTwoX, s.colTwoLineLightTwoY, s.lineWide);

  const sodiumText = classNames(s.base, s.colTwoX, s.sodiumY,s.ingredientsTextBold);
  const sodiumWt = classNames(s.base, s.sodiumWtX, s.sodiumY ,s.ingredientsText);
  const sodiumPct = classNames(s.base, s.colTwoPctX ,s.sodiumPctY , s.ingredientsTextBoldLg);

  const colTwoLineHeavyLow = classNames(s.base, s.colTwoX , s.lowerLineY, s.lineWideHeavy);

  const vitamins = classNames(s.base, s.colTwoX, s.vitaminsY, s.dailyValue);
  
  // column Three
  const colThreeHeaderAmt = classNames(s.base, s.colThreeX, s.colTwoHeaderY, s.colTwoHeader);
  const colThreeHeaderValue = classNames(s.base, s.colThreeHeaderValueX, s.colTwoHeaderY, s.colTwoHeader);
  const colThreeLineHeavyUp = classNames(s.base, s.colThreeX , s.topLineY, s.lineWideHeavy);


  const totalCarbText = classNames(s.base, s.colThreeX, s.totalFatY, s.ingredientsTextBold);
  const totalCarbWt = classNames(s.base, s.totalCarbWtX, s.totalFatY, s.ingredientsText);
  const totalCarbPct = classNames(s.base, s.colThreePctX, s.totalFatPctY, s.ingredientsTextBoldLg);

  const colThreeLineLightOne = classNames(s.base, s.colThreeX, s.colTwoLineLightOneY, s.lineWide);

  const totalSugarText = classNames(s.base, s.sugarsTextX, s.cholesterolY, s.ingredientsText);
  const totalSugarPct = classNames(s.base, s.colThreePctX, s.cholesterolPctY, s.ingredientsTextBoldLg);

  const colThreeLineLightTwo = classNames(s.base, s.colThreeX, s.colTwoLineLightTwoY, s.lineWideHeavy);

  const proteinText = classNames(s.base, s.colThreeX, s.sodiumY, s.ingredientsTextBold);
  const proteinWt = classNames(s.base, s.proteinWtX, s.sodiumY, s.ingredientsText);
  const proteinPct = classNames(s.base, s.colThreePctX, s.sodiumPctY, s.ingredientsTextBoldLg);

   /*
  const totalCarbText;
  const totalCarbAmt;
  const colThreeLineLightOne;
  const totalSugarText;
  const totalSugarAmt;
  const colThreeLineLightTwo;
  const proteinText;
  const proteinAmt;
  const colThreeLineHeavyLow;
  */
  

  return (
    <div className={s.NutritionLabel} data-testid="NutritionLabel">
      {/* Column One */}
      <div className={nutritionFacts}>
        Nutrition Facts
    </div>
      <div className={colOneLineHeavy}></div>
      <div className={servingsPerContainer}>
        1 serving per container
    </div>
      <div className={servingSizeText}>
        Serving Size
    </div>
      <div className={servingSizeAmt}>
        400mL
    </div>
      <div className={colOneLineLight}></div>
      <div className={caleriesPerServingText}>
        Calories Per serving
    </div>
      <div className={caleriesPerServingAmt}>
        {n.caloriesPerSving}
      </div>

      {/* Column Two  */}
    <div className={dailyValue}>
    *The % Daily Value (DV) tells you how much a nutrient in a serving contributes to a daily diet. 2,000 calories a day is used for general nutrition advice.
    </div>
    <div className={colTwoHeaderAmt}>
    Amount / Serving
    </div>
    <div className={colTwoHeaderValue}>
    % Daily Value*
    </div>
    <div className={colTwoLineHeavyUp}></div>
    <div className={totalFatText}>
    Total Fat
    </div>
    <div className={totalFatWt}>
      {n.fatWt}g
    </div>
    <div className={totalFatPct}>
      {n.fatPct}%
    </div>
    <div className={colTwoLineLightOne}></div>
    <div className={cholesterolText}>
    Cholesterol
    </div>
    <div className={cholesterolWt}>
    {n.cholosterolWt}mg  
    </div>
    <div className={cholesterolPct}>
    {n.cholosterolPct}%  
    </div>
    <div className={colTwoLineLightTwo}></div>
    <div className={sodiumText}>
    Sodium
    </div>
    <div className={sodiumWt}>
    {n.sodiumWt}mg  
    </div>
    <div className={sodiumPct}>
    {n.sodiumPct}%  
    </div>
    <div className={colTwoLineHeavyLow}></div>
    <div className={vitamins}>
    Vitamin D {n.vitDWt}mcg {n.vitPct}% • Calcium {n.calciumWt}mg {n.calciumPct}% • Iron {n.ironWt}mg {n.ironPct}% • Potassium {n.potassiumWt}mg {n.potassiumPct}%  • Caffeine {caffeineAmt}mg
    </div>

      {/* Column Three */}
    <div className={colThreeHeaderAmt}>
      Amount / Serving
    </div>
    <div className={colThreeHeaderValue}>
      % Daily Value*
    </div>
    <div className={colThreeLineHeavyUp}></div>
    <div className={totalCarbText}>
      Total Carbohydrate
    </div>
    <div className={totalCarbWt}>
      {n.carbWt}g
    </div>
    <div className={totalCarbPct}>
      {n.calciumPct}%
    </div>
    <div className={colThreeLineLightOne}></div>
    <div className={totalSugarText}>
    Total Sugars {n.sugarWt}g
    </div>
    <div className={totalSugarPct}>
    {n.sugarPct}%
    </div>
    <div className={colThreeLineLightTwo}></div>
    <div className={proteinText}>
      Protein
    </div>
    <div className={proteinWt}>
      {n.proteinWt}g
    </div>
    <div className={proteinPct}>
      {n.proteinPct}%
    </div>

    </div>
  )
};

export default NutritionLabel;
