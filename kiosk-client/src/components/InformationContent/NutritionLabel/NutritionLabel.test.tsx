import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import NutritionLabel from './NutritionLabel';

describe('<NutritionLabel />', () => {
  test('it should mount', () => {
    render(<NutritionLabel />);
    
    const nutritionLabel = screen.getByTestId('NutritionLabel');

    expect(nutritionLabel).toBeInTheDocument();
  });
});