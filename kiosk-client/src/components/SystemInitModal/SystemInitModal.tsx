import React, { useState, useEffect } from "react";
import styles from "./SystemInitModal.module.css";
import Modal from "@material-ui/core/Modal";
import { CircularProgress } from "@material-ui/core";

const SystemInitModal: React.FC = () => {
  const [showModal, setShowModal] = useState(true);
  const [intervalHandle, setIntervalHandle] = useState(0);
  const [progress, setProgress] = React.useState(0);

  const stopCounter = () => {
    clearInterval(intervalHandle);
    setIntervalHandle(0);
  };

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress > 1200 ? 0 : prevProgress + 2 
      );
    }, 50);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className={styles.IdleUserModal}>
      {showModal && (
        <div className={styles.modalWrap}>
          <Modal open={showModal}>
            <div className={styles.content}>
              <div className={styles.dialogTopBackground}> </div>
              <div className={styles.dialBackground}>
                <CircularProgress
                  className={styles.progress}
                  size="90"
                  variant="indeterminate"
                  value={progress}
                  color="primary"
                />
              </div>

              <div className={styles.titleText}>Starting Up...</div>

              {(progress % 400 >= 0 && progress % 400 <= 60) && (
                <div className={styles.copyText1}>Acquiring satellites</div>
              )}
              {(progress % 400 > 60 && progress % 400 <= 180) && (
                <div className={styles.copyText1}>
                  Atomizing flavor molecules
                </div>
              )}
              {(progress % 400 > 180 && progress % 400 <= 310) && (
                <div className={styles.copyText1}>
                  Optimizing thermal expansion
                </div>
              )}
              {(progress % 400 > 310 && progress % 400 <= 400) && (
                <div className={styles.copyText1}>
                  Calculating compression coefficient
                </div>
              )}
            </div>
          </Modal>
        </div>
      )}
    </div>
  );
};

export default SystemInitModal;
