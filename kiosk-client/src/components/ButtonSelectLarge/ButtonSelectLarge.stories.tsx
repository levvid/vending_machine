/* eslint-disable */
import React from 'react';
import { storiesOf } from '@storybook/react';
import ButtonSelectLarge from './ButtonSelectLarge';
import  ContainerCompostable from '../../assets/images/ContainerCompostable.svg';

storiesOf('ButtonSelectLarge', module)
    .add('default', () => <ButtonSelectLarge 
                            imageUrl={ContainerCompostable}
                            title={'COMPOSTABLE'}
                            type='bottle'
                            description={'I need a bottle'}
                            handleClick={'any'}
                            textJustify='left'
                            cost='+ $2.50'
                          />);
