import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ButtonSelectLarge from './ButtonSelectLarge';
import  ContainerCompostable from '../../assets/images/ContainerCompostable.svg';

describe('<ButtonSelectLarge />', () => {
  test('it should mount', () => {
    render(<ButtonSelectLarge imageUrl={ContainerCompostable}/>);
    
    const buttonSelectLarge = screen.getByTestId('ButtonSelectLarge');

    expect(buttonSelectLarge).toBeInTheDocument();
  });
});