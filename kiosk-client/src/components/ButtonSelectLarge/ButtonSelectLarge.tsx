import React, { useState } from 'react';
import styles from './ButtonSelectLarge.module.css';
import { useHistory } from "react-router-dom";
import classNames from 'classnames';

interface BottleSelectProps {
  imageUrl: any
  title: string
  type: string
  description: string
  handleClick: any
  textJustify: string
  cost: string
}

  const ButtonSelectLarge: React.FC<BottleSelectProps> = ({imageUrl, title, type, description, handleClick, textJustify, cost}) => {
  let history = useHistory();
  const [border, setBorder] = useState(classNames(styles.border));
  const [buttonSelectLarge, setButtonSelectLarge] = useState(classNames(styles.ButtonSelectLarge, styles.ButtonSelectLargeShadow));

  function handleButtonFocus() {
    console.log('XDEBUG should highlight');
    setBorder(classNames(styles.border, styles.borderHighlight));
    setButtonSelectLarge(classNames(styles.ButtonSelectLarge));
  }

  function NavigateTo() {
    setBorder(classNames(styles.border));
    setButtonSelectLarge(classNames(styles.ButtonSelectLarge, styles.ButtonSelectLargeShadow));
    handleClick(type);
    history.push({
      pathname: '/builder',
      search: '',
      state: { type: type}
    });
  }
  
    if (textJustify === 'right') {
      return (
        <div className={buttonSelectLarge} data-testid="BtnLarge"
          onMouseDown={handleButtonFocus}
          onTouchStart={handleButtonFocus}
          onMouseUp={NavigateTo}
          onTouchEnd={NavigateTo}
        >
            <div className={styles.backgroundImage}
              style={{ backgroundImage: `url(${imageUrl})` }}
            >
              <div className={styles.titleRight}>
                {title}
              </div>
              <div className={styles.descriptionRight}>
                {description}
              </div>
              <div className={styles.costRight}>
                {cost}
              </div>
          </div>
          <div className={border}></div>
        </div>
      )
    } else {
      return (
        <div className={buttonSelectLarge} data-testid="BtnLarge"
          onMouseDown={handleButtonFocus}
          onTouchStart={handleButtonFocus}
          onMouseUp={NavigateTo}
          onTouchEnd={NavigateTo}
        >
          <div className={styles.backgroundImage}
            style={{ backgroundImage: `url(${imageUrl})` }}
          >
            <div className={styles.titleLeft}>
              {title}
            </div>
            <div className={styles.descriptionLeft}>
              {description}
            </div>
            <div className={styles.costLeft}>
              {cost}
            </div>
          </div>
          <div className={border}></div>
        </div>
      )
    }
}

export default ButtonSelectLarge;
