import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CancelButton from './CancelButton';

describe('<CancelButton />', () => {
  test('it should mount', () => {
    render(<CancelButton />);
    
    const backButton = screen.getByTestId('CancelButton');

    expect(backButton).toBeInTheDocument();
  });
});
