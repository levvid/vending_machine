import React from 'react';
import styles from './CancelButton.module.css';
import Exit from '../../../assets/images/Exit.svg';
import { useHistory } from "react-router-dom";
import { CancelModalBtn } from '../../../services/rxjs/CancelModalBtn.service';


const CancelButton: React.FC = () => {
  const history = useHistory();

  function handleClick() {
    console.log("XDEBUG Cancel Button click")
    CancelModalBtn.sendMessage('openModal`');
    /*
    history.push({
      pathname: '/start',
    });
    */
  }

  return (
  <div className={styles.CancelButton}
        onMouseUp={handleClick}
        onTouchEnd={handleClick}
  >
  <div className={styles.CancelButtonIcn} 
    style={{ 
              backgroundImage: `url(${Exit})`
            }}
    data-testid="CancelButton"
  >
  </div>
  <div className={styles.CancelBtnText}>CANCEL</div>

  </div>

  )
};

export default CancelButton;
