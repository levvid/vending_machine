import React from "react";
import styles from "./BackButton.module.css";
import Back from "../../../assets/images/BackBtnIcn.svg";
import { useHistory } from "react-router-dom";

const BackButton: React.FC = () => {
  const history = useHistory();

  function handleClick() {
    console.log("XDEBUG Back Button click");
    history.goBack();
  }

  return (
    <div className={styles.BackButton} 
         onMouseUp={handleClick}
         onTouchEnd={handleClick}>
      <div
        className={styles.BackButtonIcn}
        style={{
          backgroundImage: `url(${Back})`,
        }}
        data-testid="BackButton"
      ></div>
      <div className={styles.BackBtnText}>BACK</div>
    </div>
  );
};

export default BackButton;
