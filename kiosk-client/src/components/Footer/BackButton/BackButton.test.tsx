import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BackButton from './BackButton';

describe('<BackButton />', () => {
  test('it should mount', () => {
    render(<BackButton />);
    
    const backButton = screen.getByTestId('BackButton');

    expect(backButton).toBeInTheDocument();
  });
});
