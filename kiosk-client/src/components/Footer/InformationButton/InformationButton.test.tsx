import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import InformationButton from './InformationButton';

describe('<InformationButton />', () => {
  test('it should mount', () => {
    render(<InformationButton />);
    
    const infoButton = screen.getByTestId('InformationButton');

    expect(infoButton).toBeInTheDocument();
  });
});
