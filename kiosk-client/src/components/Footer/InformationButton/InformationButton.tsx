import React from "react";
import styles from "./InformationButton.module.css";
import InformationIcn from "../../../assets/images/info.svg";
import { NutritionModalService } from "../../../services/rxjs/NutritionModal.service";

const InformationButton: React.FC = () => {

  function handleClick() {
    console.log("XDEBUG InformationButton click");
    NutritionModalService.sendMessage('Show')
  }

  return (
    <div className={styles.InformationButton} 
         onMouseUp={handleClick}
         onTouchEnd={handleClick}>
      <div
        className={styles.InformationButtonIcn}
        style={{
          backgroundImage: `url(${InformationIcn})`,
        }}
        data-testid="InformationButton"
      ></div>
      <div className={styles.InformationBtnText}>NUTRITION</div>
    </div>
  );
};

export default InformationButton;
