import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CartSum from './CartSum';

describe('<CartSum />', () => {
  test('it should mount', () => {
    render(<CartSum />);
    
    const infoButton = screen.getByTestId('CartSum');

    expect(infoButton).toBeInTheDocument();
  });
});
