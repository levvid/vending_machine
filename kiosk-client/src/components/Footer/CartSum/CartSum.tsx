import React, { useEffect, useState } from "react";
import styles from "./CartSum.module.css";
import { useAppSelector } from '../../../services/redux/hooks';
import { selectCartTotal, selectCart } from '../../../services/redux/slices/cartSlice';

const CartSum: React.FC = () => {
  const cost = useAppSelector(selectCartTotal);
  const cart: any = useAppSelector(selectCart);
  const [perOzText, setPerOzText] = useState('');

  useEffect(() => {
    if(cart.order_type === 'refill') {
      setPerOzText(' / 16oz');
    }
  }, [])

  return (
    <div className={styles.CartSum}>
      <div
        className={styles.CartSum}
        data-testid="CartSum"
      ></div>
      <div className={styles.amount}>
        ${cost?.toFixed(2)}{perOzText}
      </div>
      <div className={styles.CartSumText}>PRICE TO PAY</div>
    </div>
  );
};

export default CartSum;
