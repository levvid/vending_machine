import React, { useState } from "react";
import styles from "./Footer.module.css";
import CancelButton from "./CancelButton/CancelButton";
import BackButton from "./BackButton/BackButton";
import InformationButton from "./InformationButton/InformationButton";
import CartSum from "./CartSum/CartSum";

interface FooterProps {
  showBackIcn?: boolean;
  showInfoIcn?: boolean;
  showCartSum?: boolean;
  showCancelIcn?: boolean;
}
const Footer: React.FC<FooterProps> = (props) => {
  const [cancelPressed, setCancelPressed] = useState(false);
  const [showInfoPressed, setShowInfoPressed] = useState(false);
  const [backButtonPressed, setBackButtonPressed] = useState(false);

  function handleCancelPressed() {
    setCancelPressed(true);
  }

  function handleCancelLifted() {
    setCancelPressed(false);
  }

  function handleShowInfoPressed() {
    setShowInfoPressed(true);
  }

  function handleShowInfoLifted() {
    setShowInfoPressed(false);
  }

  function handleBackButtonPressed() {
    setBackButtonPressed(true);
  }

  function handleBackButtonLifted() {
    setBackButtonPressed(false);
  }

  let showBackIcn = false;
  let showInfoIcn = false;
  let showCartSum = false;
  let showCancelIcn = true;

  if (props.showBackIcn === true) {
    showBackIcn = true;
  }
  if (props.showInfoIcn === true) {
    showInfoIcn = true;
  }
  if (props.showCartSum === true) {
    showCartSum = true;
  }
  if (props.showCancelIcn === false) {
    showCancelIcn = false;
  }

  return (
    <div className={styles.Footer} data-testid="Footer">
      {showBackIcn && (
        <div
          className={
            backButtonPressed ? styles.backButtonPressed : styles.BackButtonWrap
          }
          onMouseDown={handleBackButtonPressed}
          onMouseOut={handleBackButtonLifted}
          onMouseUp={handleBackButtonLifted}
        >
          <BackButton />
        </div>
      )}
      {showInfoIcn && (
        <div
          className={
            showInfoPressed
              ? styles.showInfoPressed
              : styles.InformationButtonWrap
          }
          onMouseDown={handleShowInfoPressed}
          onMouseOut={handleShowInfoLifted}
          onMouseUp={handleShowInfoLifted}
        >
          <InformationButton />
        </div>
      )}
      {showCartSum && (
        <div className={styles.CartSumWrap}>
          <CartSum />
        </div>
      )}
      {showCancelIcn && (
        <div
          className={
            cancelPressed ? styles.cancelButtonPressed : styles.CancelButtonWrap
          }
          onMouseDown={handleCancelPressed}
          onMouseOut={handleCancelLifted}
          onMouseUp={handleCancelLifted}
        >
          <CancelButton />
        </div>
      )}
    </div>
  );
};

export default Footer;
