import React, { useState } from 'react';
import styles from './RowBtnSelSmall.module.css';
import ButtonSelectSmall from '../ButtonSelectSmall/ButtonSelectSmall';
import { IButtonSmallProps } from '../../interfaces/ProductSelect.interface';
import { isConstructorDeclaration, setSourceMapRange } from 'typescript';
import { render } from '@testing-library/react';

//const RowBtnSelSmall: React.FC<any> = (props: any) =>{
export class  RowBtnSelSmall extends React.Component<any> {
  list: IButtonSmallProps[] = this.props.list;
  curStyle = styles.RowBtnSelColumn;

  constructor(props: any) {
    super(props);
    if (props.rowDirection === 'row') {
      this.curStyle = styles.RowBtnSelRow;
    }
  }

  render() {
  return (
      <div  className={this.curStyle} 
            data-testid="RowBtnSelSmall"
      >
          {this.list.map((elt: IButtonSmallProps, idx)  =>
            <ButtonSelectSmall
              item={elt}
              key={idx}
              textJustify={this.props.textJustify}
            />
          )}
      </div>
  )}
}


export default RowBtnSelSmall;
