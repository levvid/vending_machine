import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import RowBtnSelSmall from './RowBtnSelSmall';

describe('<RowBtnSelSmall />', () => {
  test('it should mount', () => {
    render(<RowBtnSelSmall />);
    
    const rowBtnSelSmall = screen.getByTestId('RowBtnSelSmall');

    expect(rowBtnSelSmall).toBeInTheDocument();
  });
});