import React from "react";
import outOfOrder_img from "../../assets/images/out-of-order.png";
import styles from "./OutOfOrder.module.css";

const OutOfOrder: React.FC = () => {
  return (
    <div className={styles.OutOfOrder}>
      <img
        src={outOfOrder_img}
        className={styles.outOfOrderImg}
        alt="Out Of Order Img"
      />
      <div className={styles.titleText}>Out of Order</div>
      <div className={styles.copyText}>Back soon, sorry for any inconvenience</div>
    </div>
  );
};

export default OutOfOrder;
