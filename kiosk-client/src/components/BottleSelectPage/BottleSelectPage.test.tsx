import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BottleSelectPage from './BottleSelectPage';

describe('<BottleSelectPage />', () => {
  test('it should mount', () => {
    render(<BottleSelectPage />);
    
    const bottleSelectPage = screen.getByTestId('BottleSelectPage');

    expect(bottleSelectPage).toBeInTheDocument();
  });
});