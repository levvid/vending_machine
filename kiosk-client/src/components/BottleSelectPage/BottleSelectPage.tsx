import React, { useState, useEffect } from "react";
import styles from "./BottleSelectPage.module.css";
import ButtonSelectLarge from "../ButtonSelectLarge/ButtonSelectLarge";
import DropBottleBackground from "../../assets/images/LargeBtnSelectDropBottleBackground.svg";
import ContainerRefillBackground from "../../assets/images/LargeBtnSelectRefillBackground.svg";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import { useAppDispatch } from "../../services/redux/hooks";
import { addCompostBottleCost } from "../../services/redux/slices/cartSlice";
import CancelOrderModal from "../QuitModals/CancelOrderModal/CancelOrderModal";
import IdleUserModal from "../QuitModals/IdleUserModal/IdleUserModal";
import { ColorSelectWs } from "../../services/rxjs/ColorSelectWs.service";
import axios from "axios";

const BottleSelectPage: React.FC = () => {
  let dispatch = useAppDispatch();
  const [isOutOfStock, setIsOutOfStock] = useState(false);

  useEffect(() => {
    axios
      //.get("http://192.168.86.58:8000/api/inventory/")
      .get('http://localhost:8000/api/inventory/' )
      .then((res: any) => {
        console.log(res);
        let obj = res?.data[0];
        console.log(obj);
        console.log(obj.tower_two_remaining);
        console.log(typeof obj.tower_two_remaining);
        console.log(!(obj.tower_two_remaining > 0));
        let totalBottlesRemaining =
          obj.tower_one_remaining +
          obj.tower_two_remaining +
          obj.tower_three_remaining;
        //totalBottlesRemaining = 0;
        let bottlesOutOfStock = totalBottlesRemaining < 1;
        console.log("bottlesOutOfStock: " + bottlesOutOfStock);
        if (bottlesOutOfStock) {
          setIsOutOfStock(true);
        }
      })
      .catch((err: any) => {
        console.log(err);
      });

    axios
      //.get("http://192.168.86.58:8000/api/modulestatus/")
      .get("http://localhost:8000/api/modulestatus/")
      .then((res: any) => {
        console.log(res);
        let obj = res?.data[0];
        console.log(obj);
        let dropBottleStatusGood = obj.drop_bottle_status === "On";
        console.log("dropBottleStatusGood: " + dropBottleStatusGood);
        let refillStatusGood = obj.refill_status === "On";
        console.log("refillStatusGood: " + refillStatusGood);
        if (!dropBottleStatusGood || !refillStatusGood) {
          setIsOutOfStock(true);
        }
      })
      .catch((err: any) => {
        console.log(err);
      });
  }, []);

  function handleSelect(v: string) {
    console.log(v);
    let cost = 0;
    if (v === "drop_bottle") {
      cost = 1.5;
    }
    ColorSelectWs.sendMessage(v.toLowerCase());
    dispatch(
      addCompostBottleCost({
        compostBottleCost: cost,
        order_type: v,
      })
    );
  }

  return (
    <div
      className={styles.BottleSelectPage}
      data-testid="BottleSelectPage"
      style={{
        height: 1920,
        width: 1080,
        position: "relative",
      }}
    >
      <div className={styles.LogoHeader}>
        <Header />
      </div>

      <div className={styles.DropBottleButton}>
        <ButtonSelectLarge
          imageUrl={DropBottleBackground}
          title={"NEED A BOTTLE?"}
          type="drop_bottle"
          description={"USE OUR 100% COMPOSTABLE CARTON"}
          cost={"+ $2.50"}
          handleClick={handleSelect}
          textJustify="right"
        />
        {isOutOfStock && <div className={styles.outOfStock}>OUT OF STOCK</div>}
      </div>
      <div className={styles.lineLeft}></div>
      <div className={styles.centerText}>OR</div>
      <div className={styles.lineRight}></div>
      <div className={styles.RefillButton}>
        <ButtonSelectLarge
          imageUrl={ContainerRefillBackground}
          title={"GOT A BOTTLE?"}
          description={"REFILL YOUR OWN REUSABLE BOTTLE"}
          cost={"+ $0.00"}
          type="refill"
          handleClick={handleSelect}
          textJustify="left"
        />
      </div>

      <div className={styles.footerLine}></div>
      <div className={styles.FooterWrap}>
        <Footer showBackIcn={true} showCancelIcn={false}/>
      </div>
      <IdleUserModal />
      <CancelOrderModal />
    </div>
  );
};

export default BottleSelectPage;
