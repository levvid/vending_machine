import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CartDisplayItem from './CartDisplayItem';

describe('<CartDisplayItem />', () => {
  test('it should mount', () => {
    render(<CartDisplayItem />);
    
    const cartDisplayItem = screen.getByTestId('CartDisplayItem');

    expect(cartDisplayItem).toBeInTheDocument();
  });
});