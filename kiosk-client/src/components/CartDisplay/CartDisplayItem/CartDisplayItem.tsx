import React from 'react';
import styles from './CartDisplayItem.module.css';

export interface Iparams {
  image?: string;
  description?: string;
  cost?: number;
  showText: boolean;
}

const CartDisplayItem: React.FC<Iparams> = (props: Iparams) => {
  
  return (
  <div className={styles.CartDisplayItem} data-testid="CartDisplayItem">
    <div className={styles.flexWrap}>
      <div className={styles.iconContainer}
        style={
          {
            backgroundImage: `url(${props.image})`,
          }}
      ></div>
      { props.showText && <div className={styles.description}>
        {props.description}
      </div> }
      { props.showText && <div className={styles.cost}>
        +${props.cost?.toFixed(2)}
      </div> }
    </div>
  </div>
)};

export default CartDisplayItem;
