/* eslint-disable */
import React from 'react';
import { storiesOf } from '@storybook/react';
import CartDisplay from './CartDisplay';

storiesOf('CartDisplay', module).add('default', () => <CartDisplay 
        direction='row'
        showText={true}
        />);
