import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CartDisplay from './CartDisplay';

describe('<CartDisplay />', () => {
  test('it should mount', () => {
    render(<CartDisplay />);
    
    const cartDisplay = screen.getByTestId('CartDisplay');

    expect(cartDisplay).toBeInTheDocument();
  });
});