import React, { useState, useEffect } from 'react';
import styles from './CartDisplay.module.css';
import { useAppSelector } from '../../services/redux/hooks';
import { selectCart } from '../../services/redux/slices/cartSlice';
import { mapTagToImage } from '../../services/utils/Images.service';
import CartDisplayItem from './CartDisplayItem/CartDisplayItem';
import classNames from 'classnames';

export interface Iprops {
  direction: string;
  showText: boolean;
}

const CartDisplay: React.FC<Iprops> = (props: Iprops) => {
  const cart: any = useAppSelector(selectCart);
  const bottleIcn = mapTagToImage(cart?.order_type);
  const flavorIcn = mapTagToImage(cart?.flavor);
  const caffeineIcn = mapTagToImage(cart?.caffeine);
  const tempIcn = mapTagToImage(cart?.temperature);
  const title = (cart.order_type === 'drop_bottle') ? 'Compostable': 'Refill';
  const [directionClass, setDirectionClass] = useState('row');
  let newClass;

  useEffect(() => {
    if(props.direction === 'row') {
      newClass = classNames(styles.flexWrap, styles.flexRow);
    } else {
      newClass = classNames(styles.flexWrap, styles.flexColumn);
    }
    setDirectionClass(newClass);
  });
  
  return (
  <div className={styles.CartDisplay} data-testid="CartDisplay">
    <div className={directionClass}>

      <div className={styles.item}>
        <CartDisplayItem
          image={bottleIcn}
          description={title}
          cost={cart.compostBottleCost}
          showText={props.showText}

        ></CartDisplayItem>
      </div>
      <div className={styles.item}>
        <CartDisplayItem
          image={flavorIcn}
          description={cart.flavor}
          cost={cart.flavorCost}
          showText={props.showText}
        ></CartDisplayItem>
      </div>
      <div className={styles.item}>
        <CartDisplayItem
          image={caffeineIcn}
          description={cart.caffeine}
          cost={cart.caffeineCost}
          showText={props.showText}
        ></CartDisplayItem>
      </div>
      <div className={styles.item}>
        <CartDisplayItem
          image={tempIcn}
          description={cart.temperature}
          cost={0}
          showText={props.showText}
        ></CartDisplayItem>
      </div>
    </div>

    
  </div>
)};

export default CartDisplay;
