import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DropPrepare from './DropPrepare';

describe('<DropPrepare />', () => {
  test('it should mount', () => {
    render(<DropPrepare />);
    
    const dropPrepare = screen.getByTestId('DropPrepare');

    expect(dropPrepare).toBeInTheDocument();
  });
});