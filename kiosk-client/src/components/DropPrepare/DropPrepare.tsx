import React, { useEffect, useState } from 'react';
import ReactPlayer from 'react-player'
import Modal from '@material-ui/core/Modal';
import styles from './DropPrepare.module.css';
import { useHistory } from "react-router-dom";
import Header from '../Header/Header';
import PourVideo from '../../assets/videos/pour.gif';
import BlobVideo from '../../assets/videos/blob.webm';
import RippleVideo from '../../assets/videos/ripple.gif';
import PourWaterLower from '../../assets/images/water.svg';
import CartDisplay from '../CartDisplay/CartDisplay';
import ErrorModal from '../ErrorModal/ErrorModal';
import { PrepareWs } from '../../services/rxjs/PrepareWs.service';

const DropPrepare: React.FC = () => {
  const history = useHistory();
  const [showErrorModal, setShowErrorModal] = useState(false);
  
  useEffect(() => {
    // TODO: remove this when live
    // send message to web socket start prepare
    PrepareWs.sendMessage('start_drop');

    // messages from web socket
    const prepareWs = PrepareWs.onMessage().subscribe((message: any) => {
      if(message.item === 'order_complete') {
        history.push({
          pathname: '/preparestatus',
        })
      } else if(message.item === 'order_failed') {
        setShowErrorModal(true);
      }
    })

    return () => {
      prepareWs.unsubscribe();
    }
  }, [])

  function handleClick() {
    history.push({
      pathname: '/preparestatus',
    })
  }

  return (
  <div className={styles.DropPrepare} data-testid="DropPrepare"
    onMouseUp={handleClick}
    onTouchEnd={handleClick}
  >
    <div className={styles.LogoHeader}>
      <Header />
    </div>

      <div className={styles.blobVideoWrap}>
        <div >
          <ReactPlayer
            className={styles.player}
            url={BlobVideo}
            playing={true}
            loop={true}
            controls={false}
            width={'100%'}
            height={'100%'}
          />
        </div>
      </div>

    <div className={styles.pourVideoWrap}>
      <img src={PourVideo} />
    </div>

    <div className={styles.cartDisplayWrap}>
      <CartDisplay 
        direction='column'
        showText={false}
      />
    </div>


        <div className={styles.waveRippleVideoWrap}>
            <img src={RippleVideo} />
        </div>



  <div className={styles.pourWaterLower} 
    style={{ 
              backgroundImage: `url(${PourWaterLower})`,
              backgroundRepeat: 'no-repeat',
              objectFit: 'contain'
            }}
    data-testid="CancelButton"
  >
    <div className={styles.lineOne}>
    PREPARING YOUR DRINK
    </div>
    <div className={styles.lineTwo}>
    Please wait...
    </div>
  </div>
  {showErrorModal && <Modal open>
    <ErrorModal />
  </Modal>}
  </div>
)};

export default DropPrepare;
