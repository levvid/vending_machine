import React from 'react';
import styles from './Header.module.css';
import DropWaterLogoHeader from '../../assets/images/LogoHeader.svg'

const Header: React.FC = () => (
  <div className={styles.Header} data-testid="Header">
    <div className={styles.HeaderLogo} data-testid="Header"
      style={{
        backgroundImage: `url(${DropWaterLogoHeader})`,
        width: 355,
        height: 70,
        objectFit: 'contain'
      }}
    >
    </div>
    <div className={styles.headerLine}></div>
  </div>
);

export default Header;
