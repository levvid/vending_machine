import React, { useState, useEffect } from "react";
import Switch from "react-switch";
import { IButtonSmallProps } from "../../interfaces/ProductSelect.interface";
import styles from "./DrinkBuilder.module.css";
import Header from "../Header/Header";
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import Footer from "../Footer/Footer";
import IdleUserModal from "../QuitModals/IdleUserModal/IdleUserModal";
import CancelOrderModal from "../QuitModals/CancelOrderModal/CancelOrderModal";
import DrinkBuilderBackgroundImage from "../../assets/images/DrinkBuilderBackgroundImage.svg";

// icons
import RoomTempIcn from "../../assets/images/icn-room-temp.svg";
import IceColdTempIcn from "../../assets/images/icn-ice-cold.svg";

// flavors
import CucumIcn from "../../assets/images/cucumberIcn.svg";
import GuavaIcn from "../../assets/images/guavaIcn.svg";
import LemonIcn from "../../assets/images/lemonadeIcn.svg";
import WaterIcn from "../../assets/images/waterIcn.svg";
// caffeine
import Caffeine0 from "../../assets/images/icn-caffeine-0-mg.svg";
import Caffeine20 from "../../assets/images/icn-caffeine-20-mg.svg";
import Caffeine40 from "../../assets/images/icn-caffeine-40-mg.svg";
import Caffeine70 from "../../assets/images/icn-caffeine-70-mg.svg";

// compostable bottles with flavor
import CompostableIcn from "../../assets/images/CompostableIcn.svg";
import CucumberBoostCompostIcn from "../../assets/images/CucumberBoostCompostIcn.svg";
import GuavaBoostCompostIcn from "../../assets/images/GuavaBoostCompostIcn.svg";
import LemonBoostCompostIcn from "../../assets/images/LemonBoostCompostIcn.svg";

// refill bottles with flavor
import CucumberBoostRefillIcn from "../../assets/images/CucumberBoostRefillIcn.svg";
import GuavaBoostRefillIcn from "../../assets/images/GuavaBoostRefillIcn.svg";
import LemonBoostRefillIcn from "../../assets/images/LemonBoostRefillIcn.svg";
import ContainerRefill from "../../assets/images/ContainerRefill.svg";

// Caffeine Labels
import CaffeineSomeLabel from "../../assets/images/CaffeineSomeLabel.svg";
import CaffeineMoreLabel from "../../assets/images/CaffeineMoreLabel.svg";
import CaffeineExtraLabel from "../../assets/images/CaffeineExtraLabel.svg";

import InformationContent from "../InformationContent/InformationContent";

import RowBtnSelSmall from "../RowBtnSelSmall/RowBtnSelSmall";

import { useAppDispatch } from "../../services/redux/hooks";
import {
  addFlavorCost,
  addCaffeineCost,
  setTemperature,
} from "../../services/redux/slices/cartSlice";
import { ButtonSmallService } from "../../services/rxjs/ButtonSmall.service";
import { DrinkBuilderSetDefaultSmBtn } from "../../services/rxjs/DrinkBuilderSetDefaultSmBtn.service";
import { preloadImages } from "../../services/utils/Images.service";
import { ColorSelectWs } from "../../services/rxjs/ColorSelectWs.service";
import { Subscriber, Subscription } from "rxjs";
import classes from "./DrinkBuilder.module.css";

export interface Iprops {
  centerBottleImage: any;
  addFlavor: any;
  addCaffeine: any;
}

const DrinkBuilder: React.FC = (props: any) => {
  const [checked, setChecked] = useState(false);
  const handleChange = (nextChecked: any) => {
    setChecked(nextChecked);
    dispatch(setTemperature({ temperature: checked ? "ice" : "room" }));
  };
  preloadImages();
  let dispenseType: string = "compostable";
  let caffeine: IButtonSmallProps[] = [
    {
      groupId: "caffeine",
      imageUrl: Caffeine0,
      description: "None | 0 MG",
      caffeine_level: 0,
      cost: 0.0,
      active: true,
      activeClass: "flavorCaffeineNoneBorder",
    },
    {
      groupId: "caffeine",
      imageUrl: Caffeine20,
      description: "Some | 20 MG",
      caffeine_level: 1,
      cost: 0.15,
      active: false,
      activeClass: "caffeineSomeBorder",
    },
    {
      groupId: "caffeine",
      imageUrl: Caffeine40,
      description: "More | 40 MG",
      caffeine_level: 2,
      cost: 0.3,
      active: false,
      activeClass: "caffeineMoreBorder",
    },
    {
      groupId: "caffeine",
      imageUrl: Caffeine70,
      description: "Extra | 70 MG",
      caffeine_level: 3,
      cost: 0.45,
      active: false,
      activeClass: "caffeineExtraBorder",
    },
  ];

  const flavors: IButtonSmallProps[] = [
    {
      groupId: "flavor",
      imageUrl: WaterIcn,
      bottleIcnUrl: CompostableIcn,
      refillIcnUrl: ContainerRefill,
      description: "Water",
      cost: 0.0,
      active: true,
      activeClass: "flavorCaffeineNoneBorder",
    },
    {
      groupId: "flavor",
      imageUrl: GuavaIcn,
      bottleIcnUrl: GuavaBoostCompostIcn,
      refillIcnUrl: GuavaBoostRefillIcn,
      description: "Guava",
      cost: 0.49,
      active: false,
      activeClass: "flavorGuavaBorder",
    },
    {
      groupId: "flavor",
      imageUrl: LemonIcn,
      bottleIcnUrl: LemonBoostCompostIcn,
      refillIcnUrl: LemonBoostRefillIcn,
      description: "Lemonade",
      cost: 0.49,
      active: false,
      activeClass: "flavorLemonBorder",
    },
    {
      groupId: "flavor",
      imageUrl: CucumIcn,
      bottleIcnUrl: CucumberBoostCompostIcn,
      refillIcnUrl: CucumberBoostRefillIcn,
      description: "Cucumber",
      cost: 0.49,
      active: false,
      activeClass: "flavorCucumberBorder",
    },
  ];

  const temperatures: IButtonSmallProps[] = [
    {
      groupId: "temperature",
      imageUrl: RoomTempIcn,
      description: "room",
      cost: 0,
      active: false,
      activeClass: "",
    },
    {
      groupId: "temperature",
      imageUrl: IceColdTempIcn,
      description: "ice",
      cost: 0,
      active: false,
      activeClass: "",
    },
  ];

  let dispatch = useAppDispatch();

  dispenseType = props.history.location.state.type;
  let containertype =
    dispenseType === "refill" ? ContainerRefill : CompostableIcn;
  const [centerBottleImage, setCenterBottleImage] = useState(containertype);
  const [centerBottleLabel, setCenterBottleLabel] = useState(CaffeineSomeLabel);
  const [hasLabel, setHasLabel] = useState(false);
  const [componentInitialized, setComponentInitialized] = useState(false);

  useEffect(() => {
    let isMounted = true;
    console.log("DrinkBUilder: useEffect");

    // only initialize once, FC's don't seem to have this notion
    //
    //if (componentInitialized === false) {
    setComponentInitialized(true);
    DrinkBuilderSetDefaultSmBtn.sendMessage(flavors[0]);
    DrinkBuilderSetDefaultSmBtn.sendMessage(caffeine[0]);
    dispatch(
      addFlavorCost({
        flavor: flavors[0].description,
        flavorCost: flavors[0].cost,
      })
    );
    dispatch(
      addCaffeineCost({
        caffeine: caffeine[0].description,
        caffeineCost: caffeine[0].cost,
        caffeine_level: caffeine[0].caffeine_level,
      })
    );

    dispatch(setTemperature({ temperature: "ice" }));

    const subscribe = ButtonSmallService.onMessage().subscribe(
      (message: any) => {
        console.log(
          "XDEBUG: DrinkBuilder purchased: " + message.item.description
        );
        if (message.item.groupId === "flavor") {
          ColorSelectWs.sendMessage(message.item.description.toLowerCase());
          setCenterImage(
            dispenseType,
            message.item.refillIcnUrl,
            message.item.bottleIcnUrl
          );
          dispatch(
            addFlavorCost({
              flavor: message.item.description,
              flavorCost: message.item.cost,
            })
          );
        } else if (message.item.groupId === "caffeine") {
          setCenterImageLabel(message.item.caffeine_level);
          dispatch(
            addCaffeineCost({
              caffeine: message.item.description,
              caffeineCost: message.item.cost,
              caffeine_level: message.item.caffeine_level,
            })
          );
        } else if (message.item.groupId === "temperature") {
          dispatch(setTemperature({ temperature: message.item.description }));
        }
      }
    );

    //}
    return () => {
      subscribe.unsubscribe();
      //nutritionModalService.unsubscribe();
    };
  }, []);

  function setCenterImageLabel(level: number) {
    switch (level) {
      case 1:
        setCenterBottleLabel(CaffeineSomeLabel);
        setHasLabel(true);
        break;
      case 2:
        setCenterBottleLabel(CaffeineMoreLabel);
        setHasLabel(true);
        break;
      case 3:
        setCenterBottleLabel(CaffeineExtraLabel);
        setHasLabel(true);
        break;
      default:
        setHasLabel(false);
        break;
    }
  }

  function setCenterImage(type: string, refillIcn: any, bottleIcn: any) {
    if (type === "refill") {
      setCenterBottleImage(refillIcn);
    } else {
      setCenterBottleImage(bottleIcn);
    }
  }

  return (
    <div
      className={styles.DrinkBuilder}
      data-testid="DrinkBuilder"
      style={{
        height: 1920,
        width: 1080,
        position: "relative",
        backgroundImage: `url(${DrinkBuilderBackgroundImage})`,
        overflow: "hidden",
      }}
    >
      <div className={styles.LogoHeader}>
        <Header />
      </div>

      <div className={styles.Title}>Customize Drink</div>

      <div className={styles.Copy}>
        Add flavor, caffeine and adjust the temperature
      </div>

      <div
        className={styles.CenterContainer}
        style={{
          backgroundImage: `url(${centerBottleImage})`,
        }}
      >
        {hasLabel && (
          <div
            className={styles.CenterContainerLabel}
            style={{
              backgroundImage: `url(${centerBottleLabel})`,
            }}
          >
            {" "}
          </div>
        )}
      </div>

      <div className={styles.addCaffeineTextWrap}>Add Caffeine</div>
      <div className={styles.caffeineWrap}>
        <RowBtnSelSmall
          list={caffeine}
          rowDirection="column"
          textJustify="left"
        />
      </div>

      <div className={styles.addFlavorTextWrap}>Add Flavor</div>
      <div className={styles.flavorsWrap}>
        <RowBtnSelSmall
          list={flavors}
          rowDirection="column"
          textJustify="right"
        />
      </div>

      <div className={styles.pill}></div>
      <div className={styles.waterTempWrap}>
        <Switch
          onChange={handleChange}
          checked={checked}
          handleDiameter={160}
          width={298}
          height={142}
          offColor="#fcfcfc"
          onColor="#fcfcfc"
          offHandleColor="#ffffff"
          onHandleColor="#ffffff"
          boxShadow="0 8px 24px 0 rgba(54, 56, 68, 0.1)"
          activeBoxShadow="0px 0px 1px 2px #6ce3ff"
          uncheckedIcon={
            <div
              className={styles.focusRoomIcnWrap}
              style={{ backgroundImage: `url(${RoomTempIcn})` }}
            ></div>
          }
          checkedIcon={
            <div
              className={styles.focusIceColdIcnWrap}
              style={{ backgroundImage: `url(${IceColdTempIcn})` }}
            ></div>
          }
          uncheckedHandleIcon={
            <div
              className={styles.focusIceColdHandleIcnWrap}
              style={{ backgroundImage: `url(${IceColdTempIcn})` }}
            ></div>
          }
          checkedHandleIcon={
            <div
              className={styles.focusRoomHandleIcnWrap}
              style={{ backgroundImage: `url(${RoomTempIcn})` }}
            ></div>
          }
        />
        <div className={styles.iceColdText}>ICE COLD</div>
        <div className={styles.roomTempText}>ROOM TEMP</div>
        {/*<WaterTempSelect />*/}
      </div>

      <div className={styles.confirmButtonWrap}>
        <ConfirmButton />
      </div>

      <InformationContent />

      <div className={styles.FooterWrap}>
        <Footer showBackIcn={true} showInfoIcn={true} showCartSum={true} />
      </div>

      <IdleUserModal />
      <CancelOrderModal />
    </div>
  );
};

export default DrinkBuilder;
