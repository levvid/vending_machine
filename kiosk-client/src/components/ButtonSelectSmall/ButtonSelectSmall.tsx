import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import styles from './ButtonSelectSmall.module.css';
import { ButtonSmallService } from '../../services/rxjs/ButtonSmall.service'
import { DrinkBuilderSetDefaultSmBtn } from '../../services/rxjs/DrinkBuilderSetDefaultSmBtn.service'

export interface Iprops {
  item: any;
  textJustify: string;
}

const ButtonSelectSmall: React.FC<Iprops> = (props: Iprops) => {
  const [curStyles, setCurStyles] = useState(styles.iconNormal);
  const [animateClass, setAnimateClass] = useState(classNames(styles.pressContainerOff));
  const [highlight, setHighlight] = useState(false);
  const [timeoutId, setTimeoutId] = useState(0);
  const [checkActiveTimer, setCheckActiveTimer] = useState(false);

  const imageUrl = props.item.imageUrl;
  const title = props.item.description;
  const activeClass = props.item.activeClass;
  const cost = (typeof props.item.cost !== undefined) ? props.item.cost: 0;

  useEffect(() => {

    // turn off highlight unless selected
    //
    const subscribe = ButtonSmallService.onMessage().subscribe((message: any) => {
      setCheckActiveTimer(true);
      if ((message.item.groupId === props.item.groupId) && (message.item.description !== props.item.description)) {
        setCurStyles(styles.iconNormal);
        setAnimateClass(classNames(styles.pressContainerOff));
      } 
    });

    // Set default value on page load, drink builder sends message
    //
    const setDefaultSubscribe = DrinkBuilderSetDefaultSmBtn.onMessage().subscribe((message: any) => {
      if ((message.item.groupId === props.item.groupId) && (message.item.description === props.item.description)) {
        highlightButton(true);
      }
    })

    // on quick selecting, don't highlight two
    //
    if(highlight) {
      highlightButton(false);
      setHighlight(false);
    } 
  
    if(checkActiveTimer && (timeoutId > 0)) {
      clearTimeout(timeoutId);
      setCheckActiveTimer(false);
      setTimeoutId(0)
    }

    return () => {
      subscribe.unsubscribe();
      setDefaultSubscribe.unsubscribe();
    }
  }, [highlight, checkActiveTimer]);

  // since the class names are baked into webpacking
  // map text names to copmiled in names
  // classNames() will propery concat them
  //
  const mapActiveClass = (active: string) => {
    switch(active) {
      case 'flavorCaffeineNoneBorder':
        return styles.flavorCaffeineNoneBorder;
      case 'caffeineSomeBorder':
        return styles.caffeineSomeBorder;
      case 'caffeineMoreBorder':
        return styles.caffeineMoreBorder;
      case 'caffeineExtraBorder':
        return styles.caffeineExtraBorder;
      case 'flavorGuavaBorder':
        return styles.flavorGuavaBorder;
      case 'flavorLemonBorder':
        return styles.flavorLemonBorder;
      case 'flavorCucumberBorder':
        return styles.flavorCucumberBorder;
      default: 
        return styles.flavorCaffeineNoneBorder;
    }
  }

  const highlightButton = (sendMessage: boolean) => {
    let cnames = classNames(styles.iconHighlight, mapActiveClass(activeClass));
    setCurStyles(cnames);
    if(sendMessage) {
      ButtonSmallService.sendMessage(props.item);
    }
  }

  const handleClick = () => {
    highlightButton(true);
    // turn off press animation
    setAnimateClass(classNames(styles.pressContainerOff));
  }

  const handlePress = () => {
    // start press animation
    setAnimateClass(classNames(styles.pressContainerPress, styles.pressState));

    let toId: any = setTimeout(() => {
      setHighlight(true);
    }, 190);

    setTimeoutId(toId);
  }

  if(props.textJustify === 'right') {
    return (
      <div className={styles.containerTextRight} data-testid="BtnSmall"
          onMouseDown={handlePress}
          onTouchStart={handlePress}
          onMouseUp={handleClick}
          onTouchEnd={handleClick}
      >
        <div className={curStyles}>
            <div className={styles.icon}
              style={{ backgroundImage: `url(${imageUrl})`}}
            >
            </div>
            <div className={animateClass}></div>
        </div>
        <div className={styles.titleRight}>
          {title.toUpperCase()}
        </div>
        <div className={styles.costRight}>
          + ${cost.toFixed(2)}
        </div>
      </div>
    );
  } else if (props.textJustify === 'left') {
    return (
      <div className={styles.containerTextLeft} data-testid="BtnSmall"
          onMouseDown= {handlePress}
          onTouchStart= {handlePress}
          onMouseUp={handleClick}
          onTouchEnd={handleClick}
      >
        <div className={styles.textWrapLeft}>

          <div className={styles.titleLeft}>
            {title.toUpperCase()}
          </div>
          <div className={styles.costLeft}>
            + ${cost.toFixed(2)}
          </div>
        </div>
        <div className={curStyles}>
          <div className={styles.icon}
            style={{ backgroundImage: `url(${imageUrl})`}}
          >
          </div>
            <div className={animateClass}></div>
        </div>
      </div>
    );
  } else {
    return (
      <p>Set property textJustfiy to left or right</p>
    );
  }
}

export default ButtonSelectSmall;
