import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ButtonSelectSmall from './ButtonSelectSmall';

describe('<ButtonSelectSmall />', () => {
  test('it should mount', () => {
    render(<ButtonSelectSmall />);
    
    const buttonSelectSmall = screen.getByTestId('ButtonSelectSmall');

    expect(buttonSelectSmall).toBeInTheDocument();
  });
});