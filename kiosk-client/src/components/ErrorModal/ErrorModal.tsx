import React from 'react';
import styles from './ErrorModal.module.css';
import ErrorBadge from '../../assets/images/error-badge.svg';

const ErrorModal: React.FC = () => (
  <div className={styles.ErrorModal} data-testid="ErrorModal">
    <div className={styles.topBackground}> </div>
    <div className={styles.errorBadgeicn}
      style={{
        backgroundImage: `url(${ErrorBadge})`,
        backgroundRepeat: 'no-repeat',
        objectFit: 'contain',
        position: 'absolute',
        top: 173,
        left: 202,
        height: 500,
        width: 500,
        zIndex: 100,
      }}
    ></div>
    <div className={styles.subtitle}>
      Oops! There was a problem making your drink
    </div>
    <div className={styles.errorMessageText}>
      Drop cartons are currently unavailable
    </div>


  </div>
);

export default ErrorModal;
