import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ErrorModal from './ErrorModal';

describe('<ErrorModal />', () => {
  test('it should mount', () => {
    render(<ErrorModal />);
    
    const errorModal = screen.getByTestId('ErrorModal');

    expect(errorModal).toBeInTheDocument();
  });
});