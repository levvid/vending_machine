import React, { useState, useEffect } from "react";
import styles from "./WaterTempSelect.module.css";
import { IButtonSmallProps } from "../../interfaces/ProductSelect.interface";

// water temp
import RoomTempIcn from "../../assets/images/icn-room-temp.svg";
import IceColdTempIcn from "../../assets/images/icn-ice-cold.svg";

import { ButtonSmallService } from "../../services/rxjs/ButtonSmall.service";

const WaterTempSelect: React.FC = () => {
  const [iceColdFocused, setIceColdFocused] = useState(true);
  const [mouseIsDownOnIceTemp, setMouseIsDownOnIceTemp] = useState(false);
  const [mouseIsMoveFromIceTemp, setMouseIsMoveFromIceTemp] = useState(false);
  const [mouseIsDownOnRoomTemp, setMouseIsDownOnRoomTemp] = useState(false);
  const [mouseIsMoveFromRoomTemp, setMouseIsMoveFromRoomTemp] = useState(false);

  useEffect(() => {
    //default to ice cold, set in store
    ButtonSmallService.sendMessage(temperatures[1]);
  }, []);

  const temperatures: IButtonSmallProps[] = [
    {
      groupId: "temperature",
      imageUrl: RoomTempIcn,
      description: "room",
      cost: 0,
      active: false,
      activeClass: "",
    },
    {
      groupId: "temperature",
      imageUrl: IceColdTempIcn,
      description: "ice",
      cost: 0,
      active: false,
      activeClass: "",
    },
  ];

  function handleClick() {
    // toggle between temps even on same side click
    if (iceColdFocused) {
      setIceColdFocused(false);
      ButtonSmallService.sendMessage(temperatures[0]);
    } else {
      setIceColdFocused(true);
      ButtonSmallService.sendMessage(temperatures[1]);
    }
  }

  function handleMouseIsDownOnIceTemp() {
    setMouseIsDownOnIceTemp(true);
  }

  function handleMouseIsDownOnRoomTemp() {
    setMouseIsDownOnRoomTemp(true);
  }

  function handleMouseMoveToRoomTemp() {
    if (mouseIsDownOnIceTemp) {
      setMouseIsMoveFromIceTemp(true);
    }
  }

  function handleMouseMoveToIceTemp() {
    if (mouseIsDownOnRoomTemp) {
      setMouseIsMoveFromRoomTemp(true);
    }
  }

  function handleMouseUpOnRoomTemp() {
    if (mouseIsDownOnIceTemp && mouseIsMoveFromIceTemp) {
      handleSwipeToRoomTemp();
      setMouseIsDownOnIceTemp(false);
      setMouseIsMoveFromIceTemp(false);
    }
  }

  function handleMouseUpOnIceTemp() {
    if (mouseIsDownOnRoomTemp && mouseIsMoveFromRoomTemp) {
      handleSwipeToIceTemp();
      setMouseIsDownOnRoomTemp(false);
      setMouseIsMoveFromRoomTemp(false);
    }
  }

  function handleSwipeToRoomTemp() {
    setIceColdFocused(false);
    ButtonSmallService.sendMessage(temperatures[0]);
  }

  function handleSwipeToIceTemp() {
    setIceColdFocused(true);
    ButtonSmallService.sendMessage(temperatures[1]);
  }

  return (
    <div>
      <div className={styles.WaterTempSelect} data-testid="WaterTempSelect">
        <div className={styles.pill}>
          <div
            className={styles.iceCold}
            style={{ backgroundImage: `url(${IceColdTempIcn})` }}
            onMouseDown={handleMouseIsDownOnIceTemp}
            onMouseMove={handleMouseMoveToRoomTemp}
            onMouseUp={handleMouseUpOnIceTemp}
            onClick={handleClick}
          >
            {iceColdFocused && (
              <div className={styles.focusIceCold}>
                <div
                  className={styles.focusIceColdIcnWrap}
                  style={{ backgroundImage: `url(${IceColdTempIcn})` }}
                ></div>
              </div>
            )}
          </div>

          <div
            className={styles.roomTemp}
            style={{ backgroundImage: `url(${RoomTempIcn})` }}
            onMouseDown={handleMouseIsDownOnRoomTemp}
            onMouseMove={handleMouseMoveToIceTemp}
            onMouseUp={handleMouseUpOnRoomTemp}
            onClick={handleClick}
          >
            {!iceColdFocused && (
              <div className={styles.focusRoom}>
                <div
                  className={styles.focusRoomIcnWrap}
                  style={{ backgroundImage: `url(${RoomTempIcn})` }}
                ></div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className={styles.iceColdText}>ICE COLD</div>
      <div className={styles.roomTempText}>ROOM TEMP</div>
    </div>
  );
};

export default WaterTempSelect;