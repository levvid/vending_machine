import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import WaterTempSelect from './WaterTempSelect';

describe('<WaterTempSelect />', () => {
  test('it should mount', () => {
    render(<WaterTempSelect />);
    
    const waterTempSelect = screen.getByTestId('WaterTempSelect');

    expect(waterTempSelect).toBeInTheDocument();
  });
});