import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ConfirmButton from './ConfirmButton';

describe('<ConfirmButton />', () => {
  test('it should mount', () => {
    render(<ConfirmButton />);
    
    const confirmButton = screen.getByTestId('ConfirmButton');

    expect(confirmButton).toBeInTheDocument();
  });
});