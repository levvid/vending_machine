import React, { useState } from 'react';
import styles from './ConfirmButton.module.css';
import { useHistory } from "react-router-dom";
import { useAppDispatch } from '../../services/redux/hooks';
import { makeOrder } from '../../services/redux/slices/cartSlice';

const ConfirmButton: React.FC = () => {
  const history = useHistory();
  const [ buttonPressed, setButtonPressed ] = useState(false);
  const dispatch = useAppDispatch();

  function handleClick() {
    console.log('XDEBUG I got clicked');
    dispatch(makeOrder());
    /*history.push({
      pathname: '/payment',
    });*/
    history.push({
      pathname: '/prepare'
    })
  }

  function handleButtonPressed() {
    setButtonPressed(true);
  }

  return (
  <div className={ buttonPressed ? styles.confirmButtonPressed : styles.ConfirmButton} data-testid="ConfirmButton"
    onMouseDown={handleButtonPressed}
    onMouseUp={handleClick}
    onTouchEnd={handleClick}
  >
    <div className={styles.confirm}>
      CONFIRM ORDER
    </div>
  </div>
  )
};

export default ConfirmButton
