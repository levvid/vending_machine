import React from 'react';
import styles from './BottomButtonNav.module.css';
import  BottomNavButtons from '../../assets/images/PageFrameBottomNavButtons.svg';

const BottomButtonNav: React.FC = () => (
  <div className={styles.BottomButtonNav} data-testid="BottomButtonNav"
      style={{ backgroundImage: `url(${BottomNavButtons})`,
               height: 223,
               width: 1080,
               zIndex: 100 }}
    >
  </div>
);

export default BottomButtonNav;
