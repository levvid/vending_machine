import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BottomButtonNav from './BottomButtonNav';

describe('<BottomButtonNav />', () => {
  test('it should mount', () => {
    render(<BottomButtonNav />);
    
    const bottomButtonNav = screen.getByTestId('BottomButtonNav');

    expect(bottomButtonNav).toBeInTheDocument();
  });
});