import React from "react";
import styles from "./HomeFooter.module.css";


const HomeFooter: React.FC = () => {
  return (
    <div className={styles.footer}>
      <div className={styles.text}>Touch Screen To Start</div>
    </div>
  );
}

export default HomeFooter;
