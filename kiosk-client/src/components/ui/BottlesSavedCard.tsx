import React, { useState, useEffect} from "react";
import styles from "./BottlesSavedCard.module.css";
import axios from "axios";
import { ReactComponent as BottleIcn } from "../../assets/images/other_plastic-bottle.svg";


const BottlesSavedCard: React.FC = () => {
  var ErrorFetchedChecker = false;
  const [numBottlesSaved, setNumBottlesSaved] = useState("");


  useEffect(() => {
    axios
      //.get("http://192.168.86.58:8000/api/orders/count/")
      .get('http://localhost:8000/api/orders/count/' )
      .then((res: any) => {
        console.log(res);
        let obj = res?.data.num_bottles_saved;
        console.log(obj);
        let num = res?.data.num_bottles_saved;
        setNumBottlesSaved(numberWithCommas(num))  
        ErrorFetchedChecker = false;
      })
      .catch((err: any) => {
        ErrorFetchedChecker = true;
        console.log(err);
      });
  }, [ErrorFetchedChecker])

  function numberWithCommas(x:any) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

  return (
    <div className={styles.bottlesSavedCard}>
      <BottleIcn className={styles.bottleIcon}></BottleIcn>
      <div className={styles.numBottles}>{numBottlesSaved}</div>
      <div className={styles.bottlesSavedText}>PLASTIC BOTTLES SAVED</div>
    </div>


  );
};

export default BottlesSavedCard;
