import React from "react";
import styles from "./ScanCard.module.css";
import { ReactComponent as CameraIcn } from "../../assets/images/icn-camera.svg";
import qrCode from "../../assets/images/qr-code@3x.png";

const ScanCard: React.FC = () => {
  return (
    <div>
      <div className={styles.scanCard}></div>
      <CameraIcn className={styles.cameraIcn} />
      <img src={qrCode} className={styles.qrCode} />
      <div className={styles.qrCodeLineOne}>Scan For</div>
      <div className={styles.qrCodeLineTwo}>Contactless</div>
      <div className={styles.qrCodeLineThree}>Ordering</div>
    </div>
  );
};

export default ScanCard;
