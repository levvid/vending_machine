import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import styles from './QuitModalCancelOrderBtn.module.css';
import { useHistory } from "react-router-dom";
import { isPropertySignature } from 'typescript';

interface Iprops {
  cb?: ()=>void;
}

const QuitModalCancelOrderBtn: React.FC<Iprops> = (props: Iprops) => {
  const [classBtn, setClassBtn] = useState(classNames(styles.QuitModalCancelOrderBtn, styles.buttonLabel, styles.noPress));
  const history = useHistory();
  
  function handleCancelOrder() {
    if(typeof props?.cb === 'function') {
      props.cb();
    }
    history.push({
      pathname: '/',
    })
  }

  function handleTouch() {
    setClassBtn(classNames(styles.QuitModalCancelOrderBtn, styles.buttonLabel, styles.press));
  }
  
  return (
  <div className={classBtn} data-testid="QuitModalCancelOrderBtn"
      onMouseUp={handleCancelOrder}
      onTouchEnd={handleCancelOrder}

      onMouseDown={handleTouch}
      onTouchStart={handleTouch}
          >
            Cancel Order

  </div>
)};

export default QuitModalCancelOrderBtn;
