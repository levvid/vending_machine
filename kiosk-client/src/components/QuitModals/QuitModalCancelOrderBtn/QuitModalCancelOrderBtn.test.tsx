import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import QuitModalCancelOrderBtn from './QuitModalCancelOrderBtn';

describe('<QuitModalCancelOrderBtn />', () => {
  test('it should mount', () => {
    render(<QuitModalCancelOrderBtn />);
    
    const quitModalCancelOrderBtn = screen.getByTestId('QuitModalCancelOrderBtn');

    expect(quitModalCancelOrderBtn).toBeInTheDocument();
  });
});