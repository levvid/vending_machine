import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CancelOrderModal from './CancelOrderModal';

describe('<CancelOrderModal />', () => {
  test('it should mount', () => {
    render(<CancelOrderModal />);
    
    const cancelOrderModal = screen.getByTestId('CancelOrderModal');

    expect(cancelOrderModal).toBeInTheDocument();
  });
});