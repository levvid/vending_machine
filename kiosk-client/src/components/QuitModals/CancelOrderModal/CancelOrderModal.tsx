import React, { useState, useEffect } from 'react';
import Modal from '@material-ui/core/Modal';
import styles from './CancelOrderModal.module.css';
import { useHistory } from "react-router-dom";
import { mapTagToImage } from "../../../services/utils/Images.service";
import QuitModalCancelOrderBtn from '../QuitModalCancelOrderBtn/QuitModalCancelOrderBtn';
import QuitModalContinueOrderBtn from '../QuitModalContinueOrderBtn/QuitModalContinueOrderBtn';
import { CancelModalBtn } from '../../../services/rxjs/CancelModalBtn.service';

const CancelOrderModal: React.FC = () => {
  const history = useHistory();
  const [showModal, setShowModal] = useState(false);
  const ErrorBadge = mapTagToImage('error-badge');

  useEffect(() => {
    const cancelModalBtn = CancelModalBtn.onMessage().subscribe((message: any) => {
      setShowModal(true);
    })

    return () => {
      cancelModalBtn.unsubscribe();
    }
  })

  return (
    <div className={styles.CancelOrderModal} data-testid="CancelOrderModal">
      {showModal && <div className={styles.modalWrap} >
        <Modal open={showModal}>
          <div className={styles.content}>
            <div className={styles.dialogTopBackground}> </div>
            <div className={styles.errorBadgeWrap}
              style={{
                backgroundImage: `url(${ErrorBadge})`
              }}
            > </div>
            <div className={styles.title}>
              Cancel Order?
        </div>
            <div className={styles.copy}>
              Are you sure you want to cancel your custom drink order?
        </div>
            <div className={styles.buttonWrap}>
              <QuitModalCancelOrderBtn />
              <div className={styles.line}></div>
              <QuitModalContinueOrderBtn continueCB={() => { setShowModal(false) }} />
            </div>
          </div>
        </Modal>
      </div>}
    </div>
  )
};

export default CancelOrderModal;
