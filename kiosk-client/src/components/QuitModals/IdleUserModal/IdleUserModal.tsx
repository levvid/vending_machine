import React, { useState, useEffect } from 'react';
import styles from './IdleUserModal.module.css';
import Modal from '@material-ui/core/Modal';
import styled from 'styled-components';
import QuitModalCancelOrderBtn from '../QuitModalCancelOrderBtn/QuitModalCancelOrderBtn';
import QuitModalContinueOrderBtn from '../QuitModalContinueOrderBtn/QuitModalContinueOrderBtn';
import { useHistory } from "react-router-dom";
import { IdleUserTimeout } from '../../../services/rxjs/IdleUserTimeout.service';

const IdleUserModal: React.FC = () => {
  const history = useHistory();
  const [dialCount, setDialCount] = useState(20);
  const [conGradPct, setConGradPct] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [intervalHandle, setIntervalHandle] = useState(0);

  const DialTwo = styled.div`
    position: absolute;
    left: 54px;
    top: 54px;
    height: 250px;
    width: 250px;
    border-radius: 50%;
    box-shadow: 0 8px 72px 0 rgba(54, 56, 68, 0.16);
    background:
    radial-gradient(
        white calc(60% - 1px) /* fix Chromium jagged edges */,
        transparent 60%),
    conic-gradient( from 0deg, white ${conGradPct}%, #6ce3ff 0%);
    transition: trasform 1s
  `;


  const stopCounter = () => {
    //console.log("handle: " + intervalHandle);
    clearInterval(intervalHandle);
    setIntervalHandle(0);
  }

  const StartDialAnimation = ():any => {
    let i = 0;
    if(intervalHandle !== 0) {
      stopCounter();
    }
    let randomId = Math.random()
    let intervalId = setInterval(() => {
      setDialCount(20 - i);
      setConGradPct(i * 5);
      if (i++ >= 20) {
        clearInterval(intervalId)
        timerCancel();
      }
    }, 1000 );
    return intervalId;
  }

  const userContinue = () => {
    setShowModal(false);
    stopCounter();
  }

  const userCancel = () => {
    clearInterval(intervalHandle);
    stopCounter();
  }

  const timerCancel = () => {
    stopCounter();
    history.push({
      pathname: '/',
    })
  }
  
  useEffect(() => {
    // Dial animation code

    // this modal will show when global timer sends a message
    //
    const idleUserTimeout = IdleUserTimeout.onMessage().subscribe((message: any) => {
      if (message?.item === 'showIdleUserModal') {
        setIntervalHandle(StartDialAnimation());
        setShowModal(true);
      }
    });

    return () => {
      idleUserTimeout?.unsubscribe();
      clearInterval(intervalHandle);
    }
  }, []);

  return (
    <div className={styles.IdleUserModal} data-testid="IdleUserModal">
      {showModal && <div className={styles.modalWrap}>
        <Modal open={showModal}>
          <div className={styles.content}>
            <div className={styles.dialogTopBackground}> </div>
            <div className={styles.dialBackground}>
              <DialTwo>
                <div className={styles.dialCount}>
                  <span className={styles.centerText}>{dialCount}</span>
                </div>
                <div className={styles.dialUnits}>
                  <span className={styles.centerText}>SECONDS</span>
                </div>
              </DialTwo>
            </div>

            <div className={styles.titleText}>
              Need More Time?
        </div>

            <div className={styles.copy}>
              Your order will automatically cancel in 20 seconds
        </div>

            <div className={styles.buttonWrap}>
              <QuitModalCancelOrderBtn cb={() => { userCancel() }}/>
              <div className={styles.line}></div>
              <QuitModalContinueOrderBtn continueCB={() => { userContinue() }} />
            </div>
          </div>
        </Modal>
      </div>}
    </div>
  )
};

export default IdleUserModal;
