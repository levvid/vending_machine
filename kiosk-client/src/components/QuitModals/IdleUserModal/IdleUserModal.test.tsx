import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import IdleUserModal from './IdleUserModal';

describe('<IdleUserModal />', () => {
  test('it should mount', () => {
    render(<IdleUserModal />);
    
    const idleUserModal = screen.getByTestId('IdleUserModal');

    expect(idleUserModal).toBeInTheDocument();
  });
});