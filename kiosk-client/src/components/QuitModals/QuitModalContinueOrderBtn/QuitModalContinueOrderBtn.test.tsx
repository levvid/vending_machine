import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import QuitModalContinueOrderBtn from './QuitModalContinueOrderBtn';

describe('<QuitModalContinueOrderBtn />', () => {
  test('it should mount', () => {
    render(<QuitModalContinueOrderBtn />);
    
    const quitModalContinueOrderBtn = screen.getByTestId('QuitModalContinueOrderBtn');

    expect(quitModalContinueOrderBtn).toBeInTheDocument();
  });
});