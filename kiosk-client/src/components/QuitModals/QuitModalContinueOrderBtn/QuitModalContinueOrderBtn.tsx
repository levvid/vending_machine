import React, { useState } from 'react';
import classNames from 'classnames';
import styles from './QuitModalContinueOrderBtn.module.css';

interface Iprops {
  continueCB: ()=>void;
}

const QuitModalContinueOrderBtn: React.FC<Iprops> = (props: Iprops) => {
const [classBtn, setClassBtn] = useState(classNames(styles.QuitModalContinueOrderBtn, styles.buttonLabel, styles.noPress));

function handleContinueOrder() {
  props.continueCB();
}

function handleTouch() {
  setClassBtn(classNames(styles.QuitModalContinueOrderBtn, styles.buttonLabel, styles.press));
}

return (
<div className={classBtn} data-testid="QuitModalContinueOrderBtn"
    onMouseUp={handleContinueOrder}
    onTouchEnd={handleContinueOrder}

    onMouseDown={handleTouch}
    onTouchStart={handleTouch}
  >
    Continue Order
</div>
)};

export default QuitModalContinueOrderBtn;
