import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CancelOrderBtn from './CancelOrderBtn';

describe('<CancelOrderBtn />', () => {
  test('it should mount', () => {
    render(<CancelOrderBtn />);
    
    const cancelOrderBtn = screen.getByTestId('CancelOrderBtn');

    expect(cancelOrderBtn).toBeInTheDocument();
  });
});