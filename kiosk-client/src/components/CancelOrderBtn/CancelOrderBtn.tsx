import React from 'react';
import styles from './CancelOrderBtn.module.css';
import  Pill from '../../assets/images/Pill.svg';
import { useHistory } from "react-router-dom";

const CancelOrderBtn: React.FC = () => {
  let history = useHistory();
  
  function CancelOrder() {
    history.push('/start');
  }
  
  return (
  <div className={styles.CancelOrderBtn} data-testid="CancelOrderBtn">
    <div className={styles.Pill} 
      style={{ backgroundImage: `url(${Pill})`,
               width: 349,
               height: 134,
               backgroundRepeat: 'no-repeat',
               position: 'absolute'
               }}
      onMouseUp={CancelOrder}
      onTouchEnd={CancelOrder}
    >
      <div className={styles.CancelOrderText}>
        CANCEL ORDER
      </div>
    </div>
  </div>
)}

export default CancelOrderBtn;
