import React, { useEffect, useState } from "react";
import styles from './PaymentSummary.module.css';
import CartDisplay from '../../CartDisplay/CartDisplay';
import { useAppSelector } from '../../../services/redux/hooks';
import { selectCartTotal, selectCart } from '../../../services/redux/slices/cartSlice';

const PaymentSummary: React.FC = () => {
  const cost = useAppSelector(selectCartTotal);
  const cart: any = useAppSelector(selectCart);
  const [perOzText, setPerOzText] = useState('');
  const [perOzFlag, setPerOzFlag] = useState(false);

  useEffect(() => {
    if(cart.order_type === 'refill') {
      setPerOzText(' / 16oz');
      setPerOzFlag(true);
    }
  })
  
  
  return (
  <div className={styles.PaymentSummary} data-testid="PaymentSummary">
    <div className={styles.amount}>
      ${cost?.toFixed(2)}{perOzText}
    </div>
    {!perOzFlag &&<div className={styles.copy}>
      TOTAL PRICE
    </div>}
    {perOzFlag && <div className={styles.minOrderText}>
      $0.15 MINIMUM ORDER
    </div>}
    <div className={styles.vertBar}></div>
    <div className={styles.cartDisplayWrap}>
      <CartDisplay 
        direction='row'
        showText={true}
      />
    </div>
  </div>
)};

export default PaymentSummary;
