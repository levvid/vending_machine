import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PaymentSummary from './PaymentSummary';

describe('<PaymentSummary />', () => {
  test('it should mount', () => {
    render(<PaymentSummary />);
    
    const paymentSummary = screen.getByTestId('PaymentSummary');

    expect(paymentSummary).toBeInTheDocument();
  });
});