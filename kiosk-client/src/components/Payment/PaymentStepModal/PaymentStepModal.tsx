import React, { useEffect, useState } from 'react';
import styles from './PaymentStepModal.module.css';
import Modal from '@material-ui/core/Modal';
import styled from 'styled-components';
import Start from '../../Start/Start';
import ErrorIcon from '../../../assets/images/ExitWhite.svg';
import CheckIcon from '../../../assets/images/CheckWhite.svg';

interface Iprops {
  step: number;
}

const PaymentStepModal: React.FC<Iprops> = (props: Iprops) => {
  const [showModal, setShowModal] = useState(false);
  const [intervalHandle, setIntervalHandle] = useState(0);
  const [conGradPct, setConGradPct] = useState(0);
  const [title, setTitle] = useState('');
  const [copy, setCopy] = useState('');


  const DialTwo = styled.div`
    position: absolute;
    left: 54px;
    top: 54px;
    height: 250px;
    width: 250px;
    border-radius: 50%;
    box-shadow: 0 8px 72px 0 rgba(54, 56, 68, 0.16);
    background:
    radial-gradient(
        white calc(60% - 1px) /* fix Chromium jagged edges */,
        transparent 60%),
    conic-gradient( from 0deg, white ${conGradPct}%, #6ce3ff 0%);
    transition: trasform 1s
  `;

  const stopCounter = () => {
    clearInterval(intervalHandle);
    setIntervalHandle(0);
  }

  const StartDialAnimation = ():any => {
    let i = 0;
    if(intervalHandle !== 0) {
      stopCounter();
    }
    return  setInterval(() => {
      setConGradPct(i * 5);
      if (i++ >= 20) {
        stopCounter();
        setConGradPct(0);
        i = 0;
      }
    }, 1000 );
  }

  useEffect(() => {

    if(props.step > 0) {
      setShowModal(true);

      switch(props.step) {
        case 1:
          setIntervalHandle(StartDialAnimation());
          setTitle('Processing Payment');
          setCopy('Please wait...');
          break;
        case 2:
          stopCounter();
          setTitle('Payment Successful');
          setCopy('Your payment was successful');
          break;
        case 3:
          // error
          stopCounter();
          setTitle('Payment Error');
          setCopy('There was an error processing your payment, please try again');
          break;
        default:
          stopCounter();

          break;

      }
    }

    return () => {
      // cleanup 
    }
  }, [])
  
  
  return (
  <div className={styles.PaymentStepModal} data-testid="PaymentStepModal">
      {showModal && <div className={styles.modalWrap}>
        <Modal open={showModal}>
          <div className={styles.content}>
            <div className={styles.dialogTopBackground}> </div>
            <div className={styles.dialBackground}>
              {(props.step === 1) && <DialTwo> </DialTwo>}
              {(props.step === 2) && <div className={styles.successCircle}>
                <div className={styles.CheckIcn}
                  style={{
                    backgroundImage: `url(${CheckIcon})`
                  }}
                ></div>
              </div>}
              {(props.step === 3) && <div className={styles.errorCircle}>
              <div className={styles.ExitIcn}
                style={{
                  backgroundImage: `url(${ErrorIcon})`
                }}
              ></div>
              </div>}
            </div>

            <div className={styles.titleText}>
              {title}
            </div>

            <div className={styles.copy}>
              {copy}
            </div>

          </div>
        </Modal>
      </div>}

  </div>
)};

export default PaymentStepModal;
