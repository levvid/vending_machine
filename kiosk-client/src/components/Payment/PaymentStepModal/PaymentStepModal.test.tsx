import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PaymentStepModal from './PaymentStepModal';

describe('<PaymentStepModal />', () => {
  test('it should mount', () => {
    render(<PaymentStepModal />);
    
    const paymentStepModal = screen.getByTestId('PaymentStepModal');

    expect(paymentStepModal).toBeInTheDocument();
  });
});