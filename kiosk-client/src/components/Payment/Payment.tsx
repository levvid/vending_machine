import React, { useState, useEffect } from 'react';
import styles from './Payment.module.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import ReactPlayer from 'react-player'
import CardReaderVideo from '../../assets/videos/CardReaderVideo.mp4';
import PaymentStepModal from './PaymentStepModal/PaymentStepModal';
import PaymentSummary from './PaymentSummary/PaymentSummary';
import { useHistory } from "react-router-dom";
import { useAppSelector } from '../../services/redux/hooks';
import { selectCart } from '../../services/redux/slices/cartSlice';
import { PaymentWs } from '../../services/rxjs/PaymentWs.service';
import CancelOrderModal from '../QuitModals/CancelOrderModal/CancelOrderModal';

const Payment: React.FC = () => {
  const history = useHistory();
  const cart = useAppSelector(selectCart);
  const [tapCount, setTapCount] = useState(0);
  const [paymentStep, setPaymentStep] = useState(0);

  function handleClick() {
    if(tapCount > 3) {
      NavigateToPrepare();
    } else {
      console.log('XDEBUG setting payment step to: ' + paymentStep + 1);
      setPaymentStep(paymentStep + 1);
      setTapCount(tapCount + 1);
    }
  }

  function NavigateToPrepare() {
        let path="/refill"

        if(cart.order_type === 'drop_bottle') {
          path = '/prepare';
        }

        history.push({
          pathname: path,
        });
  }

  useEffect(() => {
    // TODO: remove this when live
    // send message to web socket start prepare
    PaymentWs.sendMessage('start_payment');

    // messages from web socket
    const paymentWs = PaymentWs.onMessage().subscribe((message: any) => {
      if(message.item === 'payment_processing') {
        setPaymentStep(1);
      } else if(message.item === 'payment_success') {
        setPaymentStep(2);
        setTimeout(() => {
          NavigateToPrepare();
        }, 500);
      } else if(message.item === 'failed') {
        setPaymentStep(3);
      }
    })

    return () => {
      paymentWs.unsubscribe();
    }
  }, [paymentStep])
  
  return (
  <div className={styles.Payment} data-testid="Payment"
    onMouseUp={handleClick}
    onTouchEnd={handleClick}
  >
    <div className={styles.LogoHeader}>
      <Header />
    </div>

    <div className={styles.titleText}>
      Make Payment
    </div>
    <div className={styles.copyText}>
      Please use the card machine on the kiosk to make the payment
    </div>

    {(paymentStep === 0) && <div className={styles.playerWrap}>
      <ReactPlayer
        className={styles.player}
        url={CardReaderVideo}
        playing={true}
        loop={true}
        controls={false}
        width={936}
        height={936}
      />
    </div>}

    <PaymentStepModal step={paymentStep}/>

    <div className={styles.paymentSummaryWrap} >
      <PaymentSummary />
    </div>

    <div className={styles.footerLine}></div>
    <div className={styles.FooterWrap}>
      <Footer 
        showBackIcn={true}
        showInfoIcn={true}
      />
    </div>

    <CancelOrderModal />
  </div>
)};

export default Payment;
