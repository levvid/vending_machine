import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PaymentProcessingModal from './PaymentProcessingModal';

describe('<PaymentProcessingModal />', () => {
  test('it should mount', () => {
    render(<PaymentProcessingModal />);
    
    const paymentProcessingModal = screen.getByTestId('PaymentProcessingModal');

    expect(paymentProcessingModal).toBeInTheDocument();
  });
});