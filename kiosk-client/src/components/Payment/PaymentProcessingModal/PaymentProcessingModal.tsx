import React from 'react';
import styles from './PaymentProcessingModal.module.css';
import ProcessingPaymentImg from '../../../assets/images/ProcessingPayment.png';

const PaymentProcessingModal: React.FC = () => (
  <div className={styles.PaymentProcessingModal} data-testid="PaymentProcessingModal">
    <div className={styles.wrap}>

        <div className='styles.paymentProcessing'
          style={{
            backgroundImage: `url(${ProcessingPaymentImg})`,
            objectFit: 'contain',
            position: 'absolute',
            top: 20,
            left: 50,
            width: 813,
            height: 765,
            backgroundRepeat: 'no-repeat'

          }} >
        </div>
        <div className={styles.modalTitle}>
          Processing Payment
        </div>
        <div className={styles.modalCopy}>
          Please wait a moment...
        </div>
    </div>
  </div>
);

export default PaymentProcessingModal;
