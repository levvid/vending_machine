import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PageFrame from './Start';

describe('<PageFrame />', () => {
  test('it should mount', () => {
    render(<PageFrame />);
    
    const pageFrame = screen.getByTestId('PageFrame');

    expect(pageFrame).toBeInTheDocument();
  });
});