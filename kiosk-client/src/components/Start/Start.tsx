import React from 'react';
import { useHistory } from "react-router-dom";
import ReactPlayer from 'react-player'
import styles from './Start.module.css';
import StartButton from '../StartButton/StartButton';
import WaveRipple from '../../assets/videos/WaveRipple.webm';
import BottomButtonNav from '../BottomButtonNav/BottomButtonNav';
import { preloadImages } from '../../services/utils/Images.service';

const Start: React.FC = (props: any) => {
  let history = useHistory();
  preloadImages();
  
  function NavigateSelectFlavor() {
    history.push('/bottle');
  }

  return (
  <div className={styles.Start} data-testid="Start">
    <div className={styles.ButtonWrap} 
    onMouseUp={NavigateSelectFlavor}>
    onTouchEnd={NavigateSelectFlavor}>
      <StartButton 
      />
    </div>
    <div className={styles.BottomButtonNavWrap}>
      <BottomButtonNav
      />
    </div>
    <div className={styles.playerWrap}>
      <ReactPlayer 
        className={styles.player}
        url={WaveRipple} 
        playing={true}
        loop={true}
        controls={false}
        width={1080}
        height={750}
        volume={0}
        muted={true}
      />
    </div>
  </div>
)
}

export default Start;
