import React from 'react';
import styles from './StartButton.module.css';
import startButton_img from '../../assets/images/buttons-start-btn.svg';

const StartButton: React.FC = () => (
  <div className={styles.StartButton} data-testid="StartButton">
    <img src={startButton_img} className={styles.ButtonsStartBtn} alt="Start Button" />
  </div>
);

export default StartButton;
