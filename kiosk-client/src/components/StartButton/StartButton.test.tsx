import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import StartButton from './StartButton';

describe('<StartButton />', () => {
  test('it should mount', () => {
    render(<StartButton />);
    
    const startButton = screen.getByTestId('StartButton');

    expect(startButton).toBeInTheDocument();
  });
});