import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PageFrameStatic from './PageFrameStatic';

describe('<PageFrameStatic />', () => {
  test('it should mount', () => {
    render(<PageFrameStatic />);
    
    const pageFrameStatic = screen.getByTestId('PageFrameStatic');

    expect(pageFrameStatic).toBeInTheDocument();
  });
});