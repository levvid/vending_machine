import React from 'react';
import styles from './PageFrameStatic.module.css';
import  PageFrameBackgroundGradient from '../../assets/images/PageFrameBackgroundGradient.svg';
import  PageFrameLowerSwoosh from '../../assets/images/PageFrameLowerSwoosh.svg';
import StartButton from '../StartButton/StartButton';
import BottomButtonNav from '../BottomButtonNav/BottomButtonNav';

const PageFrameStatic: React.FC = () => (
  <div 
      className={styles.PageFrameStatic} 
      data-testid="PageFrameStatic"
      style={{ backgroundImage: `url(${PageFrameBackgroundGradient})` }}
  >
    <div className={styles.ButtonWrap}>
      <StartButton />
    </div>
    <div 
      className={styles.PageFrameLowerSwoosh} 
      data-testid="PageFrameStatic"
      style={{ backgroundImage: `url(${PageFrameLowerSwoosh})` }}
    >

    <div className={styles.BottomButtonNavWrap}>
      <BottomButtonNav/>
    </div>
    </div>
  </div>
);

export default PageFrameStatic;
