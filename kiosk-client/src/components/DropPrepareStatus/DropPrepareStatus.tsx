import React, { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import { useHistory } from "react-router-dom";
import styles from "./DropPrepareStatus.module.css";
import { useAppSelector } from "../../services/redux/hooks";
import { selectCart } from "../../services/redux/slices/cartSlice";
import Header from "../Header/Header";
import { mapTagToImage } from "../../services/utils/Images.service";
import { PrepareWs } from "../../services/rxjs/PrepareWs.service";
import ErrorModal from "../ErrorModal/ErrorModal";
import classNames from "classnames";


const DropPrepareStatus: React.FC = () => {
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [circleBackground, setCircleBackground] = useState(
    classNames(styles.circleBackground, styles.defaultCircleBackground)
  );
  const history = useHistory();
  const cart: any = useAppSelector(selectCart);
  const flavorIcn = mapTagToImage(cart?.flavor);
  const caffeineIcn = mapTagToImage(cart?.caffeine);
  const bottleLogo = mapTagToImage("bottle_logo");
  const bottle = mapTagToImage(cart?.order_type);

  // text below bottle
  const [title, setTitle] = useState("Almost There");
  const [copy, setCopy] = useState("Just a few more seconds...");

  // default values, really place holders for state type
  const emptyBottle = mapTagToImage("empty_drop_bottle");
  const defaultLabel = mapTagToImage("bottle_logo");

  // switches for labels
  const [showUpperLabel, setShowUpperLabel] = useState(false);
  const [showCenterLabel, setShowCenterLabel] = useState(true);
  const [showLowerLabel, setShowLowerLabel] = useState(false);

  const [bottleIcn, setBottleIcn] = useState(emptyBottle);
  const [upperLabel, setUpperLabel] = useState(bottleLogo);
  const [centerLabel, setCenterLabel] = useState(defaultLabel);
  const [lowerLabel, setLowerLabel] = useState(bottleLogo);

  const [step, setStep] = useState(0);

  useEffect(() => {
    setShowUpperLabel(false);
    setShowCenterLabel(false);
    setShowLowerLabel(false);

    const timer = setTimeout(() => {
      handleTimeOut()
    }, 4000);

    // Step 0
    if (step === 0) {
      // caffeine
      if (cart.caffeine_level === 0) {
        //setLowerLabel(mapTagToImage('NONE | 0 MG'));
        setShowLowerLabel(false);
      } else if (cart.caffeine_level === 1) {
        setLowerLabel(mapTagToImage("Some | 20 MG"));
        setShowLowerLabel(true);
      } else if (cart.caffeine_level === 2) {
        setLowerLabel(mapTagToImage("More | 40 MG"));
        setShowLowerLabel(true);
      } else if (cart.caffeine_level === 3) {
        setLowerLabel(mapTagToImage("Extra | 70 MG"));
        setShowLowerLabel(true);
      }

      // flavors
      if (cart.flavor === "Water") {
        setUpperLabel(mapTagToImage("Water"));
        setShowUpperLabel(true);
      } else if (cart.flavor === "Guava") {
        setUpperLabel(mapTagToImage("Guava"));
        setShowUpperLabel(true);
      } else if (cart.flavor === "Lemonade") {
        setUpperLabel(mapTagToImage("Lemonade"));
        setShowUpperLabel(true);
      } else if (cart.flavor === "Cucumber") {
        setUpperLabel(mapTagToImage("Cucumber"));
        setShowUpperLabel(true);
      }
      setShowCenterLabel(false);
      // set center label if upper or lower are not set
      if (showLowerLabel === false && showUpperLabel === true) {
        setShowCenterLabel(true);
        setShowUpperLabel(false);
        setShowLowerLabel(false);
        setCenterLabel(upperLabel);
      } else if (showLowerLabel === true && showUpperLabel === false) {
        setShowCenterLabel(true);
        setShowUpperLabel(false);
        setShowLowerLabel(false);
        setCenterLabel(lowerLabel);
      }
    } else {
      // Step 1
      // flavors
      if (cart.flavor === "Water") {
        setBottleIcn(mapTagToImage("drop_bottle"));
        setCircleBackground(classNames(styles.circleBackground, styles.waterCircleBackground));
      } else if (cart.flavor === "Guava") {
        setBottleIcn(mapTagToImage("guava_bottle"));
        setCircleBackground(classNames(styles.circleBackground, styles.guavaCircleBackground));
      } else if (cart.flavor === "Lemonade") {
        setBottleIcn(mapTagToImage("lemon_bottle"));
        setCircleBackground(classNames(styles.circleBackground, styles.lemonCircleBackground));
      } else if (cart.flavor === "Cucumber") {
        setBottleIcn(mapTagToImage("cucumber_bottle"));
        setCircleBackground(classNames(styles.circleBackground, styles.cucumberCircleBackground));
      } else {
        setBottleIcn(mapTagToImage("drop_bottle"));
        setCircleBackground(classNames(styles.circleBackground, styles.waterCircleBackground));
      }

      setTitle("Please take your drink");
      setCopy("Thank you for saving the planet, one plastic bottle at a time");
      setShowLowerLabel(false);
      setShowUpperLabel(false);
      setShowCenterLabel(true);
    }
    const prepareWs = PrepareWs.onMessage().subscribe((message: any) => {
      if (message.item === "door_open") {
        setStep(1);
      } else if (message.item === "door_closed") {
        history.push({
          pathname: "/",
        });
      } else if (message.item === "order_failed") {
        setShowErrorModal(true);
      }
    });
    return () => {
      clearTimeout(timer);
      prepareWs.unsubscribe();
    };
  }, [step]);

  // TODO Remove for production
  function handleClick() {
    if (step === 0) {
      setStep(1);
    } else {
      history.push({
        pathname: "/",
      });
    }
  }

  function handleTimeOut() {
    if (step === 0) {
      setStep(1);
    }   
  }
  return (
    <div
      className={styles.DropPrepareStatus}
      data-testid="DropPrepareStatus"
      onMouseUp={handleClick}
      onTouchEnd={handleClick}
    >
      <div className={styles.LogoHeader}>
        <Header />
      </div>

      <div className={circleBackground}>
          <div
            className={styles.icon}
            style={{
              backgroundImage: `url(${bottleIcn})`,
            }}
          >
            {showUpperLabel && (
              <div
                className={styles.upperLabel}
                style={{
                  backgroundImage: `url(${upperLabel})`,
                }}
              ></div>
            )}
            {showCenterLabel && (
              <div
                className={styles.centerLabel}
                style={{
                  backgroundImage: `url(${centerLabel})`,
                }}
              ></div>
            )}
            {showLowerLabel && (
              <div
                className={styles.lowerLabel}
                style={{
                  backgroundImage: `url(${lowerLabel})`,
                }}
              ></div>
            )}
          </div>
      </div>
      <div className={styles.title}>{title}</div>
      <div className={styles.copy}>{copy}</div>
      {showErrorModal && (
        <Modal open>
          <ErrorModal />
        </Modal>
      )}
    </div>
  );
};

export default DropPrepareStatus;
