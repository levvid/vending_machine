import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DropPrepareStatus from './DropPrepareStatus';

describe('<DropPrepareStatus />', () => {
  test('it should mount', () => {
    render(<DropPrepareStatus />);
    
    const dropPrepareStatus = screen.getByTestId('DropPrepareStatus');

    expect(dropPrepareStatus).toBeInTheDocument();
  });
});