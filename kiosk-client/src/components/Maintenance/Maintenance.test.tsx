import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Maintenance from './Maintenance';

describe('<Maintenance />', () => {
  test('it should mount', () => {
    render(<Maintenance />);
    
    const maintenance = screen.getByTestId('Maintenance');

    expect(maintenance).toBeInTheDocument();
  });
});