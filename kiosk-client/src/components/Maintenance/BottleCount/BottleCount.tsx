import React, { useState } from 'react';
import styles from './BottleCount.module.css';
import { isNumber, backspace } from '../../../services/utils/FormUtils.service';

const BottleCount: React.FC = () => {
  const [count1, setCount1] = useState('');
  const [count2, setCount2] = useState('');
  const [count3, setCount3] = useState('');

  const [towerRadio, setTowerRadio] = useState('1');

  function submit() {
    console.log(count1);
    console.log(count2);
    console.log(count3);
    console.log(towerRadio);
  }

  function getTowerAmt(towerNumber: string) {
    switch(towerNumber) {
      case '1':
        return count1;
      case '2':
        return count2;
      case '3':
        return count3;
      default:
        return '';
    }
  }

  function handleChange(e: any, towerNumber: string) {
    let value = e.target.value;

    if (value === '') {
      let oldVal = getTowerAmt(towerNumber);
      value = backspace(oldVal);
    } else if(!isNumber(value)) {
      return;
    }

    switch(towerNumber) {
      case '1':
        setCount1(value);
        break;
      case '2':
        setCount2(value);
        break;
      case '3':
        setCount3(value);
        break;
      default: 
        break;
    }
  }
  
  function handleTowerChange(towerNumber: string) {
    setTowerRadio(towerNumber);
  }

  return (
  <div className={styles.BottleCount} data-testid="BottleCount">
    <div className={styles.tableWrap}>
    <table>
      <tr>
        <th>Tower Number</th>
        <th>Active Tower</th>
        <th>Bottle Count</th>
      </tr>
      <tr>
        <td>1</td>
        <td>
          <label>
            <input key='towerRadio1' name='towerRadio' checked={towerRadio === '1'} onChange={() => handleTowerChange('1')} type="radio" value={towerRadio} />
            <span>Active</span>
          </label>
        </td>
        <td className="input-field">
            <input placeholder='Number Bottles' key='towerInput1' type="text" value={count1} onChange={(e) => handleChange(e, '1')}/>
        </td>
      </tr>

      <tr>
        <td>2</td>
        <td>
          <label>
            <input key='towerRadio2' name='towerRadio' checked={towerRadio === '2'} onChange={() => handleTowerChange('2')} type="radio" value={towerRadio} />
            <span>Active</span>
          </label>
        </td>
        <td className="input-field">
            <input placeholder='Number Bottles' key='towerInput2' type="text" value={count2} onChange={(e) => handleChange(e, '2')}/>
        </td>
      </tr>

      <tr>
        <td>3</td>
        <td>
          <label>
            <input key='towerRadio3' name='towerRadio' checked={towerRadio === '3'} onChange={() => handleTowerChange('3')} type="radio" value={towerRadio} />
            <span>Active</span>
          </label>
        </td>
        <td className="input-field">
            <input placeholder='Number Bottles' key='towerInput3' type="text" value={count3} onChange={(e) => handleChange(e, '3')}/>
        </td>
      </tr>
    </table>
    </div>
    <div className={styles.submitBtn}>
      <button className="btn waves-effect waves-light"  onClick={submit} name="action">Save Changes
      </button>
    </div>
  </div>
)};

export default BottleCount;
