import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BottleCount from './BottleCount';

describe('<BottleCount />', () => {
  test('it should mount', () => {
    render(<BottleCount />);
    
    const bottleCount = screen.getByTestId('BottleCount');

    expect(bottleCount).toBeInTheDocument();
  });
});