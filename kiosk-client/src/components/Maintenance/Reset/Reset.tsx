import React from 'react';
import styles from './Reset.module.css';
import { useHistory } from "react-router-dom";

const Reset: React.FC = () => {
  let history = useHistory();

  function resetKiosk() {
    console.log('Resetting Kiosk');
    history.push('/home');
  }

  function exit() {
    history.push('/home');
  }

  return (
    <div className={styles.Reset} data-testid="Reset">
      <div className={styles.container}>
        <div className={styles.upperText}>

          Reseting the kiosk is needed to make changes take effect.
        </div>
        <div className={styles.lowerButtons}>
          <button className="btn waves-effect waves-light" onClick={resetKiosk} name="action">Reset Kiosk</button>
          <button className="btn waves-effect waves-light" onClick={exit} name="action">Exit Maintenance</button>
        </div>
      </div>
    </div>
  )
};

export default Reset;
