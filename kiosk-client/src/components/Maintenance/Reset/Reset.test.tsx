import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Reset from './Reset';

describe('<Reset />', () => {
  test('it should mount', () => {
    render(<Reset />);
    
    const reset = screen.getByTestId('Reset');

    expect(reset).toBeInTheDocument();
  });
});