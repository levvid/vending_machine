import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Additives from './Additives';

describe('<Additives />', () => {
  test('it should mount', () => {
    render(<Additives />);
    
    const additives = screen.getByTestId('Additives');

    expect(additives).toBeInTheDocument();
  });
});