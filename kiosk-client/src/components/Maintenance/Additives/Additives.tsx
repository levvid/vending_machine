import React, { useState } from 'react';
// flavors
import CucumIcn from '../../../assets/images/cucumberIcn.svg';
import GuavaIcn from '../../../assets/images/guavaIcn.svg';
import LemonIcn from '../../../assets/images/lemonadeIcn.svg';
import CusaMango from '../../../assets/images/cusaMango-drop-mini.png';
// caffeine
import Caffeine0 from '../../../assets/images/icn-caffeine-0-mg.svg';

import styles from './Additives.module.css';
import { isReturnStatement } from 'typescript';

const Additives: React.FC = () => {
  const [pumpRadioSel1, setPumpRadioSel1] = useState('guava');
  const [pumpRadioSel2, setPumpRadioSel2] = useState('guava');
  const [pumpRadioSel3, setPumpRadioSel3] = useState('guava');
  const [pumpRadioSel4, setPumpRadioSel4] = useState('guava');
  const [pumpRadioSel5, setPumpRadioSel5] = useState('guava');
  const [pumpRadioSel6, setPumpRadioSel6] = useState('guava');

  const [pumpAmt1, setPumpAmt1] = useState('');
  const [pumpAmt2, setPumpAmt2] = useState('');
  const [pumpAmt3, setPumpAmt3] = useState('');
  const [pumpAmt4, setPumpAmt4] = useState('');
  const [pumpAmt5, setPumpAmt5] = useState('');
  const [pumpAmt6, setPumpAmt6] = useState('');

  interface IpumpSettingsProps {
    pumpAmountId: string;
    pumpAmt: string;
    pumpRadioId: string;
    pumpNumber: string;
    pumpRadioSel: string;
  }
  function submit() {
    console.log(pumpRadioSel1);
    console.log(pumpRadioSel2);
    console.log(pumpRadioSel3);
    console.log(pumpRadioSel4);
    console.log(pumpRadioSel5);
    console.log(pumpRadioSel6);
    console.log(pumpAmt1);
    console.log(pumpAmt2);
    console.log(pumpAmt3);
    console.log(pumpAmt4);
    console.log(pumpAmt5);
    console.log(pumpAmt6);
  }

  function handleRadioChange(pumpNumber: string, value: string) {
    switch(pumpNumber) {
      case '1':
        setPumpRadioSel1(value);
        break;
      case '2':
        setPumpRadioSel2(value);
        break;
      case '3':
        setPumpRadioSel3(value);
        break;
      case '4':
        setPumpRadioSel4(value);
        break;
      case '5':
        setPumpRadioSel5(value);
        break;
      case '6':
        setPumpRadioSel6(value);
        break;
      default: 
        break;
    }
  }
  function getPumpAmt(pumpNumber: string) {
    switch(pumpNumber) {
      case '1':
        return pumpAmt1;
        break;
      case '2':
        return pumpAmt2;
        break;
      case '3':
        return pumpAmt3;
        break;
      case '4':
        return pumpAmt4;
        break;
      case '5':
        return pumpAmt5;
        break;
      case '6':
        return pumpAmt6;
        break;
      default: 
        return '';
        break;
    }
  }

  function backspace(pumpNumber: string) {
    let value = getPumpAmt(pumpNumber);

    if (value.length > 1) {
      return value.substring(0, value.length - 2);
    } else {
      return '';
    }
  }

  const isNumber = new RegExp('^[0-9]+$');

  function handleAmtChange(e: any, pumpNumber: string) {
    let value = e.target.value;

    if (value === '') {
      value = backspace(pumpNumber);
    } else if(!isNumber.test(value)) {
      return;
    }

    switch(pumpNumber) {
      case '1':
        setPumpAmt1(value);
        break;
      case '2':
        setPumpAmt2(value);
        break;
      case '3':
        setPumpAmt3(value);
        break;
      case '4':
        setPumpAmt4(value);
        break;
      case '5':
        setPumpAmt5(value);
        break;
      case '6':
        setPumpAmt6(value);
        break;
      default: 
        break;
    }
  }

  function TableHeader() {
    return (
      <tr>
      <th>Pump Number</th>
      <th>Amount</th>
      <th>
        <div className={styles.icon}
            style={{ backgroundImage: `url(${GuavaIcn})`}}
          >
        </div>
      </th>
      <th>
        <div className={styles.icon}
            style={{ backgroundImage: `url(${CucumIcn})`}}
          >
        </div>
      </th>
      <th>
        <div className={styles.icon}
            style={{ backgroundImage: `url(${LemonIcn})`}}
          >
        </div>
      </th>
      <th>
        <div className={styles.icon}
            style={{ backgroundImage: `url(${CusaMango})`}}
          >
        </div>
      </th>
      <th>
        <div className={styles.icon}
            style={{ backgroundImage: `url(${Caffeine0})`}}
          >
        </div>
      </th>
      </tr>
    )

  }

  const pumps: IpumpSettingsProps[] = [
    {
      pumpAmountId: 'pumpAmount1',
      pumpAmt: pumpAmt1,
      pumpRadioId: 'pumpRadio1',
      pumpNumber: '1',
      pumpRadioSel: pumpRadioSel1
    },
    {
      pumpAmountId: 'pumpAmount2',
      pumpAmt: pumpAmt2,
      pumpRadioId: 'pumpRadio2',
      pumpNumber: '2',
      pumpRadioSel: pumpRadioSel2
    },
    {
      pumpAmountId: 'pumpAmount3',
      pumpAmt: pumpAmt3,
      pumpRadioId: 'pumpRadio3',
      pumpNumber: '3',
      pumpRadioSel: pumpRadioSel3
    },
    {
      pumpAmountId: 'pumpAmount4',
      pumpAmt: pumpAmt4,
      pumpRadioId: 'pumpRadio4',
      pumpNumber: '4',
      pumpRadioSel: pumpRadioSel4
    },
    {
      pumpAmountId: 'pumpAmount5',
      pumpAmt: pumpAmt5,
      pumpRadioId: 'pumpRadio5',
      pumpNumber: '5',
      pumpRadioSel: pumpRadioSel5
    },
    {
      pumpAmountId: 'pumpAmount6',
      pumpAmt: pumpAmt6,
      pumpRadioId: 'pumpRadio6',
      pumpNumber: '6',
      pumpRadioSel: pumpRadioSel6
    },
  ];

  return (
    <div className={styles.Additives} data-testid="Additives">
      <div className={styles.pumpFormContainer}>
        <table>
          <TableHeader />
          {
            pumps.map(item => {
              return (
                <tr>
                  <td>
                    {item.pumpNumber}
                  </td>
                  <td className="input-field">
                    <input placeholder='Ounces' type="text" id={item.pumpAmountId} value={item.pumpAmt} onChange={(e) => handleAmtChange(e, item.pumpNumber)} />
                  </td>
                  <td>
                    <label>
                      <input id={item.pumpRadioId} name={item.pumpRadioId} checked={item.pumpRadioSel === 'guava'} onChange={() => handleRadioChange(item.pumpNumber, 'guava')} type="radio" value="guava" />
                      <span>Guava</span>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input id={item.pumpRadioId} name={item.pumpRadioId} checked={item.pumpRadioSel === 'cucumber'} onChange={() => handleRadioChange(item.pumpNumber, 'cucumber')} type="radio" value="cucumber" />
                      <span>Cucumber</span>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input id={item.pumpRadioId} name={item.pumpRadioId} checked={item.pumpRadioSel === 'lemonade'} onChange={() => handleRadioChange(item.pumpNumber, 'lemonade')} type="radio" value="lemonade" />
                      <span>Lemonade</span>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input id={item.pumpRadioId} name={item.pumpRadioId} checked={item.pumpRadioSel === 'cusamango'} onChange={() => handleRadioChange(item.pumpNumber, 'cusamango')} type="radio" value="cusamango" />
                      <span>Cusa Mango</span>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input id={item.pumpRadioId} name={item.pumpRadioId} checked={item.pumpRadioSel === 'caffeine'} onChange={() => handleRadioChange(item.pumpNumber, 'caffeine')} type="radio" value="caffeine" />
                      <span>Caffeine</span>
                    </label>
                  </td>
                </tr>

              )
            })
          }
        </table>
      </div>
      <div className={styles.submitBtn}>
        <button className="btn waves-effect waves-light" onClick={submit} name="action">Save Changes
        </button>
      </div>
    </div>
  )
};

export default Additives;
