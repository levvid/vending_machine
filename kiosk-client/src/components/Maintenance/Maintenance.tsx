import React, { useState } from 'react';
import styles from './Maintenance.module.css';
import Header from '../Header/Header';
import MenuSwitch from './MenuSwitch/MenuSwitch';
import { useHistory } from "react-router-dom";

const Maintenance: React.FC = () => {
  const history = useHistory();

  const [currentComponent, setCurrentComponent] = useState('additives')

  function  handleClick(menuOpt: string) {
    console.log(menuOpt);
    setCurrentComponent(menuOpt);
  }

  return (
    <div className={styles.Maintenance} data-testid="Maintenance">
      <div className={styles.LogoHeader}>
        <Header />
      </div>
      <div className={styles.mainPage}>
        <div className={styles.navBar}
        >
          <nav>
            <div className="nav-wrapper">
              <a href="#" className="brand-logo">&nbsp;dropwater</a>
              <ul id="nav-mobile" className="right hide-on-med-and-down">
                <li><a href='#' onClick={() => handleClick('Additives')}>Additives</a></li>
                <li><a href='#' onClick={() => handleClick('BottleCount')}>Bottle Count</a></li>
                <li><a href='#' onClick={() => handleClick('Pricing')}>Pricing</a></li>
                <li><a href='#' onClick={() => handleClick('KioskControls')}>Kiosk Controls</a></li>
                <li><a href='#' onClick={() => handleClick('Reset')}>Exit</a></li>
              </ul>
            </div>
          </nav>
          <div className={styles.container}>
            <MenuSwitch item={currentComponent} />
          </div>
        </div>
      </div>
    </div>
)};

export default Maintenance;
