import React from 'react';
import Additives from '../Additives/Additives';
import BottleCount from '../BottleCount/BottleCount';
import Pricing from '../Pricing/Pricing';
import KioskControls from '../KioskControls/KioskControls';
import Reset from '../Reset/Reset';

export interface MenuSwitchProps {
  item?: string;
}

const MenuSwitch: React.FC<MenuSwitchProps> = (props) => {

  switch (props.item) {
    case 'Additives' :
      return (<Additives />)
    case 'BottleCount' :
      return (<BottleCount />)
    case 'Pricing' :
      return (<Pricing />)
    case 'KioskControls' :
      return (<KioskControls />)
    case 'Reset':
      return (<Reset />)
    default: 
      return (<Additives />)
  }
};

export default MenuSwitch;
