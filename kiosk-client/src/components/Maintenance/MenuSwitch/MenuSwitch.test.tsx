import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MenuSwitch from './MenuSwitch';

describe('<MenuSwitch />', () => {
  test('it should mount', () => {
    render(<MenuSwitch />);
    
    const menuSwitch = screen.getByTestId('MenuSwitch');

    expect(menuSwitch).toBeInTheDocument();
  });
});