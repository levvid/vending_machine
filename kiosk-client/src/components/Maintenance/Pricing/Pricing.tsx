import React, { useState } from 'react';
import { isNoSubstitutionTemplateLiteral } from 'typescript';
import styles from './Pricing.module.css';
import { isNumber, backspace } from '../../../services/utils/FormUtils.service';

const Pricing: React.FC = () => {
  const [plainWaterDB, setPlainWaterDB] = useState();
  const [flavoredDB, setFlavoredDB] = useState();
  const [supplementDB, setSupplementDB] = useState();
  const [plainWaterRefill, setPlainWaterRefill] = useState();
  const [flavoredRefill, setFlavoredRefill] = useState();
  const [supplementRefill, setSupplementRefill] = useState();
  
  function submit() {
    console.log(plainWaterDB);
    console.log(flavoredDB);
    console.log(supplementDB);
    console.log(plainWaterRefill);
    console.log(flavoredRefill);
    console.log(supplementRefill);
  }

  function getPrevCost(type: string) {

    switch(type) {
      case 'plainDB':
        return plainWaterDB;
      case 'flavorDB':
        return flavoredDB;
      case 'supplementDB':
        return supplementDB
      case 'plainRefill':
        return plainWaterRefill;
      case 'flavorRefill':
        return flavoredRefill;
      case 'supplementRefill':
        return supplementRefill
      default:
        return '';
    }
  }

  function handleChange(e: any, type: string) {
    let value = e.target.value;

    if (value === '') {
      let oldVal: any = getPrevCost(type);
      value = backspace(oldVal);
    } else if(!isNumber(value)) {
      return;
    }

    switch(type) {
      case 'plainDB':
        setPlainWaterDB(value);
        break;
      case 'flavorDB':
        setFlavoredDB(value);
        break;
      case 'supplementDB':
        setSupplementDB(value);
        break;
      case 'plainRefill':
        setPlainWaterRefill(value);
        break;
      case 'flavorRefill':
        setFlavoredRefill(value);
        break;
      case 'supplementRefill':
        setSupplementRefill(value);
        break;
    }
  }

  
  return (
  <div className={styles.Pricing} data-testid="Pricing">
    <div className={styles.tableWrap}>
    <table>
      <tr>
        <th colSpan={2}>Drop Bottle (Cents / Bottle)</th>
      </tr>
      <tr>
        <td>Plain Water: </td>
        <td className="input-field">
            <input  placeholder='Cost in Cents' key='plainWaterDB' type="text" value={plainWaterDB} onChange={(e) => handleChange(e, 'plainDB')}/>
        </td>
      </tr>
      <tr>
        <td>Flavored Drink: </td>
        <td className="input-field">
            <input placeholder='Cost in Cents' key='flavoredDB' type="text" value={flavoredDB} onChange={(e) => handleChange(e, 'flavorDB')}/>
        </td>
      </tr>
      <tr>
        <td>Supplement: </td>
        <td className="input-field">
            <input placeholder='Cost in Cents' key='supplementDB' type="text" value={supplementDB} onChange={(e) => handleChange(e, 'supplementDB')}/>
        </td>
      </tr>
      <tr>
        <th colSpan={2}>Refill (Cents / 16oz)</th>
      </tr>
      <tr>
        <td>Plain Water (Always Free):</td>
        <td className="input-field">
            <input placeholder='Cost in Cents' key='plainWaterRefill' type="text" value={plainWaterRefill} onChange={(e) => handleChange(e, 'plainRefill')}/>
        </td>
      </tr>
      <tr>
        <td>Flavored Drink: </td>
        <td className="input-field">
            <input placeholder='Cost in Cents' key='flavoredRefill' type="text" value={flavoredRefill} onChange={(e) => handleChange(e, 'flavorRefill')}/>
        </td>
      </tr>
      <tr>
        <td>Supplement: </td>
        <td className="input-field">
            <input placeholder='Cost in Cents' key='supplementRefill' type="text" value={supplementRefill} onChange={(e) => handleChange(e, 'supplementRefill')}/>
        </td>
      </tr>
    </table>
    </div>
    <div className={styles.submitBtn}>
      <button className="btn waves-effect waves-light"  onClick={submit} name="action">Save Changes
      </button>
    </div>
  </div>
)};

export default Pricing;
