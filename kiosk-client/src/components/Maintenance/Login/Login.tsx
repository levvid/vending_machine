import React, { useState } from 'react';
import styles from './Login.module.css';
import { useHistory } from "react-router-dom";
import Modal from '@material-ui/core/Modal';
import { HomeLoginService } from "../../../services/rxjs/HomeLogin.service";

const Login: React.FC = () => {
  let history = useHistory();
  const [loginId, setLoginId] = useState('');
  const [password, setPassword] = useState('');
  const [loginError, setLoginError] = useState(false);
  const secureLoginId = 'admin';
  const secureLoginPw = '3507';


  function validate() {
    if((loginId === secureLoginId) && (password === secureLoginPw)) {
      history.push('/maintenance');
    } else {
      setLoginError(true);
    }
  }

  function cancel() {
    HomeLoginService.sendMessage('hide')
  }

  function handleClick(e: any) {
    e.stopPropagation();
  }

  
  return (
  <div className={styles.Login} data-testid="Login">
      <Modal open >
        <div className={styles.loginFormContainer} onClick={handleClick}>
          <div className={styles.loginForm}>
            <div className={styles.loginFormElt}>
              {loginError && <div className={styles.loginError}>
                  Invalid Credentials
                </div>}
            </div>
            <div className={styles.loginFormElt}>
              <input placeholder='Login' type="text" value={loginId} onClick={handleClick} onChange={(e) => setLoginId(e.target.value)} />
            </div>
            <div className={styles.loginFormElt}>
              <input placeholder='Password' type="password" value={password} onClick={handleClick} onChange={(e) => setPassword(e.target.value)} />
            </div>
            <div className={styles.loginFormElt}>
              <div className={styles.buttonWrap}>
                <button className="btn waves-effect waves-light" onClick={cancel} name="action">Cancel</button>
                <button className="btn waves-effect waves-light" onClick={validate} name="action">Login</button>
              </div>
            </div>
          </div>
        </div>
    </Modal>
</div>
)};

export default Login;
