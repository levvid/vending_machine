import React, {useState} from 'react';
import styles from './KioskControls.module.css';

/*
    "Raise Left Tower",
    "Raise Right Tower",
    "Lower Left Tower",
    "Lower Right Tower",
    "Purge Additive 1 Refill",
    "Purge Additive 2 Refill",
    "Purge Additive 3 Refill",
    "Purge Additive 4 Refill",
    "Purge Additive 5 Refill",
    "Purge Additive 6 Refill",
    "Flush Refill (Ambient)",
    "Flush Refill (Cold)",
*/

const KioskControls: React.FC = () => {
  const [command, setCommand] = useState('');

  function submit() {
    console.log(command);
  }

  return (
  <div className={styles.KioskControls} data-testid="KioskControls">
    <table>
      <tr>
        <th colSpan={4}>
          Select Command To Run
        </th>
      </tr>
      <tr>
        <td>
          <label>
            <input key="RaiseLeftTower" name="RaiseLeftTower" checked={command === 'RaiseLeftTower'} onChange={() => setCommand('RaiseLeftTower')} type="radio"/>
            <span>Raise Left Tower</span>
          </label>
        </td>
        <td>
          <label>
            <input key="RaiseRightTower" name="RaiseRightTower" checked={command === 'RaiseRightTower'} onChange={() => setCommand('RaiseRightTower')} type="radio"/>
            <span>Raise Right Tower</span>
          </label>
        </td>
        <td>
          <label>
            <input key="LowerLeftTower" name="LowerLeftTower" checked={command === 'LowerLeftTower'} onChange={() => setCommand('LowerLeftTower')} type="radio"/>
            <span>Lower Left Tower</span>
          </label>
        </td>
        <td>
          <label>
            <input key="LowerRightTower" name="LowerRightTower" checked={command === 'LowerRightTower'} onChange={() => setCommand('LowerRightTower')} type="radio"/>
            <span>Lower Right Tower</span>
          </label>
        </td>
        </tr>
        <tr>
        <td>
          <label>
            <input key="PurgeAdditive1" name="PurgeAdditive1" checked={command === 'PurgeAdditive1'} onChange={() => setCommand('PurgeAdditive1')} type="radio"/>
            <span>Purge Additive Refill 1</span>
          </label>
        </td>
        <td>
          <label>
            <input key="PurgeAdditive2" name="PurgeAdditive2" checked={command === 'PurgeAdditive2'} onChange={() => setCommand('PurgeAdditive2')} type="radio"/>
            <span>Purge Additive Refill 2</span>
          </label>
        </td>
        <td>
          <label>
            <input key="PurgeAdditive3" name="PurgeAdditive3" checked={command === 'PurgeAdditive3'} onChange={() => setCommand('PurgeAdditive3')} type="radio"/>
            <span>Purge Additive Refill 3</span>
          </label>
        </td>
        <td>
          <label>
            <input key="PurgeAdditive4" name="PurgeAdditive4" checked={command === 'PurgeAdditive4'} onChange={() => setCommand('PurgeAdditive4')} type="radio"/>
            <span>Purge Additive Refill 4</span>
          </label>
        </td>
      </tr>
      <tr>
        <td>
          <label>
            <input key="PurgeAdditive5" name="PurgeAdditive5" checked={command === 'PurgeAdditive5'} onChange={() => setCommand('PurgeAdditive5')} type="radio"/>
            <span>Purge Additive Refill 5</span>
          </label>
        </td>
        <td>
          <label>
            <input key="PurgeAdditive6" name="PurgeAdditive6" checked={command === 'PurgeAdditive6'} onChange={() => setCommand('PurgeAdditive6')} type="radio"/>
            <span>Purge Additive Refill 6</span>
          </label>
        </td>

        <td>
          <label>
            <input key="FlushRefillAmbient" name="FlushRefillAmbient" checked={command === 'FlushRefillAmbient'} onChange={() => setCommand('FlushRefillAmbient')} type="radio"/>
            <span>Flush Refill (Ambient)</span>
          </label>
        </td>
        <td>
          <label>
            <input key="FlushRefillCold" name="FlushRefillCold" checked={command === 'FlushRefillCold'} onChange={() => setCommand('FlushRefillCold')} type="radio"/>
            <span>Flush Refill (Cold)</span>
          </label>
        </td>
      </tr>
    </table>
    <div className={styles.submitBtn}>
      <button className="btn waves-effect waves-light"  onClick={submit} name="action">Execute Command
    </button>
    </div>
  </div>
)};

export default KioskControls;
