import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import KioskControls from './KioskControls';

describe('<KioskControls />', () => {
  test('it should mount', () => {
    render(<KioskControls />);
    
    const kioskControls = screen.getByTestId('KioskControls');

    expect(kioskControls).toBeInTheDocument();
  });
});