import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import PourVideo from "../../assets/videos/pour.gif";
import BlobVideo from "../../assets/videos/blob.webm";
import RippleVideo from "../../assets/videos/ripple.gif";
import WaveVideo from "../../assets/videos/wave.gif";
import styles from "./RefillPour.module.css";
import { useHistory } from "react-router-dom";
import Check from "../../assets/images/CheckIcn.svg";
import Header from "../Header/Header";
import CartDisplay from "../CartDisplay/CartDisplay";
import classNames from "classnames";
import { useAppDispatch } from "../../services/redux/hooks";
import {
  startPourOrder,
  startPour,
  stopPour,
  finishPour,
} from "../../services/redux/slices/cartSlice";
import { mapTagToImage } from "../../services/utils/Images.service";
import { RefillPourWs } from "../../services/rxjs/RefillPourWs.service";

const RefillPour: React.FC = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const RefillLeftSlice = mapTagToImage("refill-left-slice");
  const [isPouring, setIsPouring] = useState(false);
  const [pourBackground, setPourBackground] = useState(
    classNames(styles.pourBtn, styles.defaultBackgroundBtn)
  );
  const [doneBackground, setDoneBackground] = useState(
    classNames(styles.doneBtn, styles.defaultBackgroundBtn)
  );
  const [pourVideoWrap, setPourVideoWrap] = useState(
    classNames(styles.pourVideoWrap, styles.hidePourVideoWrap)
  );

  //  Default state show wave hide ripple
  const [waveVideoWrap, setWaveVideoWrap] = useState(
    classNames(styles.waveRippleVideoWrap, styles.showWaveVideoWrap)
  );
  const [rippleVideoWrap, setRippleVideoWrap] = useState(
    classNames(styles.waveRippleVideoWrap, styles.hideRippleVideoWrap)
  );

  const [unitCost, setUnitCost] = useState(0);
  const [pouredAmt, setPouredAmt] = useState(0);
  const [totalAmt, setTotalAmt] = useState(0);

  const unitCostClass = classNames(styles.costContainer, styles.unitCost);
  const pouredAmtClass = classNames(styles.costContainer, styles.pouredAmt);
  const totalAmtClass = classNames(styles.costContainer, styles.totalAmt);

  useEffect(() => {
    //let cnames = classNames(styles.iconHighlight, this.mapActiveClass(this.activeClass));
    console.log("about to make request");
    dispatch(startPourOrder());

    RefillPourWs.sendMessage("begin_refill");

    // listen for pour status
    const refillPourWs = RefillPourWs.onMessage().subscribe((obj: any) => {
      console.log(obj?.item?.message);
      const topic = obj?.item?.message?.topic;
      if (topic === "refill_order_status") {
        console.log("received message!");
        let data = obj?.item?.message?.data;
        if (typeof data !== "string") {
          setUnitCost(0.49);
          setPouredAmt(data.water_amount);
          setTotalAmt(data.price / 100);
        }
      }
    });
    return () => {
      refillPourWs.unsubscribe();
    };
  }, []);

  function handlePourStart() {
    console.log("handle pour start");
    setIsPouring(true)
    setPourBackground(classNames(styles.pourBtn, styles.pressBackgroundBtn));
    setPourVideoWrap(
      classNames(styles.pourVideoWrap, styles.showPourVideoWrap)
    );
    //dispatch(startPour())
    RefillPourWs.sendMessage("start_pour");

    //  Pour state show wave hide ripple
    setWaveVideoWrap(
      classNames(styles.waveRippleVideoWrap, styles.hideWaveVideoWrap)
    );
    setRippleVideoWrap(
      classNames(styles.waveRippleVideoWrap, styles.showRippleVideoWrap)
    );
  }

  function handlePourStop() {
    console.log("handle pour end");
    setIsPouring(false);
    setPourBackground(classNames(styles.pourBtn, styles.defaultBackgroundBtn));
    setPourVideoWrap(
      classNames(styles.pourVideoWrap, styles.hidePourVideoWrap)
    );
    //dispatch(stopPour())
    RefillPourWs.sendMessage("stop_pour");

    //  Default state show wave hide ripple
    setWaveVideoWrap(
      classNames(styles.waveRippleVideoWrap, styles.showWaveVideoWrap)
    );
    setRippleVideoWrap(
      classNames(styles.waveRippleVideoWrap, styles.hideRippleVideoWrap)
    );
  }

  function handlePourMovedAway() {
    if (isPouring) {
      handlePourStop()
    }
  }

  function handleDoneStart() {
    console.log("handle done start");
    setDoneBackground(classNames(styles.doneBtn, styles.pressBackgroundBtn));
  }

  function handleDoneMovedAway() {
    setDoneBackground(classNames(styles.doneBtn, styles.defaultBackgroundBtn));
  }

  function handleDoneStop() {
    console.log("handle done end");
    //dispatch(finishPour())
    RefillPourWs.sendMessage("end_refill");
    setDoneBackground(classNames(styles.doneBtn, styles.defaultBackgroundBtn));
    history.push({
      pathname: "/",
    });
  }

  return (
    <div className={styles.RefillPour} data-testid="RefillPour">
      <div className={styles.LogoHeader}>
        <Header />
      </div>
      <div className={styles.blobVideoWrap}>
        <div>
          <ReactPlayer
            className={styles.player}
            url={BlobVideo}
            playing={true}
            loop={true}
            controls={false}
            width={"100%"}
            height={"100%"}
          />
        </div>
      </div>
      <div className={styles.refillInstructionsWrap}>
        <div
          className={styles.refillLeftSlice}
          style={{
            backgroundImage: `url(${RefillLeftSlice})`,
            backgroundRepeat: "no-repeat",
            objectFit: "contain",
          }}
        ></div>
        <div className={styles.refillInstructionsText}>
          Place your bottle under the dispenser on the left
        </div>
      </div>

      <div className={pourVideoWrap}>
        <img src={PourVideo} />
      </div>

      <div className={styles.cartDisplayWrap}>
        <CartDisplay direction="column" showText={false} />
      </div>

      <div className={styles.pourWaterLower}>
        <div className={waveVideoWrap}>
          <img src={WaveVideo} />
        </div>

        <div className={rippleVideoWrap}>
          <img src={RippleVideo} />
        </div>

        <div className={styles.priceContainer}>
          <div className={unitCostClass}>
            ${unitCost?.toFixed(2)}
            <br />
            PER 16oz
          </div>
          <div className={pouredAmtClass}>
            {pouredAmt}oz
            <br />
            POURED
          </div>
          <div className={totalAmtClass}>
            ${totalAmt.toFixed(2)}
            <br />
            PRICE
          </div>
        </div>
        <div
          className={pourBackground}
          onMouseDown={handlePourStart}
          onTouchStart={handlePourStart}
          onMouseLeave={handlePourMovedAway}
          onMouseUp={handlePourStop}
          onTouchEnd={handlePourStop}
        >
          <div className={styles.pourBtnLineOne}>POUR</div>
          <div className={styles.pourBtnLineTwo}>PRESS + HOLD</div>
        </div>

        <div
          className={doneBackground}
          onMouseDown={handleDoneStart}
          onTouchStart={handleDoneStart}
          onMouseLeave={handleDoneMovedAway}
          onMouseUp={handleDoneStop}
          onTouchEnd={handleDoneStop}
        >
          <div
            className={styles.checkIcn}
            style={{
              backgroundImage: `url(${Check})`,
              backgroundRepeat: "no-repeat",
            }}
          ></div>
          <div className={styles.doneLineOne}>DONE</div>
        </div>
      </div>
    </div>
  );
};

export default RefillPour;
