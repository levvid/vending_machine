import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import RefillPour from './RefillPour';

describe('<RefillPour />', () => {
  test('it should mount', () => {
    render(<RefillPour />);
    
    const refillPour = screen.getByTestId('RefillPour');

    expect(refillPour).toBeInTheDocument();
  });
});