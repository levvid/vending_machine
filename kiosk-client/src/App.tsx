import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import React, { useEffect } from "react";
import BottleSelectPage from "./components/BottleSelectPage/BottleSelectPage";
import DrinkBuilder from "./components/DrinkBuilder/DrinkBuilder";
import Payment from "./components/Payment/Payment";
import RefillPour from "./components/RefillPour/RefillPour";
import DropPrepare from "./components/DropPrepare/DropPrepare";
import DropPrepareStatus from "./components/DropPrepareStatus/DropPrepareStatus";
import Home from "./components/Home/Home";
import Maintenance from "./components/Maintenance/Maintenance";
import OutOfOrder from "./components/OutOfOrderPage/OutOfOrder";
import OrderPage from "./components/DrinkBuilder/DrinkBuilder";
import IdleTimer, { useIdleTimer } from "react-idle-timer";
import SystemInitModal from "./components/SystemInitModal/SystemInitModal";
import { IdleUserTimeout } from "./services/rxjs/IdleUserTimeout.service";

function App() {
  const handleOnIdle = () => {
    console.log("user is idle");
    console.log("last active", getLastActiveTime());
    IdleUserTimeout.sendMessage("showIdleUserModal");
  };

  const handleOnActive = () => {
    console.log("user is active");
    console.log("time remaining", getRemainingTime());
  };

  const handleOnAction = () => {
    console.log("user did something");
  };

  const { getRemainingTime, getLastActiveTime } = useIdleTimer({
    timeout: 1000 * 40,
    onIdle: handleOnIdle,
    onActive: handleOnActive,
    onAction: handleOnAction,
    debounce: 500,
  });

  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/start" component={Home} />
          <Route exact path="/order" component={OrderPage} />
          <Route exact path="/bottle" component={BottleSelectPage} />
          <Route exact path="/builder" component={DrinkBuilder} />
          <Route exact path="/payment" component={Payment} />
          <Route exact path="/refill" component={RefillPour} />
          <Route exact path="/prepare" component={DropPrepare} />
          <Route exact path="/preparestatus" component={DropPrepareStatus} />
          <Route exact path="/maintenance" component={Maintenance} />
          <Route exact path="/outoforder" component={OutOfOrder} />
          <Route exact path="/init" component={SystemInitModal} />
          <Route exact path="*" component={Home} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
