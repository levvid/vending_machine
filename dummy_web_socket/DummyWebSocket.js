const WebSocket = require("ws");

exports.kioskClient = function () {
  let webSocket = new WebSocket.Server({ port: 8888, path: "/kiosk_client" });
  webSocket.on("connection", (ws) => {
    console.log("connection");

    ws.on("message", (data) => {
      switch (data) {
        case "start_drop":
          console.log('DEBUG got start drop command');
          processDrink(ws);
          break;
        case "start_payment":
          console.log('DEBUG got start payment command');
          processPayment(ws);
          break;
        case "begin_refill":
            beginRefill(ws);
            console.log('DEBUG begin refill');
            break;
        case "end_refill":
            endRefill();
            console.log('DEBUG end refill');
            break;
        case "start_pour":
            startPour()
            console.log('DEBUG start pour');
            break;
        case "stop_pour":
            stopPour()
            console.log('DEBUG stop pour');
            break;
        default:
          ws.send(data);
          break;
      }
    });
    ws.on("close", (data) => {
        console.log('CLOSED Huh??? why???');
        console.log(data);
    });
    ws.on("error", (data) => {
        console.log('CLOSED Huh??? why???');
        console.log(data);
    });
  });
};

const refill_status =  { 
    'message': {
         'topic': 'refill_order_status',
         'data': { 
             'water_amount': 30, 
             'price': 100 
            }, 
         'order_type': 'refill'
    }
}

let pour = false;
let refill = false;
let unitPrice = .49;
let pourAmt = 0;

const pourWater = async function (ws) {
    console.log('Begin Refill' + refill);
    while(refill) {
        console.log('DEBUG refill loop');
        while(pour) {
            refill_status.message.data.water_amount = pourAmt;
            refill_status.message.data.price = pourAmt * unitPrice;
            ws.send(JSON.stringify(refill_status));
            pourAmt++;
            console.log('sendingWaterAmt' + pourAmt);
            await waitForMe(1);
        }
        await waitForMe(1);
    }
    console.log('Exiting Refill')
}

const beginRefill = async function (ws) {
    refill=true;
    pourWater(ws);
}

const startPour = async function () {
    pour = true;
}

const stopPour = async function () {
    pour = false;

}

const endRefill = async function () {
    pour = false;
    refill = false;
    pourAmt = 0;
}


const processDrink = async function (ws) {
  const message1 = {
    message: {
      topic: "order_status",
      data: "order_complete",
      order_type: "drop_bottle",
    },
  };
  const message2 = {
    message: {
      topic: "order_status",
      data: "door_open",
      order_type: "drop_bottle",
    },
  };
  const messageError = {
    message: {
      topic: "order_status",
      data: "order_failed",
      order_type: "drop_bottle",
    },
  };

  await waitForMe(2);
  console.log('sending message 1');
  ws.send(JSON.stringify(message1));
  //ws.send(JSON.stringify(messageError));
  await waitForMe(2);
  console.log('sending message 2');
  ws.send(JSON.stringify(message2));
  //ws.send(JSON.stringify(messageError));
};

const processPayment = async function (ws) {
  const message1 = {
    message: {
      topic: "order_status",
      data: "preauthorization_success",
      status: "success",
    },
  };
  const message2 = {
    message: {
      topic: "order_status",
      data: "preauthorization_failure",
      status: "failed",
    },
  };
  const message3 = {
    message: {
      topic: "order_status",
      data: "preauthorization_failure",
      status: "retrying",
    },
  };

  await waitForMe(2);
  console.log('sending message 1');
  ws.send(JSON.stringify(message1));
  //ws.send(JSON.stringify(message2));
  //ws.send(JSON.stringify(message3));
};

const waitForMe = function (seconds) {
  return new Promise((resolve) => {
    setTimeout(function () {
      console.log("waited some time");
      resolve();
    }, seconds * 1000);
  });
};
